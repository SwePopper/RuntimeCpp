#include "platform.h"
#include "game.h"

#include <algorithm>
#include <string>

int main(int /*argc*/, char** argv)
{
	std::string root_path = argv[0];
	std::replace(root_path.begin(), root_path.end(), '\\', '/');

	std::size_t pos = root_path.rfind('/');
	if(std::string::npos == pos)
	{
		root_path = "";
	}
	else
	{
		root_path.erase(pos, root_path.size() - pos);
	}

	Game game;
	if(game.Initialize(root_path.c_str()))
	{
		while(game.Update())
			;
	}
	return 0;
}

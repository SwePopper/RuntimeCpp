# Locate SDL2 library
# SDL2_LIBRARY, the name of the library to link against
# SDL2_FOUND, if false, do not try to link to SDL2
# SDL2_INCLUDE_DIR, where to find SDL.h
#
# Locate SDL2_image library
# SDL2_IMAGE_INCLUDE_DIRS, path where to find SDL_image.h
# SDL2_IMAGE_LIBRARIES, the library to link against
# SDL2_IMAGE_FOUND, if false, do not try to use SDL2_image
#
# Locate SDL2_ttf library
# SDL2_TTF_INCLUDE_DIRS, path where to find SDL_ttf.h
# SDL2_TTF_LIBRARIES, the library to link against
# SDL2_TTF_FOUND, if false, do not try to use SDL_ttf


#--------------------------------------------------------------------------------
# SDL2

set(SDL2_SEARCH_PATHS
	~/Library/Frameworks
	/Library/Frameworks
	/usr/local
	/usr
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
)

find_path(SDL2_INCLUDE_DIR SDL.h
	HINTS
	$ENV{SDL2DIR}
	PATH_SUFFIXES include/SDL2 include SDL2
	PATHS ${SDL2_SEARCH_PATHS}
)

find_library(SDL2_LIBRARY_TEMP
	NAMES SDL2
	HINTS
	$ENV{SDL2DIR}
	PATH_SUFFIXES lib lib/x86 lib64 lib/x64
	PATHS ${SDL2_SEARCH_PATHS}
)

if(NOT SDL2_BUILDING_LIBRARY)
	if(NOT ${SDL2_INCLUDE_DIR} MATCHES ".framework")
		# Non-OS X framework versions expect you to also dynamically link to
		# SDL2main. This is mainly for Windows and OS X. Other (Unix) platforms
		# seem to provide SDL2main for compatibility even though they don't
		# necessarily need it.
		find_library(SDL2MAIN_LIBRARY
			NAMES SDL2main
			HINTS
			$ENV{SDL2DIR}
			PATH_SUFFIXES lib lib/x86 # lib64 lib/x64
			PATHS ${SDL2_SEARCH_PATHS}
		)
	endif(NOT ${SDL2_INCLUDE_DIR} MATCHES ".framework")
endif(NOT SDL2_BUILDING_LIBRARY)

# SDL2 may require threads on your system.
# The Apple build may not need an explicit flag because one of the
# frameworks may already provide it.
# But for non-OSX systems, I will use the CMake Threads package.
if(NOT APPLE)
	find_package(Threads)
endif(NOT APPLE)

# MinGW needs an additional library, mwindows
# It's total link flags should look like -lmingw32 -lSDL2main -lSDL2 -lmwindows
# (Actually on second look, I think it only needs one of the m* libraries.)
if(MINGW)
	set(MINGW32_LIBRARY mingw32 CACHE STRING "mwindows for MinGW")
endif(MINGW)

if(SDL2_LIBRARY_TEMP)
	# For SDL2main
	if(NOT SDL2_BUILDING_LIBRARY)
		if(SDL2MAIN_LIBRARY)
			set(SDL2_LIBRARY_TEMP ${SDL2MAIN_LIBRARY} ${SDL2_LIBRARY_TEMP})
		endif(SDL2MAIN_LIBRARY)
	endif(NOT SDL2_BUILDING_LIBRARY)

	# For OS X, SDL2 uses Cocoa as a backend so it must link to Cocoa.
	# CMake doesn't display the -framework Cocoa string in the UI even
	# though it actually is there if I modify a pre-used variable.
	# I think it has something to do with the CACHE STRING.
	# So I use a temporary variable until the end so I can set the
	# "real" variable in one-shot.
	if(APPLE)
		set(SDL2_LIBRARY_TEMP ${SDL2_LIBRARY_TEMP} "-framework Cocoa")
	endif(APPLE)

	# For threads, as mentioned Apple doesn't need this.
	# In fact, there seems to be a problem if I used the Threads package
	# and try using this line, so I'm just skipping it entirely for OS X.
	if(NOT APPLE)
		set(SDL2_LIBRARY_TEMP ${SDL2_LIBRARY_TEMP} ${CMAKE_THREAD_LIBS_INIT})
	endif(NOT APPLE)

	# For MinGW library
	if(MINGW)
		set(SDL2_LIBRARY_TEMP ${MINGW32_LIBRARY} ${SDL2_LIBRARY_TEMP})
	endif(MINGW)

	# set the final string here so the GUI reflects the final state.
	set(SDL2_LIBRARY ${SDL2_LIBRARY_TEMP} CACHE STRING "Where the SDL2 Library can be found")
	# set the temp variable to INTERNAL so it is not seen in the CMake GUI
	set(SDL2_LIBRARY_TEMP "${SDL2_LIBRARY_TEMP}" CACHE INTERNAL "")
endif(SDL2_LIBRARY_TEMP)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(SDL2 REQUIRED_VARS SDL2_LIBRARY SDL2_INCLUDE_DIR)

#--------------------------------------------------------------------------------
# SDL2_image

find_library(SDL2_IMAGE_LIBRARIES SDL2_image
	HINTS
	$ENV{SDL2DIR}
	PATH_SUFFIXES lib lib/x86 lib64 lib/x64
	PATHS ${SDL2_SEARCH_PATHS}
)

find_path(SDL2_IMAGE_INCLUDE_DIRS SDL_image.h
	HINTS
	$ENV{SDL2DIR}
	PATH_SUFFIXES include/SDL2 include SDL2
	PATHS ${SDL2_SEARCH_PATHS}
)

find_package_handle_standard_args(SDL2_IMAGE REQUIRED_VARS SDL2_IMAGE_LIBRARIES SDL2_IMAGE_INCLUDE_DIRS)

#--------------------------------------------------------------------------------
# SDL2_ttf

find_library(SDL2_TTF_LIBRARIES SDL2_ttf
	HINTS
	$ENV{SDL2DIR}
	PATH_SUFFIXES lib lib/x86 lib64 lib/x64
	PATHS ${SDL2_SEARCH_PATHS}
)

find_path(SDL2_TTF_INCLUDE_DIRS SDL_ttf.h
	HINTS
	$ENV{SDL2DIR}
	PATH_SUFFIXES include/SDL2 include SDL2
	PATHS ${SDL2_SEARCH_PATHS}
)

find_package_handle_standard_args(SDL2_TTF REQUIRED_VARS SDL2_TTF_LIBRARIES SDL2_TTF_INCLUDE_DIRS)

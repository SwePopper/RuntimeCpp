# Find all dependencies
find_package(Assimp)
find_package(Box2D)
find_package(EXPAT)
find_package(Freetype)
find_package(GLM)
find_package(Imgui)
find_package(LIBPNG)
find_package(NFD)
find_package(SDL2)
find_package(Vulkan)
find_package(ZLIB)

if(NOT ZLIB_FOUND)
	# download
	set(ZLIB_URL "http://zlib.net/zlib-1.2.11.tar.gz")
	set(ZLIB_DOWNLOAD_PATH ${PROJECT_BINARY_DIR}/zlib.tar.gz)
	set(ZLIB_EXTRACTED_FILE ${CMAKE_SOURCE_DIR}/submodules)

	if (NOT EXISTS "${ZLIB_DOWNLOAD_PATH}")
		file(DOWNLOAD "${ZLIB_URL}" "${ZLIB_DOWNLOAD_PATH}")
	endif()

	# extract the source
	execute_process(
		COMMAND ${CMAKE_COMMAND} -E tar xzf ${ZLIB_DOWNLOAD_PATH}
		WORKING_DIRECTORY ${ZLIB_EXTRACTED_FILE})

	set(ZLIB_DIR ${CMAKE_SOURCE_DIR}/submodules/zlib-1.2.11)
	add_subdirectory(${ZLIB_DIR})

	# set the output variables
	set(ZLIB_LIBRARIES zlib)
	set(ZLIB_INCLUDE_DIRS ${ZLIB_DIR} ${PROJECT_BINARY_DIR}/submodules/zlib-1.2.11)
	set(ZLIB_LIBRARY ${ZLIB_LIBRARIES})
	set(ZLIB_INCLUDE_DIR ${ZLIB_INCLUDE_DIRS})
endif()

if(NOT LIBPNG_FOUND)
	# download
	set(LIBPNG_URL "http://prdownloads.sourceforge.net/libpng/libpng-1.6.29.tar.gz?download")
	set(LIBPNG_DOWNLOAD_PATH ${PROJECT_BINARY_DIR}/libpng.tar.gz)
	set(LIBPNG_EXTRACTED_FILE ${CMAKE_SOURCE_DIR}/submodules)

	if (NOT EXISTS "${LIBPNG_DOWNLOAD_PATH}")
		file(DOWNLOAD "${LIBPNG_URL}" "${LIBPNG_DOWNLOAD_PATH}")
	endif()

	# extract the source
	execute_process(
		COMMAND ${CMAKE_COMMAND} -E tar xzf ${LIBPNG_DOWNLOAD_PATH}
		WORKING_DIRECTORY ${LIBPNG_EXTRACTED_FILE})

	set(LIBPNG_DIR ${LIBPNG_EXTRACTED_FILE}/libpng-1.6.29)

	set(SKIP_INSTALL_ALL ON)
	option(PNG_BUILD_ZLIB "Custom zlib Location, else find_package is used" ON)
	add_subdirectory(${LIBPNG_DIR})

	# set the output variables
	set(LIBPNG_LIBRARIES png)
	set(LIBPNG_INCLUDE_DIRS ${LIBPNG_DIR} ${PROJECT_BINARY_DIR}/submodules/libpng-1.6.29)
endif()

if(NOT FREETYPE_FOUND)
	# download
	set(FREETYPE_URL "https://download.savannah.gnu.org/releases/freetype/freetype-2.8.tar.gz")
	set(FREETYPE_DOWNLOAD_PATH ${PROJECT_BINARY_DIR}/freetype.tar.gz)
	set(FREETYPE_EXTRACTED_FILE ${CMAKE_SOURCE_DIR}/submodules)

	if (NOT EXISTS "${FREETYPE_DOWNLOAD_PATH}")
		file(DOWNLOAD "${FREETYPE_URL}" "${FREETYPE_DOWNLOAD_PATH}")
	endif()

	# extract the source
	execute_process(
		COMMAND ${CMAKE_COMMAND} -E tar xzf ${FREETYPE_DOWNLOAD_PATH}
		WORKING_DIRECTORY ${FREETYPE_EXTRACTED_FILE})

	set(FREETYPE_DIR ${FREETYPE_EXTRACTED_FILE}/freetype-2.8)

	# build
	if (MSVC)
		set(BUILD_SHARED_LIBS OFF)
	else()
		set(BUILD_SHARED_LIBS ON)
	endif()
	add_subdirectory(${FREETYPE_DIR})

	# set the output variables
	set(FREETYPE_LIBRARIES freetype)
	set(FREETYPE_INCLUDE_DIRS ${FREETYPE_DIR}/include)
endif()

if(NOT SDL2_FOUND)
	# download
	set(SDL_DOWNLOAD_VERSION 2.0.7)
	set(SDL2_URL "https://www.libsdl.org/release/SDL2-${SDL_DOWNLOAD_VERSION}.tar.gz")
	set(SDL2_DOWNLOAD_PATH ${PROJECT_BINARY_DIR}/sdl2.tar.gz)
	set(SDL2_EXTRACTED_FILE ${CMAKE_SOURCE_DIR}/submodules)

	message(STATUS "Downloading SDL2 from: ${SDL2_URL}")

	if (NOT EXISTS "${SDL2_DOWNLOAD_PATH}")
		file(DOWNLOAD "${SDL2_URL}" "${SDL2_DOWNLOAD_PATH}")
	endif()

	# extract the source
	execute_process(
		COMMAND ${CMAKE_COMMAND} -E tar xzf ${SDL2_DOWNLOAD_PATH}
		WORKING_DIRECTORY ${SDL2_EXTRACTED_FILE})

	message(STATUS ${SDL2_DOWNLOAD_PATH})
	if(EXISTS ${SDL2_EXTRACTED_FILE}/SDL2-${SDL_DOWNLOAD_VERSION}/src/main/windows/version.rc)
		# remove rc because it failes the build
		execute_process(
			COMMAND ${CMAKE_COMMAND} -E remove SDL2-${SDL_DOWNLOAD_VERSION}/src/main/windows/version.rc
			WORKING_DIRECTORY ${SDL2_EXTRACTED_FILE})
	endif()

	# Copy include dir to SDL2 dir to mimic installed SDL2
	execute_process(
		COMMAND ${CMAKE_COMMAND} -E copy_directory
			${SDL2_EXTRACTED_FILE}/SDL2-${SDL_DOWNLOAD_VERSION}/include
			${SDL2_EXTRACTED_FILE}/SDL2-${SDL_DOWNLOAD_VERSION}/SDL2
		WORKING_DIRECTORY ${SDL2_EXTRACTED_FILE})

	# build
	option(VIDEO_OPENGLES OFF)
	set(VIDEO_VULKAN ON)
	option(VIDEO_WAYLAND OFF)
	add_subdirectory(${SDL2_EXTRACTED_FILE}/SDL2-${SDL_DOWNLOAD_VERSION})

	# set the output variables
	set(SDL2_LIBRARY SDL2)
	set(SDL2_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/submodules/SDL2-${SDL_DOWNLOAD_VERSION} ${CMAKE_SOURCE_DIR}/submodules/SDL2-${SDL_DOWNLOAD_VERSION}/SDL2)
endif()

if(NOT SDL2_IMAGE_FOUND)
	set(SDL2_URL "https://www.libsdl.org/projects/SDL_image/release/SDL2_image-2.0.2.tar.gz")
	set(SDL2_DOWNLOAD_PATH ${PROJECT_BINARY_DIR}/sdl2_image.tar.gz)
	set(SDL2_EXTRACTED_FILE ${CMAKE_SOURCE_DIR}/submodules)

	if (NOT EXISTS "${SDL2_DOWNLOAD_PATH}")
		file(DOWNLOAD "${SDL2_URL}" "${SDL2_DOWNLOAD_PATH}")
	endif()

	# extract the source
	execute_process(
		COMMAND ${CMAKE_COMMAND} -E tar xzf ${SDL2_DOWNLOAD_PATH}
		WORKING_DIRECTORY ${SDL2_EXTRACTED_FILE})

	set(SDL2_IMAGE_SOURCES_ROOT ${SDL2_EXTRACTED_FILE}/SDL2_image-2.0.2)

	# build
	set(SDL2_IMAGE_SOURCES
		${SDL2_IMAGE_SOURCES_ROOT}/IMG.c
		${SDL2_IMAGE_SOURCES_ROOT}/IMG_bmp.c
		${SDL2_IMAGE_SOURCES_ROOT}/IMG_gif.c
		${SDL2_IMAGE_SOURCES_ROOT}/IMG_jpg.c
		${SDL2_IMAGE_SOURCES_ROOT}/IMG_lbm.c
		${SDL2_IMAGE_SOURCES_ROOT}/IMG_pcx.c
		${SDL2_IMAGE_SOURCES_ROOT}/IMG_png.c
		${SDL2_IMAGE_SOURCES_ROOT}/IMG_pnm.c
		${SDL2_IMAGE_SOURCES_ROOT}/IMG_svg.c
		${SDL2_IMAGE_SOURCES_ROOT}/IMG_tga.c
		${SDL2_IMAGE_SOURCES_ROOT}/IMG_tif.c
		${SDL2_IMAGE_SOURCES_ROOT}/IMG_xcf.c
		${SDL2_IMAGE_SOURCES_ROOT}/IMG_xpm.c
		${SDL2_IMAGE_SOURCES_ROOT}/IMG_xv.c
		${SDL2_IMAGE_SOURCES_ROOT}/IMG_webp.c
	)
	add_library(SDL2_image SHARED ${SDL2_IMAGE_SOURCES})
	target_link_libraries(SDL2_image
		${SDL2_LIBRARY}
		${LIBPNG_LIBRARIES}
	)
	target_include_directories(SDL2_image
		PRIVATE ${SDL2_IMAGE_SOURCES_ROOT}
		${SDL2_INCLUDE_DIR}
		${LIBPNG_INCLUDE_DIRS}
	)
	target_compile_definitions(SDL2_image PRIVATE LOAD_PNG)

	# set the output variables
	set(SDL2_IMAGE_LIBRARIES SDL2_image)
	set(SDL2_IMAGE_INCLUDE_DIRS ${SDL2_IMAGE_SOURCES_ROOT})
endif()

if(NOT SDL2_TTF_FOUND)
	# download
	set(SDL2_URL "https://www.libsdl.org/projects/SDL_ttf/release/SDL2_ttf-2.0.14.tar.gz")
	set(SDL2_DOWNLOAD_PATH ${PROJECT_BINARY_DIR}/sdl2_ttf.tar.gz)
	set(SDL2_EXTRACTED_FILE ${CMAKE_SOURCE_DIR}/submodules)

	if (NOT EXISTS "${SDL2_DOWNLOAD_PATH}")
		file(DOWNLOAD "${SDL2_URL}" "${SDL2_DOWNLOAD_PATH}")
	endif()

	# extract the source
	execute_process(
		COMMAND ${CMAKE_COMMAND} -E tar xzf ${SDL2_DOWNLOAD_PATH}
		WORKING_DIRECTORY ${SDL2_EXTRACTED_FILE})

	set(SDL2_TTF_SOURCES_ROOT ${SDL2_EXTRACTED_FILE}/SDL2_ttf-2.0.14)

	# build
	set(SDL2_TTF_SOURCES
		${SDL2_TTF_SOURCES_ROOT}/SDL_ttf.c
	)
	add_library(SDL2_ttf SHARED ${SDL2_TTF_SOURCES})
	target_link_libraries(SDL2_ttf
		${SDL2_LIBRARY}
		${FREETYPE_LIBRARIES}
	)
	target_include_directories(SDL2_ttf
		PRIVATE ${SDL2_TTF_SOURCES_ROOT}
		${SDL2_INCLUDE_DIR}
		${FREETYPE_INCLUDE_DIRS}
	)

	# set the output variables
	set(SDL2_TTF_LIBRARIES SDL2_ttf)
	set(SDL2_TTF_INCLUDE_DIRS ${SDL2_TTF_SOURCES_ROOT})
endif()

if(NOT EXPAT_FOUND)
	# get the dependency from git
	execute_process(
		COMMAND git clone https://github.com/libexpat/libexpat submodules/libexpat
		WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})

	# turn off some options
	option(BUILD_tools "build the xmlwf tool for expat library" OFF)
	option(BUILD_examples "build the examples for expat library" OFF)
	option(BUILD_tests "build the tests for expat library" OFF)
	option(BUILD_doc "build man page for xmlwf" OFF)
	option(INSTALL "install expat files in cmake install target" OFF)

	# build it
	add_subdirectory(${CMAKE_SOURCE_DIR}/submodules/libexpat/expat)
	target_include_directories(expat
		PRIVATE ${PROJECT_BINARY_DIR}/submodules/libexpat/expat
	)

	# set the output variables
	set(EXPAT_LIBRARIES expat)
	set(EXPAT_INCLUDE_DIRS ${CMAKE_SOURCE_DIR}/submodules/libexpat/expat/lib)
endif()

if(NOT BOX2D_FOUND)
	# get the dependency from git
	execute_process(
		COMMAND git clone https://github.com/erincatto/Box2D.git submodules/box2d
		WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})

	execute_process(
		COMMAND git checkout tags/v2.3.1
		WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/submodules/box2d)

	set(BOX2D_ROOT ${CMAKE_SOURCE_DIR}/submodules/box2d/Box2D)

	# build
	file(GLOB_RECURSE BOX2D_SOURCES ${BOX2D_ROOT}/Box2D/*.c**)
	add_library(Box2D SHARED ${BOX2D_SOURCES})
	#target_link_libraries(Box2D	)
	target_include_directories(Box2D
		PRIVATE ${BOX2D_ROOT}
	)

	# set the output variables
	set(BOX2D_LIBRARY Box2D)
	set(BOX2D_INCLUDE_DIR ${BOX2D_ROOT})
endif()

if(NOT Vulkan_FOUND)
	message(FATAL "Download the vulkan sdk from https://vulkan.lunarg.com/")
endif()

if(NOT ASSIMP_FOUND)
	# get the dependency from git
	execute_process(
		COMMAND git clone https://github.com/assimp/assimp.git submodules/assimp
		WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})

	option(ASSIMP_BUILD_ASSIMP_TOOLS OFF)
	option(ASSIMP_BUILD_TESTS OFF)
	add_subdirectory(${CMAKE_SOURCE_DIR}/submodules/assimp)

	set(ASSIMP_LIBRARY assimp)
	set(ASSIMP_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/submodules/assimp/include)
endif()

if(NOT GLM_FOUND)
	# get the dependency from git
	execute_process(
		COMMAND git clone https://github.com/g-truc/glm.git submodules/glm
		WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})

	set(GLM_INCLUDE_DIRS ${CMAKE_SOURCE_DIR}/submodules/glm)
endif()

if(NOT NFD_FOUND)
	# get the dependency from git
	execute_process(
		COMMAND git clone https://github.com/mlabbe/nativefiledialog.git submodules/nfd
		WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})

	set(NFD_PLATFORM_SOURCES )
	set(GTK3_INCLUDE_DIRS )
	set(GTK3_LIBRARIES )
	if(MSVC)
		set(NFD_PLATFORM_SOURCES ${CMAKE_SOURCE_DIR}/submodules/nfd/src/nfd_win.cpp)
	else()
		set(NFD_PLATFORM_SOURCES ${CMAKE_SOURCE_DIR}/submodules/nfd/src/nfd_gtk.c)

		find_package(PkgConfig REQUIRED)
		pkg_check_modules(GTK3 REQUIRED gtk+-3.0)
	endif()

	set(NFD_LIBRARY nfd)
	set(NFD_INCLUDE_DIR submodules/nfd/src/include)
	set(NFD_SOURCES
		submodules/nfd/src/nfd_common.c
		${NFD_PLATFORM_SOURCES}
	)
	add_library(${NFD_LIBRARY} SHARED ${NFD_SOURCES})
	target_link_libraries(${NFD_LIBRARY} ${GTK3_LIBRARIES})
	target_include_directories(${NFD_LIBRARY} PRIVATE ${NFD_INCLUDE_DIR} ${GTK3_INCLUDE_DIRS})

	if(NOT CMAKE_HOST_WIN32)
		target_compile_options(${NFD_LIBRARY} PRIVATE -fPIC)
	endif()
endif()

if(NOT IMGUI_FOUND)
	# get the dependency from git
	execute_process(
		COMMAND git clone https://github.com/ocornut/imgui.git submodules/imgui
		WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})

	set(IMGUI_LIBRARY imgui)
	set(IMGUI_INCLUDE_DIR submodules/imgui)

	file(GLOB IMGUI_SOURCES submodules/imgui/*.c**)
	file(GLOB IMGUI_HEADERS submodules/imgui/*.h**)

	add_library(${IMGUI_LIBRARY} SHARED ${IMGUI_SOURCES} ${IMGUI_HEADERS})
	target_include_directories(${IMGUI_LIBRARY} PRIVATE ${IMGUI_INCLUDE_DIR})

	if(NOT CMAKE_HOST_WIN32)
		target_compile_options(${IMGUI_LIBRARY} PRIVATE -fPIC)
	endif()
endif()

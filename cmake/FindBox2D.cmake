#.rst:
# FindBOX2D
# ---------
#
# Find BOX2D
#
# Find the native BOX2D headers and libraries.
#
# ::
#
#   BOX2D_INCLUDE_DIR
#   BOX2D_LIBRARY
#   BOX2D_FOUND

set(BOX2D_SEARCH_PATHS
	~/Library/Frameworks
	/Library/Frameworks
	/usr/local
	/usr
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
)

# Look for the header file.
find_path(BOX2D_INCLUDE_DIR NAMES Box2D/Box2D.h
	HINTS
	$ENV{BOX2DIR}
	PATH_SUFFIXES include/SDL2 include SDL2
	PATHS ${BOX2D_SEARCH_PATHS}
)

# Look for the library.
find_library(BOX2D_LIBRARY NAMES Box2D libBox2D
	HINTS
	$ENV{BOX2DIR}
	PATH_SUFFIXES lib lib/x86 lib64 lib/x64
	PATHS ${BOX2D_SEARCH_PATHS}
)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(BOX2D REQUIRED_VARS BOX2D_LIBRARY BOX2D_INCLUDE_DIR)

mark_as_advanced(BOX2D_INCLUDE_DIR BOX2D_LIBRARY)

#.rst:
# FindGLEW
# --------
#
# Find the OpenGL Extension Wrangler Library (GLEW)
#
# IMPORTED Targets
# ^^^^^^^^^^^^^^^^
#
# This module defines the :prop_tgt:`IMPORTED` target ``GLEW::GLEW``,
# if GLEW has been found.
#
# Result Variables
# ^^^^^^^^^^^^^^^^
#
# This module defines the following variables:
#
# ::
#
#   GLEW_INCLUDE_DIRS - include directories for GLEW
#   GLEW_LIBRARIES - libraries to link against GLEW
#   GLEW_FOUND - true if GLEW has been found and can be used

#=============================================================================
# Copyright 2012 Benjamin Eikel
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
# (To distribute this file outside of CMake, substitute the full
#  License text for the above reference.)

set(GLEW_SEARCH_PATHS
	~/Library/Frameworks
	/Library/Frameworks
	/usr/local
	/usr
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
)

find_path(GLEW_INCLUDE_DIR GL/glew.h
	HINTS
	$ENV{BOX2DIR}
	PATH_SUFFIXES include/SDL2 include SDL2
	PATHS ${GLEW_SEARCH_PATHS}
)

find_library(GLEW_LIBRARY NAMES GLEW glew32s glew glew32 PATH_SUFFIXES lib64
	HINTS
	$ENV{EXPATDIR}
	PATH_SUFFIXES lib lib/x86 lib64 lib/x64
	PATHS ${GLEW_SEARCH_PATHS}
)

set(GLEW_INCLUDE_DIRS ${GLEW_INCLUDE_DIR})
set(GLEW_LIBRARIES ${GLEW_LIBRARY})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GLEW REQUIRED_VARS GLEW_INCLUDE_DIRS GLEW_LIBRARIES)

if(GLEW_FOUND AND NOT TARGET GLEW::GLEW)
	add_library(GLEW::GLEW UNKNOWN IMPORTED)
	set_target_properties(GLEW::GLEW PROPERTIES IMPORTED_LOCATION "${GLEW_LIBRARY}" INTERFACE_INCLUDE_DIRECTORIES "${GLEW_INCLUDE_DIRS}")
endif()

mark_as_advanced(GLEW_INCLUDE_DIR GLEW_LIBRARY)

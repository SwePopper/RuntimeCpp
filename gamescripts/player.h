#ifndef PLAYER_H
#define PLAYER_H

class PhysicsActor;
class Model;
class Transform;

#include "entity/components.h"

class Player : public Component
{
	COMPONENT_DATA(Player);

public:
	Player(Entity* entity);
	virtual ~Player();

	virtual void Update(float dt);

private:
	void UpdateInput(float dt);

	PhysicsActor* m_physics_actor;
	Model* m_model;

	std::uint32_t m_input_walk_up;
	std::uint32_t m_input_walk_left;
	std::uint32_t m_input_walk_down;
	std::uint32_t m_input_walk_right;

	std::uint32_t m_input_look_up;
	std::uint32_t m_input_look_left;
	std::uint32_t m_input_look_down;
	std::uint32_t m_input_look_right;

	std::uint32_t m_input_look_mouse_x;
	std::uint32_t m_input_look_mouse_y;
};

#endif

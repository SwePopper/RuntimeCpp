#include "platform.h"
#include "editor.h"
#include "game.h"
#include "entity/entity.h"
#include "io/xml_reader.h"

// Components
#include "entity/components.h"
#include "entity/components/transform.h"
#include "entity/components/camera.h"

#include "player.h"
#include "debug_components/debug_info.h"
#include "debug_components/hierarchy_view.h"
#include "debug_components/log_view.h"
#include "entity/components/model.h"

#include <cstdio>
#include <cstdint>
#include <vector>
#include <algorithm>
#include <cstring>

#include <imgui/imgui.h>
#include <nfd.h>

Editor::ComponentContainer Editor::sCreatedComponents;

Editor::Editor() : m_debug_entity(nullptr), m_play(true), m_step(false) {}

bool Editor::Initialize()
{
	Component::RegisterComponent(Player::name, Player::id, Create<Player>);
	Component::RegisterComponent(HierarchyView::name, HierarchyView::id, Create<HierarchyView>);
	Component::RegisterComponent(LogView::name, LogView::id, Create<LogView>);
	Component::RegisterComponent(DebugInfo::name, DebugInfo::id, Create<DebugInfo>);

	m_debug_entity = New(Entity) Entity("Debug", 0, true);
	m_debug_entity->CreateComponent<HierarchyView>();
	m_debug_entity->CreateComponent<LogView>();
	m_debug_entity->CreateComponent<DebugInfo>();

	NewScene();

	return true;
}

void Editor::Release()
{
	for(ComponentContainer::iterator it = sCreatedComponents.begin(); it != sCreatedComponents.end(); ++it)
	{
		Delete(*it);
	}
	sCreatedComponents.clear();

	Component::UnregisterComponent(Player::id);
	Component::UnregisterComponent(HierarchyView::id);
	Component::UnregisterComponent(LogView::id);
	Component::UnregisterComponent(DebugInfo::id);
}

void Editor::Update(float dt) { UNUSED(dt); }

void Editor::Draw(float dt)
{
	{
		Entity* entity = m_debug_entity;
		while(entity)
		{
			entity->Update(dt);
			entity = entity->GetNext();
		}
	}

	if(ImGui::BeginMainMenuBar())
	{
		if(ImGui::BeginMenu("File"))
		{
			if(ImGui::MenuItem("New Scene", "CTRL+S"))
			{
				NewScene();
			}
			if(ImGui::MenuItem("Save Scene", "CTRL+S"))
			{
				nfdchar_t* path = nullptr;
				if(NFD_SaveDialog(nullptr, Game::GetInstance()->GetAssetsFolder(), &path) == NFD_OKAY)
				{
					SaveScene(path);
					free(path);
				}
				else
				{
					LogInfo("Cancel save scene.\n");
				}
			}
			if(ImGui::MenuItem("Load Scene", ""))
			{
				nfdchar_t* path = nullptr;
				if(NFD_OpenDialog(nullptr, Game::GetInstance()->GetAssetsFolder(), &path) == NFD_OKAY)
				{
					ReadScene(path);
					free(path);
				}
				else
				{
					LogInfo("Cancel open scene.\n");
				}
			}
			ImGui::Separator();
			if(ImGui::MenuItem("Quit", ""))
			{
				Game::Quit();
			}
			ImGui::EndMenu();
		}
		if(ImGui::BeginMenu("Edit"))
		{
			if(ImGui::MenuItem("Play", "CTRL+P", false, !m_play))
			{
				m_play = true;
			}
			if(ImGui::MenuItem("Pause", "SHIFT+CTRL+P", false, m_play))
			{
				m_play = false;
			}
			if(ImGui::MenuItem("Step", "CTRL+ALT+P", false, !m_play))
			{
				m_step = true;
			}
			ImGui::EndMenu();
		}
		ImGui::EndMainMenuBar();
	}
}

void Editor::SerializeState(const char* file_path)
{
	FILE* file = fopen(file_path, "w");
	{
		File::Write(file, "<Backup>");
		{
			for(ComponentContainer::iterator it = sCreatedComponents.begin(); it != sCreatedComponents.end(); ++it)
			{
				File::Write(file, "<Component id=\"%u\" entity_uid=\"%u\">", static_cast<uint32_t>((*it)->GetID()), (*it)->GetEntity()->GetUID());
				(*it)->Serialize(file);
				File::Write(file, "</Component>");
			}
		}
		File::Write(file, "</Backup>");
	}
	fclose(file);
}

void Editor::DeserializeState(const char* file_path)
{
	struct ParseData
	{
		Entity* current;
		Component* component;
		ComponentID current_component_id;
		std::uint32_t padding;
	};

	ParseData data{nullptr, nullptr, static_cast<ComponentID>(-1), 0};

	XmlReader::ReadXML(file_path, &data,
					   [](void* user_data, const char* name, const char** atts) {
						   ParseData* pd = reinterpret_cast<ParseData*>(user_data);
						   if(strncmp(name, "Component", 10) == 0)
						   {
							   uint32_t entity_id = 0;
							   for(std::uint32_t i = 0; atts[i]; i += 2)
							   {
								   if(strncmp(atts[i], "id", 3) == 0)
								   {
									   pd->current_component_id = static_cast<ComponentID>(strtoul(atts[i + 1], nullptr, 0));
								   }
								   else if(strncmp(atts[i], "entity_uid", 11) == 0)
								   {
									   entity_id = static_cast<uint32_t>(strtoul(atts[i + 1], nullptr, 0));
								   }
							   }

							   pd->current = Game::GetInstance()->GetEntity(entity_id);
							   if(pd->current)
							   {
								   pd->component = pd->current->CreateComponent(pd->current_component_id);
							   }
						   }
						   else
						   {
							   if(pd->current)
							   {
								   pd->current->Deserialize(pd->current_component_id, name, atts);
							   }
						   }
					   },
					   [](void* user_data, const char* name) {
						   ParseData* pd = reinterpret_cast<ParseData*>(user_data);
						   if(strncmp(name, "Component", 10) == 0)
						   {
							   pd->component->DeserializeDone();
							   pd->current = nullptr;
							   pd->component = nullptr;
							   pd->current_component_id = 0;
						   }
					   });
}

void Editor::NewScene()
{
	Entity* player = New(Entity) Entity("Player", 0, false);
	player->CreateComponent(Hash32("Player"));

	Entity* camera = New(Entity) Entity("Camera", 1, false);
	camera->CreateComponent<Camera>();

	Transform* transform = camera->GetComponent<Transform>();
	transform->SetPosition(glm::vec3(0.0f, 5.0f, -15.0f));
	// transform->SetRotation(glm::vec3(0.0f, kPI, 0.0f));

	transform->SetParent(player->GetComponent<Transform>());

	player->SetNext(camera);

	Entity* floor = New(Entity) Entity("Floor", 0, false);

	Model* model = floor->CreateComponent<Model>();
	model->Initialize("floor.dae");

	player->SetNext(floor);

	Game::GetInstance()->SetRootEntity(player);
}

bool Editor::ReadScene(const char* path)
{
	struct ParseData
	{
		Entity* root;
		Entity* current;
		ComponentID current_component_id;
		std::uint32_t padding;
	};

	ParseData data{nullptr, nullptr, static_cast<ComponentID>(-1), 0};

	if(!XmlReader::ReadXML(path, &data,
						   [](void* user_data, const char* n, const char** atts) {
							   std::string_view name = n;
							   ParseData* pd = reinterpret_cast<ParseData*>(user_data);
							   if(name == "Entity")
							   {
								   std::string_view name = "";
								   uint32_t uid = 0;
								   for(std::uint32_t i = 0; atts[i]; i += 2)
								   {
									   if(strncmp(atts[i], "name", 5) == 0)
									   {
										   name = atts[i + 1];
									   }
									   else if(strncmp(atts[i], "uid", 4) == 0)
									   {
										   uid = static_cast<uint32_t>(strtoull(atts[i + 1], nullptr, 0));
									   }
								   }

								   Entity* entity = New(Entity) Entity(name, uid, false);
								   if(pd->current)
								   {
									   pd->current->SetNext(entity);
									   pd->current = entity;
								   }
								   else
								   {
									   pd->current = pd->root = entity;
								   }
							   }
							   else if(name == "Component")
							   {
								   for(std::uint32_t i = 0; atts[i]; i += 2)
								   {
									   if(strncmp(atts[i], "id", 3) == 0)
									   {
										   pd->current_component_id = static_cast<ComponentID>(strtoul(atts[i + 1], nullptr, 0));
										   pd->current->CreateComponent(pd->current_component_id);
									   }
								   }
							   }
							   else
							   {
								   if(pd->current)
								   {
									   pd->current->Deserialize(pd->current_component_id, name, atts);
								   }
							   }
						   },
						   nullptr))
	{
		if(data.root)
		{
			while(data.root->GetNext()) Delete(data.root->GetNext());
			Delete(data.root);
		}
	}

	Game::GetInstance()->SetRootEntity(data.root);

	{
		Entity* entity = data.root;
		while(entity)
		{
			entity->DeserializeDone();
			entity = entity->GetNext();
		}
	}
	return true;
}

void Editor::SaveScene(const char* path)
{
	Entity* entity = Game::GetInstance()->GetRootEntity();
	if(!entity) return;
	FILE* file = fopen(path, "w");
	File::Write(file, "<Entities>");

	while(entity)
	{
		if(!entity->IsTemporary())
		{
			File::Write(file, "<Entity name=\"%s\" uid=\"%u\">", entity->GetName(), entity->GetUID());
			entity->Serialize(file);
			File::Write(file, "</Entity>");
		}
		entity = entity->GetNext();
	}

	File::Write(file, "</Entities>");

	fclose(file);
}

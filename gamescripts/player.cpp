#include "platform.h"
#include "player.h"
#include "game.h"
#include "input.h"
#include "entity/components.h"
#include "entity/entity.h"
#include "entity/components/physics_actor.h"
#include "entity/components/model.h"
#include "entity/components/transform.h"
#include "entity/components/camera.h"

#include <imgui/imgui.h>
#include <Box2D/Dynamics/b2Body.h>
#include <SDL2/SDL_rect.h>

Player::Player(Entity* entity) : Component(id, entity), m_physics_actor(nullptr), m_model(nullptr)
{
	m_model = m_entity->CreateComponent<Model>();
	m_model->Initialize("voyager.dae");
	// m_model->Initialize("Rupee/Rupee.dae");

	m_physics_actor = m_entity->CreateComponent<PhysicsActor>();
	m_physics_actor->Initialize(16.0f, 32.0f, 32.0f, false);

	m_input_walk_up = Game::GetInstance()->GetInput()->RegisterAction("PlayerWalkUp", InputButton::kScancodeW);
	m_input_walk_left = Game::GetInstance()->GetInput()->RegisterAction("PlayerWalkLeft", InputButton::kScancodeA);
	m_input_walk_down = Game::GetInstance()->GetInput()->RegisterAction("PlayerWalkDown", InputButton::kScancodeS);
	m_input_walk_right = Game::GetInstance()->GetInput()->RegisterAction("PlayerWalkRight", InputButton::kScancodeD);

	m_input_look_up = Game::GetInstance()->GetInput()->RegisterAction("PlayerLookUp", InputButton::kScancodeUp);
	m_input_look_left = Game::GetInstance()->GetInput()->RegisterAction("PlayerLookLeft", InputButton::kScancodeLeft);
	m_input_look_down = Game::GetInstance()->GetInput()->RegisterAction("PlayerLookDown", InputButton::kScancodeDown);
	m_input_look_right = Game::GetInstance()->GetInput()->RegisterAction("PlayerLookRight", InputButton::kScancodeRight);

	m_input_look_mouse_x = Game::GetInstance()->GetInput()->RegisterAction("PlayerLookMouseX", InputButton::kMousePositionX);
	m_input_look_mouse_y = Game::GetInstance()->GetInput()->RegisterAction("PlayerLookMouseY", InputButton::kMousePositionY);
}

Player::~Player() {}

void Player::Update(float dt)
{
	if(!ImGui::GetIO().WantCaptureKeyboard && !ImGui::GetIO().WantCaptureMouse)
	{
		UpdateInput(dt);
	}
}

void Player::UpdateInput(float /*dt*/)
{
	Input* input = Game::GetInstance()->GetInput();

	float force = 10.0f * m_physics_actor->GetBody()->GetMass();
	b2Vec2 movement((-input->GetAction(m_input_walk_left).value + input->GetAction(m_input_walk_right).value) * force,
					(-input->GetAction(m_input_walk_up).value + input->GetAction(m_input_walk_down).value) * force);

	m_physics_actor->GetBody()->ApplyLinearImpulse(movement, m_physics_actor->GetBody()->GetWorldCenter(), true);

	glm::vec2 look;
	if(input->HasMouseInput())
	{
		const glm::vec2& window_size = Game::GetInstance()->GetWindowSize();
		glm::vec2 mouse_position(input->GetAction(m_input_look_mouse_x).value * window_size.x,
								 input->GetAction(m_input_look_mouse_y).value * window_size.y);
		// mouse_position -= Game::GetInstance()->GetCamera()->GetCamera();

		look = glm::vec2(m_physics_actor->GetBody()->GetPosition().x, m_physics_actor->GetBody()->GetPosition().y);
		look = mouse_position - look;
		glm::normalize(look);
	}
	else
	{
		look = glm::vec2(-input->GetAction(m_input_look_left).value + input->GetAction(m_input_look_right).value,
						 -input->GetAction(m_input_look_up).value + input->GetAction(m_input_look_down).value);
	}

	float desired_angle = atan2f(look.x, -look.y);
	m_physics_actor->GetBody()->SetTransform(m_physics_actor->GetBody()->GetPosition(), desired_angle);
}

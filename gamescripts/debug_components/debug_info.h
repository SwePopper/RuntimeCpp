#ifndef DEBUG_INFO_H
#define DEBUG_INFO_H

#include "entity/components.h"

class DebugInfo : public Component
{
	COMPONENT_DATA(DebugInfo);

public:
	DebugInfo(Entity* entity);
	virtual ~DebugInfo();

	virtual void Update(float dt);

	float m_delta_times[90];
	bool m_draw_box2d;

	bool m_show;
	std::uint32_t m_input_show;
};

#endif

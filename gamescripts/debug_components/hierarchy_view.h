#ifndef HIERARCHY_VIEW_H
#define HIERARCHY_VIEW_H

#include "entity/components.h"

class HierarchyView : public Component
{
	COMPONENT_DATA(HierarchyView);

public:
	HierarchyView(Entity* entity);
	virtual ~HierarchyView();

	virtual void Update(float dt);

private:
	void DrawEntity(Entity* entity);
	void DrawComponent(Component* component);

	bool m_show;
	std::uint32_t m_input_show;
};

#endif

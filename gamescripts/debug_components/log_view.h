#ifndef LOG_VIEW_H
#define LOG_VIEW_H

#include "entity/components.h"

#include <string>
#include <vector>

class LogView : public Component
{
	COMPONENT_DATA(LogView);

public:
	LogView(Entity* entity);
	virtual ~LogView();

	virtual void Update(float dt);

	void Clear();

	void AddLog(const char* msg);

private:
	using LineOffsets = std::vector<std::size_t>;

	bool m_show;
	std::uint32_t m_logger_id;

	std::string m_buffer;
	LineOffsets m_line_offsets;
	std::uint32_t m_input_show;
	bool m_scroll_to_bottom;
};

#endif

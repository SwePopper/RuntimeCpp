#include "platform.h"
#include "hierarchy_view.h"
#include "game.h"
#include "input.h"
#include "entity/entity.h"
#include "entity/components.h"

#include <imgui/imgui.h>

HierarchyView::HierarchyView(Entity* entity) : Component(id, entity), m_show(true)
{
	m_input_show = Game::GetInstance()->GetInput()->RegisterAction("DebugShowHierarchy", InputButton::kScancodeF2);
}

HierarchyView::~HierarchyView() {}

void HierarchyView::Update(float /*dt*/)
{
	if(m_show)
	{
		ImGui::SetNextWindowSize(ImVec2(200, 100), ImGuiSetCond_FirstUseEver);
		ImGui::Begin("Property Editor", &m_show);

		ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(2, 2));
		ImGui::Columns(2);
		ImGui::Separator();

		{
			Entity* entity = Game::GetInstance()->GetRootEntity();
			while(entity)
			{
				DrawEntity(entity);
				entity = entity->GetNext();
			}
		}

		ImGui::Columns(1);
		ImGui::Separator();
		ImGui::PopStyleVar();
		ImGui::End();
	}
	else
	{
		m_show = Game::GetInstance()->GetInput()->GetAction(m_input_show).Down();
	}
}

void HierarchyView::DrawEntity(Entity* entity)
{
	// Use object uid as identifier. Most commonly you could also use the object pointer as a base ID.
	ImGui::PushID(entity);

	// Text and Tree nodes are less high than regular widgets, here we add vertical spacing to
	// make the tree lines equal high.
	ImGui::AlignFirstTextHeightToWidgets();

	bool node_open = ImGui::TreeNode("Entity", "%s (%u)", entity->GetName(), entity->GetUID());
	ImGui::NextColumn();
	ImGui::AlignFirstTextHeightToWidgets();
	ImGui::Text("");
	ImGui::NextColumn();
	if(node_open)
	{
		Component* components = entity->GetComponents();
		while(components)
		{
			DrawComponent(components);
			components = components->GetNext();
		}

		ImGui::TreePop();
	}
	ImGui::PopID();
}

void HierarchyView::DrawComponent(Component* component)
{
	// Use field index as identifier.
	ImGui::PushID(component);
	ImGui::AlignFirstTextHeightToWidgets();

	bool node_open = ImGui::TreeNode("Component", "%s (%lu)", Component::GetTypeName(component->GetID()), reinterpret_cast<uintptr_t>(component));
	ImGui::NextColumn();
	ImGui::AlignFirstTextHeightToWidgets();
	ImGui::Text("");
	ImGui::NextColumn();
	if(node_open)
	{
		component->DrawImGui();
		ImGui::TreePop();
	}

	ImGui::PopID();
}

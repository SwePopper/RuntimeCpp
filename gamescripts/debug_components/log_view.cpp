#include "platform.h"
#include "log_view.h"
#include "game.h"
#include "input.h"
#include "entity/entity.h"
#include "entity/components.h"

#include <imgui/imgui.h>

#include <limits>

LogView::LogView(Entity* entity) : Component(id, entity), m_show(true), m_scroll_to_bottom(false)
{
	m_logger_id = Log::AddLogger(this, [](void* user_data, const char* message) {
		LogView* logger = reinterpret_cast<LogView*>(user_data);
		logger->AddLog(message);
	});

	m_input_show = Game::GetInstance()->GetInput()->RegisterAction("DebugShowHierarchy", InputButton::kScancodeF3);
}

LogView::~LogView()
{
	Log::RemoveLogger(m_logger_id);
	m_logger_id = std::numeric_limits<std::uint32_t>::max();
}

void LogView::Update(float /*dt*/)
{
	if(m_show)
	{
		static ImGuiTextFilter filter;

		ImGui::SetNextWindowSize(ImVec2(500, 400), ImGuiSetCond_FirstUseEver);
		ImGui::Begin("Log Window", &m_show);
		if(ImGui::Button("Clear")) Clear();
		ImGui::SameLine();
		ImGui::SameLine();
		filter.Draw("Filter", -100.0f);
		ImGui::Separator();
		ImGui::BeginChild("scrolling", ImVec2(0, 0), false, ImGuiWindowFlags_HorizontalScrollbar);

		if(filter.IsActive())
		{
			const char* buf_begin = m_buffer.c_str();
			const char* line = buf_begin;
			std::size_t line_offsets_count = m_line_offsets.size();
			for(std::size_t line_no = 0; line != NULL; ++line_no)
			{
				const char* line_end = (line_no < line_offsets_count) ? buf_begin + m_line_offsets[line_no] : NULL;
				if(filter.PassFilter(line, line_end)) ImGui::TextUnformatted(line, line_end);
				line = line_end && line_end[1] ? line_end + 1 : NULL;
			}
		}
		else
		{
			ImGui::TextUnformatted(m_buffer.c_str());
		}

		if(m_scroll_to_bottom) ImGui::SetScrollHere(1.0f);
		m_scroll_to_bottom = false;
		ImGui::EndChild();
		ImGui::End();
	}
	else
	{
		m_show = Game::GetInstance()->GetInput()->GetAction(m_input_show).Down();
	}
}

void LogView::Clear()
{
	m_buffer.clear();
	m_line_offsets.clear();
}

void LogView::AddLog(const char* msg)
{
	std::size_t old_size = m_buffer.size();
	m_buffer.append(msg);
	for(std::size_t new_size = m_buffer.size(); old_size < new_size; ++old_size)
	{
		if(m_buffer[old_size] == '\n') m_line_offsets.push_back(old_size);
	}
	m_scroll_to_bottom = true;
}

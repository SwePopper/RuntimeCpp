#include "platform.h"
#include "debug_info.h"
#include "game.h"
#include "input.h"
#include "entity/components.h"

#include <imgui/imgui.h>

DebugInfo::DebugInfo(Entity* entity) : Component(id, entity), m_draw_box2d(true), m_show(true)
{
	m_input_show = Game::GetInstance()->GetInput()->RegisterAction("DebugInfo", InputButton::kScancodeF4);
	memset(m_delta_times, 0, sizeof(m_delta_times));
}

DebugInfo::~DebugInfo() {}

void DebugInfo::Update(float dt)
{
	if(m_show)
	{
		double average = static_cast<double>(dt);
		for(int32_t i = ARRAY_COUNT(m_delta_times) - 1; i > 0; --i)
		{
			m_delta_times[i] = m_delta_times[i - 1];
			average += static_cast<double>(m_delta_times[i]);
		}
		m_delta_times[0] = dt;
		average /= ARRAY_COUNT(m_delta_times);

		ImGui::SetNextWindowSize(ImVec2(Game::GetInstance()->GetWindowSize().x - 50, 100), ImGuiSetCond_FirstUseEver);
		ImGui::Begin("Debug Info", &m_show);
		ImGui::PlotLines("", m_delta_times, ARRAY_COUNT(m_delta_times));
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();
		ImGui::Text("Frame Time Avg: %f", average);
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();
		ImGui::Checkbox("Draw Box2D", &m_draw_box2d);
		ImGui::End();

		Game::GetInstance()->DebugDrawBox2d(m_draw_box2d);
	}
	else
	{
		m_show = Game::GetInstance()->GetInput()->GetAction(m_input_show).Down();
	}
}

#ifndef EDITOR_H
#define EDITOR_H

class Component;
class Entity;

#include <vector>

class Editor
{
public:
	Editor();
	~Editor() { Release(); }

	bool Initialize();
	void Release();

	void Update(float dt);
	void Draw(float dt);

	void SerializeState(const char* file_path);
	void DeserializeState(const char* file_path);

	template <typename T>
	static Component* Create(Entity* entity)
	{
		Component* comp = New(T) T(entity);
		sCreatedComponents.push_back(comp);
		return comp;
	}

private:
	void NewScene();
	bool ReadScene(const char* path);
	void SaveScene(const char* path);

	Entity* m_debug_entity;
	bool m_play;
	bool m_step;

	using ComponentContainer = std::vector<Component*>;
	static ComponentContainer sCreatedComponents;
};

#endif

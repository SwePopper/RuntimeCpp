#include "platform.h"
#include "editor.h"

namespace
{
Editor* sEditor = nullptr;
}

#if defined(PLATFORM_WINDOWS)
#define DLLEXPORT __declspec(dllexport)
#else
#define DLLEXPORT
#endif

#ifdef __cplusplus
extern "C" {
#endif

DLLEXPORT bool Initialize(Allocator::AllocateFunction allocate_function, Allocator::DeallocateFunction deallocate_function)
{
	UNUSED(allocate_function);
	UNUSED(deallocate_function);

	sEditor = New(Editor) Editor();
	return sEditor->Initialize();
}

DLLEXPORT void Release()
{
	Delete(sEditor);
	sEditor = nullptr;
}

DLLEXPORT void Update(float dt)
{
	if(sEditor) sEditor->Update(dt);
}

DLLEXPORT void Draw(float dt)
{
	if(sEditor) sEditor->Draw(dt);
}

DLLEXPORT void Serialize(const char* file_path)
{
	if(sEditor) sEditor->SerializeState(file_path);
}

DLLEXPORT void Deserialize(const char* file_path)
{
	if(sEditor) sEditor->DeserializeState(file_path);
}

#ifdef __cplusplus
}
#endif

#include "platform.h"
#include "log.h"

#include <vector>
#include <limits>
#include <algorithm>
#include <string>
#include <cstring>
#include <cstdarg>
#include <ctime>

#if defined(PLATFORM_WINDOWS)
#include <windows.h>
#endif

class LogFile
{
public:
	LogFile() : m_file(nullptr) {}
	~LogFile()
	{
		if(m_file)
		{
			time_t t = time(0);
			struct tm* now = localtime(&t);

			char* time_string = asctime(now);
			time_string[std::strlen(time_string) - 1] = '\0';

			fwrite("\n\n", 1, 2, m_file);
			fwrite("Ended logging: ", 1, std::strlen("Ended logging: "), m_file);
			fwrite(time_string, 1, std::strlen(time_string), m_file);

			fclose(m_file);
			m_file = nullptr;
		}
	}

	void LogMessage(const char* message)
	{
		if(!m_file)
		{
			m_file = fopen("output.log", "w");
			if(!m_file)
			{
				fprintf(stderr, "Failed to open logfile: output.log\n");
				return;
			}

			time_t t = time(0);
			struct tm* now = localtime(&t);

			char* time_string = asctime(now);
			time_string[std::strlen(time_string) - 1] = '\0';

			fwrite("Started logging: ", 1, std::strlen("Started logging: "), m_file);
			fwrite(time_string, 1, std::strlen(time_string), m_file);

			LogInfo("\nGit-Branch: %s\nGit-Commit hash: %s", GAME_GIT_BRANCH, GAME_GIT_COMMIT_HASH);

			fwrite("\n\n", 1, 2, m_file);
		}

		fwrite(message, 1, std::strlen(message), m_file);
	}

private:
	FILE* m_file;
};

namespace Log
{
constexpr std::size_t kBufSize = 2048;

struct Logger
{
	std::uint32_t m_id;
	void* m_user_ptr;
	LogFunction m_log_function;
};
using Loggers = std::vector<Logger>;

static std::uint32_t sNextID = 0;
static Loggers sLoggers;
static LogFile sLogFile;
static std::uint32_t sLogFileID = std::numeric_limits<std::uint32_t>::max();

std::uint32_t AddLogger(void* user_data, LogFunction func)
{
	if(!func) return std::numeric_limits<std::uint32_t>::max();
	sLoggers.push_back({sNextID++, user_data, func});
	return sLoggers.back().m_id;
}

void RemoveLogger(std::uint32_t id)
{
	if(!sLoggers.empty())
	{
		sLoggers.erase(std::remove_if(sLoggers.begin(), sLoggers.end(), [&](const Logger& l) { return l.m_id == id; }), sLoggers.end());
	}
}

void LogMessage(const char* file, int line, LogSeverity severity, const char* format, ...)
{
	if(sLogFileID == std::numeric_limits<std::uint32_t>::max())
	{
		sLogFileID = AddLogger(nullptr, [](void*, const char* message) { sLogFile.LogMessage(message); });
	}

	char buff[kBufSize];

	va_list args;
	va_start(args, format);
	vsnprintf(buff, kBufSize, format, args);
	va_end(args);

	char message[kBufSize * 2];
	snprintf(message, kBufSize * 2, "%s::%i - %s", file, line, buff);

	static FILE* severity_logs[] = {stdout, stdout, stdout, stderr, stderr};
	fprintf(severity_logs[static_cast<int>(severity)], "%s\n", message);

#if defined(PLATFORM_WINDOWS)
	OutputDebugString(message);
#endif

	for(Loggers::const_iterator it = sLoggers.begin(); it != sLoggers.end(); ++it)
	{
		it->m_log_function(it->m_user_ptr, message);
	}
}
}

#ifndef GAME_H
#define GAME_H

class Assets;
class b2World;
class Camera;
class Config;
class Entity;
class ImGuiRenderer;
class Input;
class Renderer;
class Scripts;
class Vulkan;

typedef struct SDL_Window SDL_Window;

class Game
{
public:
	Game();
	~Game() { Release(); }

	bool Initialize(const char* root_path);
	void Release();

	bool Update();

	const char* GetRootFolder() const { return m_root_path; }
	const char* GetAssetsFolder() const { return m_assets_folder; }
	const char* GetSavePath() const { return m_save_path; }

	SDL_Window* GetWindow() const { return m_window; }

	Vulkan* GetVulkan() const { return m_vulkan; }
	Renderer* GetRenderer() const { return m_renderer; }

	const glm::vec2& GetWindowSize() const { return m_window_size; }
	b2World* GetWorld() const { return m_world; }

	const bool& IsDebugDrawBox2d() const { return m_debug_draw_box2d; }
	void DebugDrawBox2d(const bool& enable) { m_debug_draw_box2d = enable; }

	static Game* GetInstance() { return s_instance; }

	Input* GetInput() const { return m_input; }
	ImGuiRenderer* GetImGuiRenderer() const { return m_imgui_renderer; }
	Assets* GetAssets() const { return m_assets; }

	Entity* GetRootEntity() const { return m_entity; }
	void SetRootEntity(Entity* root);
	Entity* GetEntity(const uint32_t& entity_id) const;

	static void Quit();

private:
	bool SetupSignalHandling();

	const char* m_root_path;
	char* m_assets_folder;
	char* m_save_path;
	SDL_Window* m_window;
	Vulkan* m_vulkan;
	glm::vec2 m_window_size;
	Config* m_config;
	Scripts* m_scripts;
	Assets* m_assets;
	Renderer* m_renderer;

	b2World* m_world;
	bool m_debug_draw_box2d;

	Entity* m_entity;

	Input* m_input;
	ImGuiRenderer* m_imgui_renderer;

	bool m_play;
	bool m_step;

	std::uint32_t m_last_frame_timestamp;

	static Game* s_instance;
};

#endif

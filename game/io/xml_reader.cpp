#include "platform.h"
#include "xml_reader.h"

#include <expat.h>

#ifdef XML_LARGE_SIZE
#if defined(XML_USE_MSC_EXTENSIONS) && _MSC_VER < 1400
#define XML_FMT_INT_MOD "I64"
#else
#define XML_FMT_INT_MOD "ll"
#endif
#else
#define XML_FMT_INT_MOD "l"
#endif

namespace XmlReader
{
bool ReadXML(const char* path, void* user_data, StartElementFunction start_element_function, EndElementFunction end_element_function)
{
	FILE* file = fopen(path, "r");
	if(file == nullptr)
	{
		LogError("Failed to open xml file: %s\n", path);
		return false;
	}

	XML_Parser parser = XML_ParserCreate(nullptr);
	if(parser == nullptr)
	{
		LogError("Failed to create xml parser");
		fclose(file);
		return false;
	}

	XML_SetUserData(parser, user_data);
	XML_SetElementHandler(parser, start_element_function, end_element_function);

	char memory[BUFSIZ];
	int done = 0;
	do
	{
		int read = static_cast<int>(fread(memory, 1, BUFSIZ, file));
		done = feof(file);

		if(XML_Parse(parser, memory, read, done) == XML_STATUS_ERROR)
		{
			LogError("XML ERROR: %s at line %" XML_FMT_INT_MOD "u", XML_ErrorString(XML_GetErrorCode(parser)), XML_GetCurrentLineNumber(parser));
			XML_ParserFree(parser);
			fclose(file);
			return false;
		}
	} while(done == 0);

	XML_ParserFree(parser);
	fclose(file);
	return true;
}
}

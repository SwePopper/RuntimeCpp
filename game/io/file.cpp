#include "platform.h"
#include "file.h"

#include <sys/stat.h>
#include <cstring>
#include <cstdarg>

namespace File
{
bool FileExists(const char* path)
{
	struct stat buffer;
	return (stat(path, &buffer) == 0);
}

void Write(FILE* file, const char* format, ...)
{
	char buff[BUFSIZ];
	va_list args;
	va_start(args, format);

#ifdef PLATFORM_LINUX
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
#endif

	vsnprintf(buff, BUFSIZ, format, args);

#ifdef PLATFORM_LINUX
#pragma GCC diagnostic pop
#endif

	va_end(args);

	fwrite(buff, 1, std::strlen(buff), file);
}
}

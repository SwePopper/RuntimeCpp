#ifndef FILE_H
#define FILE_H

#include <cstdio>

namespace File
{
bool FileExists(const char* path);
void Write(FILE* file, const char* format, ...);
}

#endif

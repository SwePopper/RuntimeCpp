#ifndef XML_READER_H
#define XML_READER_H

namespace XmlReader
{
using StartElementFunction = void (*)(void* user_data, const char* name, const char** atts);
using EndElementFunction = void (*)(void* user_data, const char* name);
bool ReadXML(const char* path, void* user_data, StartElementFunction start_element_function, EndElementFunction end_element_function);
};

#endif

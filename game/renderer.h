#ifndef RENDERER_H
#define RENDERER_H

#include "vulkan/vulkan_renderer.h"

class Camera;
class Model;

class Renderer
{
public:
	Renderer();
	~Renderer() { Release(); }

	bool Initialize();
	void Release();

	void Draw();

	void AddModel(Model* model);
	void RemoveModel(Model* model);

	void SetCamera(Camera* camera) { m_camera = camera; }
	Camera* GetCamera() const { return m_camera; }

	vk::DescriptorSetLayout* GetDescriptorSetLayout() { return &m_multirendertarget.m_descriptor_layout; }

private:
	struct FrameBufferAttachment
	{
		vk::Image m_image;
		vk::DeviceMemory m_memory;
		vk::ImageView m_view;
		vk::Format m_format;
		uint32_t padding;
	};

	enum class Attachments : uint32_t
	{
		kPosition,
		kNormal,
		kAlbedo,
		kDepth,
		kCount
	};

	struct PipelineData
	{
		vk::Pipeline m_pipeline;
		vk::PipelineLayout m_layout;
		vk::DescriptorSetLayout m_descriptor_layout;
	};

	struct DeferredBuffer
	{
		glm::mat4 m_projection;
		glm::mat4 m_view;
		glm::mat4 m_model;
		glm::mat4 m_view_model;
	};

	struct Light
	{
		glm::vec4 position;
		glm::vec3 color;
		float radius;
	};

	struct DeferredLight
	{
		Light lights[6];
		glm::vec4 viewPos;
		glm::ivec2 windowSize;
	};

	using Models = std::vector<Model*>;

	bool CreateMultiRenderTargets();
	bool CreateAttachment(FrameBufferAttachment& attachment, vk::Format format, vk::ImageUsageFlags usage);
	bool CreatePipelineLayouts();
	bool CreatePipelines();
	bool CreateUniformBuffers();
	bool CreateDescriptorSet();

	void UpdateUniformBuffers();

	std::vector<vk::VertexInputBindingDescription> m_vertex_desc;
	std::vector<vk::VertexInputAttributeDescription> m_vertex_attributes;
	vk::PipelineVertexInputStateCreateInfo m_vertex_input_state;

	// Multi Render Target
	vk::Extent2D m_multirendertarget_size;
	vk::RenderPass m_multirendertarget_pass;
	vk::Framebuffer m_multirendertarget_frame;
	FrameBufferAttachment m_multirendertarget_attachments[static_cast<uint32_t>(Attachments::kCount)];
	vk::Sampler m_multirendertarget_sampler;
	PipelineData m_multirendertarget;

	// Deferred
	PipelineData m_deferred;
	PipelineData m_debug;

	Vulkan::Buffer m_deferred_buffer;
	Vulkan::Buffer m_lights_buffer;
	vk::DescriptorSet m_deferred_descriptor_set;

	Camera* m_camera;
	Models m_models;

	DeferredBuffer m_matrix_buffer;
	DeferredLight m_light_buffer;
};

#endif

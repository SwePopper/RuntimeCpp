#include "sharedlibrary_windows.h"

#if defined(WIN32) || defined(WIN64)

#include <cstdio>
#include <cstdint>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstring>
#include <memory>
#include <string>

LPTSTR GetLastErrorMessage()
{
	char* error_text = nullptr;
	FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_IGNORE_INSERTS, nullptr, GetLastError(),
				  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&error_text, 0, nullptr);
	return error_text;
}

void PrintError(const char* format)
{
	LPTSTR error_string = GetLastErrorMessage();
	if(error_string)
	{
		printf(format, error_string);
		LocalFree(error_string);
	}
	else
	{
		printf(format, "Unknown error");
	}
}

SharedLibraryWindows::SharedLibraryWindows() : m_handle(nullptr), m_last_modification_time(0), m_file_path(nullptr) {}
SharedLibraryWindows::~SharedLibraryWindows() { Release(); }

bool SharedLibraryWindows::Initialize(const char* path)
{
	std::string file_path = path;
	file_path += ".dll";

	struct stat st;
	if(stat(file_path.c_str(), &st))
	{
		printf("Failed to stat library: %s\n", file_path.c_str());
		return false;
	}

	Release();

	std::size_t length = file_path.size() + 1;
	m_file_path = new char[length];
	std::strncpy(m_file_path, file_path.c_str(), length);
	m_file_path[length - 1] = 0;

	m_handle = LoadLibrary(m_file_path);
	if(m_handle == nullptr)
	{
		PrintError("Failed to load library: %s\n");
		return false;
	}

	m_last_modification_time = st.st_mtime;
	return true;
}

void SharedLibraryWindows::Release()
{
	if(m_handle)
	{
		if(!FreeLibrary(m_handle)) PrintError("Failed to free library: %s\n");
		m_handle = nullptr;
	}

	delete[] m_file_path;
	m_file_path = nullptr;
}

bool SharedLibraryWindows::ShouldReload()
{
	if(m_file_path == nullptr) return false;

	struct stat st;
	if(stat(m_file_path, &st))
	{
		return false;
	}

	if(m_last_modification_time == st.st_mtime) return false;

	time_t rawtime;
	time(&rawtime);

	return rawtime != st.st_mtime + 10000;
}

bool SharedLibraryWindows::Reload()
{
	if(!ShouldReload()) return false;

	std::size_t length = strlen(m_file_path) + 1;
	char* path = static_cast<char*>(alloca(length));
	std::strncpy(path, m_file_path, length);
	path[length - 1] = 0;

	if(Initialize(path)) return true;
	printf("Failed to initialize library: %s\n", path);
	return false;
}

void* SharedLibraryWindows::GetAddress(const char* name)
{
	void* address = nullptr;
	if(m_handle)
	{
		address = reinterpret_cast<void*>(GetProcAddress(m_handle, name));
		if(address == nullptr)
		{
			PrintError("Failed to get library address: %s\n");
		}
	}
	return address;
}

#endif

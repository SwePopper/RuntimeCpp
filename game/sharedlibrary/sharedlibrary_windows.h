#ifndef SHAREDLIBRARY_UNIX_H
#define SHAREDLIBRARY_UNIX_H

#if defined(WIN32) || defined(WIN64)

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <windows.h>
#include <ctime>

#include "sharedlibrary.h"

class SharedLibraryWindows : public SharedLibrary
{
public:
	SharedLibraryWindows();
	virtual ~SharedLibraryWindows();

	virtual bool ShouldReload();
	virtual bool Reload();
	virtual void* GetAddress(const char* name);

protected:
	virtual bool Initialize(const char* path);
	void Release();

private:
	HMODULE m_handle;
	__time64_t m_last_modification_time;
	char* m_file_path;
};

#endif

#endif  // SHAREDLIBRARY_H

#ifndef SHAREDLIBRARY_H
#define SHAREDLIBRARY_H

class SharedLibrary
{
public:
	static SharedLibrary* Load(const char* path);
	virtual ~SharedLibrary();

	virtual bool ShouldReload();
	virtual bool Reload();

	virtual void* GetAddress(const char* /*name*/);

protected:
	SharedLibrary() {}

	virtual bool Initialize(const char* /*path*/);
};

#endif  // SHAREDLIBRARY_H

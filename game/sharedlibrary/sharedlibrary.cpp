#include "sharedlibrary.h"

#ifdef __linux
#include "sharedlibrary_unix.h"
using SharedLibraryType = SharedLibraryUnix;
#elif defined(WIN32) || defined(WIN64)
#include "sharedlibrary_windows.h"
using SharedLibraryType = SharedLibraryWindows;
#endif

SharedLibrary::~SharedLibrary() {}

bool SharedLibrary::Initialize(const char* /*path*/) { return false; }

bool SharedLibrary::ShouldReload() { return false; }
bool SharedLibrary::Reload() { return false; }

void* SharedLibrary::GetAddress(const char* /*name*/) { return nullptr; }

SharedLibrary* SharedLibrary::Load(const char* path)
{
	SharedLibrary* lib = new SharedLibraryType;
	if(!lib->Initialize(path))
	{
		delete lib;
		lib = nullptr;
	}
	return lib;
}

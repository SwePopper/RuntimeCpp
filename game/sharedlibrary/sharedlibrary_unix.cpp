#include "sharedlibrary_unix.h"

#ifdef __linux

#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dlfcn.h>
#include <cstring>
#include <memory>
#include <string>

SharedLibraryUnix::SharedLibraryUnix() : m_handle(nullptr), m_last_modification_time(0), m_file_path(nullptr) {}
SharedLibraryUnix::~SharedLibraryUnix() { Release(); }

bool SharedLibraryUnix::Initialize(const char* path)
{
	std::string file_path = path;
	file_path += ".so";

	Release();

	size_t length = file_path.size() + 1;
	m_file_path = new char[length];
	strncpy(m_file_path, file_path.c_str(), length);
	m_file_path[length - 1] = 0;

	struct stat st;
	if(stat(m_file_path, &st))
	{
		fprintf(stderr, "Failed to stat library: %s\n", file_path.c_str());
		fprintf(stderr, "stat error: %i\n", errno);
		return false;
	}

	m_handle = dlopen(m_file_path, RTLD_LAZY);
	if(m_handle == nullptr)
	{
		printf("Failed to load library: %s\n", dlerror());
		return false;
	}

	m_last_modification_time = st.st_mtim.tv_sec;
	return true;
}

void SharedLibraryUnix::Release()
{
	if(m_handle)
	{
		dlclose(m_handle);
		m_handle = nullptr;
	}

	delete[] m_file_path;
	m_file_path = nullptr;
}

bool SharedLibraryUnix::ShouldReload()
{
	if(m_file_path == nullptr) return false;

	struct stat st;
	if(stat(m_file_path, &st))
	{
		return false;
	}

	if(m_last_modification_time == st.st_mtim.tv_sec) return false;

	time_t rawtime;
	time(&rawtime);

	return rawtime != st.st_mtim.tv_sec + 10000;
}

bool SharedLibraryUnix::Reload()
{
	size_t length = strlen(m_file_path) - 2;
	char* path = static_cast<char*>(alloca(length));
	strncpy(path, m_file_path, length);
	path[length - 1] = 0;

	if(Initialize(path)) return true;
	printf("Failed to initialize library: %s\n", path);
	return false;
}

void* SharedLibraryUnix::GetAddress(const char* name)
{
	void* address = nullptr;
	if(m_handle)
	{
		address = dlsym(m_handle, name);
		if(address == nullptr)
		{
			printf("Failed to get library address: %s\n", name);
		}
	}
	return address;
}

#endif

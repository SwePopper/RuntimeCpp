#ifndef SHAREDLIBRARY_UNIX_H
#define SHAREDLIBRARY_UNIX_H

#ifdef __linux

#include <ctime>

#include "sharedlibrary.h"

class SharedLibraryUnix : public SharedLibrary
{
public:
	SharedLibraryUnix();
	virtual ~SharedLibraryUnix();

	virtual bool ShouldReload();
	virtual bool Reload();
	virtual void* GetAddress(const char* name);

protected:
	virtual bool Initialize(const char* path);
	void Release();

private:
	void* m_handle;
	__time_t m_last_modification_time;
	char* m_file_path;
};

#endif

#endif  // SHAREDLIBRARY_H

#include "platform.h"
#include "assets.h"
#include "game.h"
#include "vulkan/vulkan_renderer.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <SDL_image.h>
#include <string>
#include <algorithm>

Assets::Assets() {}

bool Assets::Initialize() { return true; }

void Assets::Release()
{
	for(Textures::iterator it = m_textures.begin(); it != m_textures.end(); ++it)
	{
		if(it->m_asset.m_image)
		{
			Game::GetInstance()->GetVulkan()->DestroyImage(it->m_asset);
		}
		delete[] it->m_path;
	}
	m_textures.clear();

	for(ModelDatas::iterator it = m_model_datas.begin(); it != m_model_datas.end(); ++it)
	{
		it->m_asset.Release();
		delete[] it->m_path;
	}
	m_model_datas.clear();

	for(Shaders::iterator it = m_shaders.begin(); it != m_shaders.end(); ++it)
	{
		Game::GetInstance()->GetVulkan()->GetDevice().destroyShaderModule(it->m_asset.m_shader_module, nullptr);
		delete[] it->m_path;
	}
	m_shaders.clear();
}

Vulkan::Texture Assets::LoadTexture(const char* path)
{
	uint32_t path_length = static_cast<uint32_t>(strlen(path));
	Textures::iterator it = std::find_if(m_textures.begin(), m_textures.end(), [path_length, path](const Asset<Vulkan::Texture>& item) {
		return item.m_path_length == path_length && strncmp(item.m_path, path, path_length) == 0;
	});

	if(it != m_textures.end())
	{
		it->m_ref_count++;
		return it->m_asset;
	}

	std::string real_path = Game::GetInstance()->GetAssetsFolder();
	real_path += path;

	SDL_Surface* surface = IMG_Load(real_path.c_str());
	if(surface == nullptr)
	{
		LogError("Failed IMG_Load: %s\n", path);
		return Vulkan::Texture();
	}

	SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, 0xFF, 0, 0xFF));

	vk::DeviceSize image_size = static_cast<uint64_t>((surface->format->BitsPerPixel / 8) * surface->w * surface->h);

	vk::ImageCreateInfo info;
	info.format = vk::Format::eR8G8B8A8Unorm;
	info.tiling = vk::ImageTiling::eOptimal;
	info.usage = vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled;
	info.imageType = vk::ImageType::e2D;
	info.extent.width = static_cast<uint32_t>(surface->w);
	info.extent.height = static_cast<uint32_t>(surface->h);
	info.extent.depth = 1;
	info.mipLevels = 1;
	info.arrayLayers = 1;
	info.samples = vk::SampleCountFlagBits::e1;
	info.sharingMode = vk::SharingMode::eExclusive;
	info.initialLayout = vk::ImageLayout::eUndefined;

	Vulkan::Texture texture = Game::GetInstance()->GetVulkan()->CreateImage(&info, surface->pixels, image_size, surface->format->BitsPerPixel);

	SDL_FreeSurface(surface);

	Asset<Vulkan::Texture> asset;
	asset.m_asset = texture;
	asset.m_path_length = path_length;
	asset.m_ref_count = 1;
	asset.m_path = new char[asset.m_path_length + 1];
	strncpy(asset.m_path, path, asset.m_path_length);
	asset.m_path[asset.m_path_length] = '\0';
	m_textures.push_back(asset);
	return texture;
}

void Assets::UnloadTexture(Vulkan::Texture& texture)
{
	if(texture.m_image)
	{
		Textures::iterator it = std::find_if(m_textures.begin(), m_textures.end(),
											 [texture](const Asset<Vulkan::Texture>& item) { return item.m_asset.m_image == texture.m_image; });
		if(it != m_textures.end())
		{
			if(--it->m_ref_count > 0) return;

			delete[] it->m_path;
			m_textures.erase(it);
		}

		Game::GetInstance()->GetVulkan()->DestroyImage(texture);
	}
}

void Assets::ProcessMaterial(uint32_t material_index, const aiScene* scene, Vulkan::Mesh& mesh, std::string_view directory)
{
	if(material_index < scene->mNumMaterials)
	{
		if(scene->mMaterials[material_index]->GetTextureCount(aiTextureType::aiTextureType_DIFFUSE) > 0)
		{
			aiString path;
			ai_real blend = 0.0f;
			aiTextureOp texture_op;
			aiTextureMapMode mapmode;

			if(aiReturn_SUCCESS == scene->mMaterials[material_index]->GetTexture(aiTextureType::aiTextureType_DIFFUSE, 0, &path, nullptr, nullptr,
																				 &blend, &texture_op, &mapmode))
			{
				std::string real_path = directory.data();
				real_path += "/";
				real_path += path.C_Str();

				mesh.m_albedo = LoadTexture(real_path.c_str());
			}
		}

		if(!mesh.m_albedo.m_image)
		{
			mesh.m_albedo = LoadTexture("white.png");
		}

		if(!mesh.m_normal.m_image)
		{
			mesh.m_normal = LoadTexture("normal.png");
		}
	}
}

template <typename Integer>
Integer* CopyIndices(const aiMesh* mesh, uint32_t& index_buffer_size)
{
	index_buffer_size = sizeof(Integer) * mesh->mNumFaces * 3;
	Integer* dest = reinterpret_cast<Integer*>(AllocateMemAligned(index_buffer_size, alignof(Integer)));
	for(uint32_t i = 0, index = 0; i < mesh->mNumFaces; ++i)
	{
		for(uint32_t j = 0; j < 3; ++j)
		{
			dest[index++] = static_cast<Integer>(mesh->mFaces[i].mIndices[j]);
		}
	}
	return dest;
}

void Assets::ProcessNode(const aiNode* node, const aiScene* scene, ModelData& model, std::string_view directory)
{
	for(uint32_t i = 0; i < node->mNumMeshes; ++i)
	{
		const aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];

		glm::vec3 bounds_min(std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
		glm::vec3 bounds_max(std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min());

		Vertex* vertices = NewArray(Vertex, mesh->mNumVertices);
		for(uint32_t i = 0; i < mesh->mNumVertices; ++i)
		{
			memcpy(vertices[i].m_position, &mesh->mVertices[i].x, sizeof(float) * 3);
			memcpy(vertices[i].m_normal, &mesh->mNormals[i].x, sizeof(float) * 3);
			memcpy(vertices[i].m_tangent, &mesh->mTangents[i].x, sizeof(float) * 3);
			memcpy(vertices[i].m_uv, &mesh->mTextureCoords[0][i].x, sizeof(float) * 2);
			if(mesh->HasVertexColors(0))
			{
				memcpy(vertices[i].m_color, &mesh->mColors[0][i].r, sizeof(float) * 3);
			}
			else
			{
				constexpr float default_color[]{1.0f, 1.0f, 1.0f};
				memcpy(vertices[i].m_color, default_color, sizeof(default_color));
			}

			// Vulkan uses a right-handed NDC (contrary to OpenGL), so simply flip Y-Axis
			vertices[i].m_position[1] *= -1.0f;

			for(int j = 0; j < 3; ++j)
			{
				bounds_min[j] = std::min(vertices[i].m_position[j], bounds_min[j]);
				bounds_max[j] = std::max(vertices[i].m_position[j], bounds_max[j]);
			}
		}

		uint32_t index_type = 0;
		uint32_t index_buffer_size = 0;
		void* indices = nullptr;
		if(mesh->mNumVertices < std::numeric_limits<uint16_t>::max())
		{
			indices = CopyIndices<uint16_t>(mesh, index_buffer_size);
			index_type = static_cast<uint32_t>(vk::IndexType::eUint16);
		}
		else
		{
			indices = CopyIndices<uint32_t>(mesh, index_buffer_size);
			index_type = static_cast<uint32_t>(vk::IndexType::eUint32);
		}

		Vulkan::Mesh model_mesh =
			Game::GetInstance()->GetVulkan()->CreateMesh(vertices, sizeof(Vertex) * mesh->mNumVertices, indices, index_buffer_size);
		model_mesh.m_index_type = index_type;
		model_mesh.m_index_count = mesh->mNumFaces * 3;
		ProcessMaterial(mesh->mMaterialIndex, scene, model_mesh, directory);

		// Bounds
		model_mesh.m_bounds.m_center = (bounds_max - bounds_min) * 0.5f;
		float radius = std::max(std::max(model_mesh.m_bounds.m_center.x, model_mesh.m_bounds.m_center.y), model_mesh.m_bounds.m_center.z);
		model_mesh.m_bounds.m_center += bounds_min;

		model_mesh.m_bounds.m_min = bounds_min;
		model_mesh.m_bounds.m_max = bounds_max;
		model_mesh.m_bounds.radius = radius;

		model.AddMesh(model_mesh);

		DeleteArray(vertices);
		FreeMem(indices);
	}
	for(uint32_t i = 0; i < node->mNumChildren; ++i) ProcessNode(node->mChildren[i], scene, model, directory);
}

ModelData Assets::LoadModel(const char* path)
{
	uint32_t path_length = static_cast<uint32_t>(strlen(path));
	ModelDatas::iterator it = std::find_if(m_model_datas.begin(), m_model_datas.end(), [path_length, path](const Asset<ModelData>& item) {
		return item.m_path_length == path_length && strncmp(item.m_path, path, path_length) == 0;
	});

	if(it != m_model_datas.end())
	{
		it->m_ref_count++;
		return it->m_asset;
	}

	std::string real_path = Game::GetInstance()->GetAssetsFolder();
	real_path += path;

	Assimp::Importer importer;
	const aiScene* scene =
		importer.ReadFile(real_path.c_str(), aiProcessPreset_TargetRealtime_MaxQuality | aiProcess_PreTransformVertices | aiProcess_FlipWindingOrder);

	if(scene == nullptr)
	{
		LogError("Failed Assimp::Importer::ReadFile: %s\n", path);
		return ModelData();
	}

	real_path = path;
	std::string::size_type pos = real_path.find_last_of('/');
	if(std::string::npos != pos)
	{
		real_path.resize(pos);
	}

	Asset<ModelData> asset;
	ProcessNode(scene->mRootNode, scene, asset.m_asset, real_path);
	asset.m_path_length = path_length;
	asset.m_ref_count = 1;
	asset.m_path = new char[asset.m_path_length + 1];
	strncpy(asset.m_path, path, asset.m_path_length);
	asset.m_path[asset.m_path_length] = '\0';
	m_model_datas.push_back(asset);
	return asset.m_asset;
}

void Assets::UnloadModel(ModelData model)
{
	ModelDatas::iterator it =
		std::find_if(m_model_datas.begin(), m_model_datas.end(), [model](const Asset<ModelData>& item) { return item.m_asset == model; });
	if(it != m_model_datas.end())
	{
		if(--it->m_ref_count > 0) return;

		delete[] it->m_path;
		m_model_datas.erase(it);
	}

	model.Release();
}

ShaderModule Assets::LoadShader(const char* path)
{
	uint32_t path_length = static_cast<uint32_t>(strlen(path));
	Shaders::iterator it = std::find_if(m_shaders.begin(), m_shaders.end(), [path_length, path](const Asset<ShaderModule>& item) {
		return item.m_path_length == path_length && strncmp(item.m_path, path, path_length) == 0;
	});

	if(it != m_shaders.end())
	{
		it->m_ref_count++;
		return it->m_asset;
	}

	std::string real_path = Game::GetInstance()->GetAssetsFolder();
	real_path += path;

	FILE* file = fopen(real_path.c_str(), "rb");
	if(file == nullptr) return ShaderModule();

	fseek(file, 0, SEEK_END);
	size_t size = static_cast<size_t>(ftell(file));
	fseek(file, 0, SEEK_SET);

	char* shader_mem = reinterpret_cast<char*>(AllocateMem(size + 1));
	fread(shader_mem, 1, size, file);
	shader_mem[size] = 0;

	fclose(file);

	Asset<ShaderModule> asset;
	bool result = asset.m_asset.Create(shader_mem, size);
	FreeMem(shader_mem);

	if(!result)
	{
		LogError("Failed to create shader module: %s\n", path);
		return ShaderModule();
	}

	asset.m_path_length = path_length;
	asset.m_ref_count = 1;
	asset.m_path = new char[asset.m_path_length + 1];
	strncpy(asset.m_path, path, asset.m_path_length);
	asset.m_path[asset.m_path_length] = '\0';
	m_shaders.push_back(asset);
	return asset.m_asset;
}

void Assets::UnloadShader(ShaderModule shader)
{
	if(shader.m_shader_module)
	{
		Shaders::iterator it = std::find_if(m_shaders.begin(), m_shaders.end(), [shader](const Asset<ShaderModule>& item) {
			return item.m_asset.m_shader_module == shader.m_shader_module;
		});
		if(it != m_shaders.end())
		{
			if(--it->m_ref_count > 0) return;

			delete[] it->m_path;
			m_shaders.erase(it);
		}

		Game::GetInstance()->GetVulkan()->GetDevice().destroyShaderModule(shader.m_shader_module, nullptr);
	}
}

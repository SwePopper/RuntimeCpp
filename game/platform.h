#ifndef PLATFORM_H
#define PLATFORM_H

#if !defined(BUILD_DEBUG) && !defined(BUILD_RELEASE)
#define BUILD_DEBUG
#endif

// Defines
#define ARRAY_COUNT(array) (sizeof(array) / sizeof(array[0]))
#define STATIC_ASSERT(exp) static_assert(exp, __FILE__ ": " #exp)
#define CHECK_BIT(var, pos) ((var) & (1 << (pos)))

#ifndef NOMINMAX
#define NOMINMAX
#endif

#ifndef UNUSED
#define UNUSED(var) (void)var
#endif

#define COMPONENT_DATA(Type)              \
public:                                   \
	static constexpr char name[] = #Type; \
	static constexpr uint32_t id = Hash32(#Type)

// C/C++ headers
#include <cstdint>
#include <string_view>

// System defines and headers
#ifdef __linux

#define PLATFORM_LINUX

#elif defined(WIN32) || defined(WIN64)

#define PLATFORM_WINDOWS

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

#endif

// 3rd party headers
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

// Project headers
#include "allocator.h"
#include "log.h"
#include "io/file.h"

// http://isthe.com/chongo/tech/comp/fnv/#FNV-param
constexpr std::uint32_t kHash32Prime = 16777619u;
constexpr std::uint32_t kHash32Offset = 2166136261u;
constexpr std::uint32_t Hash32(const char* s, std::uint32_t last_hash = kHash32Offset)
{
	return *s ? Hash32(s + 1, static_cast<std::uint32_t>((static_cast<std::uint32_t>(*s) ^ last_hash) * static_cast<std::uint64_t>(kHash32Prime)))
			  : last_hash;
}

constexpr float kPI = 3.14159265359f;
constexpr double kPId = 3.14159265358979323846;

constexpr float k2PI = kPI * 2.0f;
constexpr double k2PId = kPId * 2.0;

constexpr float kRadToDeg = 180.0f / kPI;
constexpr double kRadToDegd = 180.0 / kPId;

constexpr float kDegToRad = kPI / 180.0f;
constexpr double kDegToRadd = kPId / 180.0;

template <typename T>
const T& Clamp(const T& value, const T& min, const T& max)
{
	return value <= min ? min : value >= max ? max : value;
}

#endif

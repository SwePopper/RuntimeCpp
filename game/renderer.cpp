#include "platform.h"
#include "renderer.h"
#include "game.h"
#include "vulkan/vulkan_renderer.h"
#include "vulkan/mesh.h"
#include "assets.h"
#include "math/frustum.h"
#include "entity/components/camera.h"
#include "entity/components/model.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

Renderer::Renderer() {}

bool Renderer::Initialize()
{
	m_vertex_desc.resize(1);
	m_vertex_desc[0].binding = 0;
	m_vertex_desc[0].stride = sizeof(Vertex);
	m_vertex_desc[0].inputRate = vk::VertexInputRate::eVertex;

	m_vertex_attributes.resize(5);
	m_vertex_attributes[0].binding = 0;
	m_vertex_attributes[0].location = 0;
	m_vertex_attributes[0].format = vk::Format::eR32G32B32Sfloat;
	m_vertex_attributes[0].offset = offsetof(Vertex, m_position);

	m_vertex_attributes[1].binding = 0;
	m_vertex_attributes[1].location = 1;
	m_vertex_attributes[1].format = vk::Format::eR32G32Sfloat;
	m_vertex_attributes[1].offset = offsetof(Vertex, m_uv);

	m_vertex_attributes[2].binding = 0;
	m_vertex_attributes[2].location = 2;
	m_vertex_attributes[2].format = vk::Format::eR32G32B32Sfloat;
	m_vertex_attributes[2].offset = offsetof(Vertex, m_color);

	m_vertex_attributes[3].binding = 0;
	m_vertex_attributes[3].location = 3;
	m_vertex_attributes[3].format = vk::Format::eR32G32B32Sfloat;
	m_vertex_attributes[3].offset = offsetof(Vertex, m_normal);

	m_vertex_attributes[4].binding = 0;
	m_vertex_attributes[4].location = 4;
	m_vertex_attributes[4].format = vk::Format::eR32G32B32Sfloat;
	m_vertex_attributes[4].offset = offsetof(Vertex, m_tangent);

	m_vertex_input_state.pVertexBindingDescriptions = m_vertex_desc.data();
	m_vertex_input_state.vertexBindingDescriptionCount = static_cast<uint32_t>(m_vertex_desc.size());
	m_vertex_input_state.pVertexAttributeDescriptions = m_vertex_attributes.data();
	m_vertex_input_state.vertexAttributeDescriptionCount = static_cast<uint32_t>(m_vertex_attributes.size());

	if(!CreateMultiRenderTargets())
	{
		LogError("Failed to create multirendertargets!\n");
		return false;
	}

	if(!CreatePipelineLayouts())
	{
		LogError("Failed to create pipeline layouts!\n");
		return false;
	}

	if(!CreatePipelines())
	{
		LogError("Failed to create pipelines!\n");
		return false;
	}

	if(!CreateUniformBuffers())
	{
		LogError("Failed to create deferred uniforms\n");
		return false;
	}

	if(!CreateDescriptorSet())
	{
		LogError("Failed to create descriptor set\n");
		return false;
	}

	return true;
}

void Renderer::Release()
{
	vk::Device& device = Game::GetInstance()->GetVulkan()->GetDevice();

	if(m_debug.m_pipeline)
	{
		device.destroyPipeline(m_debug.m_pipeline, nullptr);
		m_debug.m_pipeline = nullptr;
	}

	if(m_debug.m_layout)
	{
		device.destroyPipelineLayout(m_debug.m_layout, nullptr);
		m_debug.m_layout = nullptr;
	}

	if(m_debug.m_descriptor_layout)
	{
		device.destroyDescriptorSetLayout(m_debug.m_descriptor_layout, nullptr);
		m_debug.m_descriptor_layout = nullptr;
	}

	if(m_deferred.m_pipeline)
	{
		device.destroyPipeline(m_deferred.m_pipeline, nullptr);
		m_deferred.m_pipeline = nullptr;
	}

	if(m_deferred.m_layout)
	{
		device.destroyPipelineLayout(m_deferred.m_layout, nullptr);
		m_deferred.m_layout = nullptr;
	}

	if(m_deferred.m_descriptor_layout)
	{
		device.destroyDescriptorSetLayout(m_deferred.m_descriptor_layout, nullptr);
		m_deferred.m_descriptor_layout = nullptr;
	}

	if(m_multirendertarget.m_pipeline)
	{
		device.destroyPipeline(m_multirendertarget.m_pipeline, nullptr);
		m_multirendertarget.m_pipeline = nullptr;
	}

	if(m_multirendertarget.m_layout)
	{
		device.destroyPipelineLayout(m_multirendertarget.m_layout, nullptr);
		m_multirendertarget.m_layout = nullptr;
	}

	if(m_multirendertarget.m_descriptor_layout)
	{
		device.destroyDescriptorSetLayout(m_multirendertarget.m_descriptor_layout, nullptr);
		m_multirendertarget.m_descriptor_layout = nullptr;
	}

	if(m_multirendertarget_sampler)
	{
		device.destroySampler(m_multirendertarget_sampler, nullptr);
		m_multirendertarget_sampler = nullptr;
	}

	if(m_multirendertarget_pass)
	{
		device.destroyRenderPass(m_multirendertarget_pass);
		m_multirendertarget_pass = nullptr;
	}

	if(m_multirendertarget_frame)
	{
		device.destroyFramebuffer(m_multirendertarget_frame);
		m_multirendertarget_frame = nullptr;
	}

	for(uint32_t i = 0; i < static_cast<uint32_t>(Attachments::kCount); ++i)
	{
		if(m_multirendertarget_attachments[i].m_view)
		{
			device.destroyImageView(m_multirendertarget_attachments[i].m_view);
			m_multirendertarget_attachments[i].m_view = nullptr;
		}

		if(m_multirendertarget_attachments[i].m_image)
		{
			device.destroyImage(m_multirendertarget_attachments[i].m_image);
			m_multirendertarget_attachments[i].m_image = nullptr;
		}

		if(m_multirendertarget_attachments[i].m_memory)
		{
			device.freeMemory(m_multirendertarget_attachments[i].m_memory);
			m_multirendertarget_attachments[i].m_memory = nullptr;
		}
	}
}

void Renderer::Draw()
{
	if(!m_camera) return;

	Vulkan* vulkan = Game::GetInstance()->GetVulkan();
	vk::CommandBuffer& command_buffer = vulkan->GetCurrentCommandBuffer();

	// Begin render pass
	{
		std::array<vk::ClearValue, static_cast<uint32_t>(Attachments::kCount)> clear_values;
		clear_values[0].color.setFloat32({{0.0f, 0.0f, 0.0f, 0.0f}});
		clear_values[1].color.setFloat32({{0.0f, 0.0f, 0.0f, 0.0f}});
		clear_values[2].color.setFloat32({{0.0f, 0.0f, 0.0f, 0.0f}});
		clear_values[3].depthStencil.depth = 1.0f;
		clear_values[3].depthStencil.stencil = 0.0f;

		vk::RenderPassBeginInfo render_pass_begin_info;
		render_pass_begin_info.renderPass = m_multirendertarget_pass;
		render_pass_begin_info.framebuffer = m_multirendertarget_frame;
		render_pass_begin_info.renderArea.offset.x = 0;
		render_pass_begin_info.renderArea.offset.y = 0;
		render_pass_begin_info.renderArea.extent = m_multirendertarget_size;
		render_pass_begin_info.clearValueCount = clear_values.size();
		render_pass_begin_info.pClearValues = clear_values.data();

		command_buffer.beginRenderPass(&render_pass_begin_info, vk::SubpassContents::eInline);
	}

	// Setup viewport and scissors
	{
		vk::Viewport viewport;
		viewport.width = m_multirendertarget_size.width;
		viewport.height = m_multirendertarget_size.height;
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;
		command_buffer.setViewport(0, 1, &viewport);

		vk::Rect2D scissor(vk::Offset2D(0, 0), vk::Extent2D(static_cast<uint32_t>(viewport.width), static_cast<uint32_t>(viewport.height)));
		command_buffer.setScissor(0, 1, &scissor);
	}

	// Bind pipeline:
	{
		command_buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, m_multirendertarget.m_pipeline);
	}

	Frustum frustum(m_camera->GetView() * m_camera->GetProjection());

	Models drawlist;
	drawlist.reserve(m_models.size());

	// cull meshes
	for(Models::iterator model = m_models.begin(); model != m_models.end(); ++model)
	{
		const ModelData& model_data = (*model)->GetModelData();
		for(ModelData::Meshes::const_iterator mesh = model_data.m_meshes.begin(); mesh != model_data.m_meshes.end(); ++mesh)
		{
			// if(frustum.IsInside(mesh->m_bounds))
			{
				drawlist.push_back(*model);
				break;
			}
		}
	}

	UpdateUniformBuffers();

	// draw depth?
	// draw shadow maps (later...)

	// draw opaque meshes
	{
		for(Models::iterator model = drawlist.begin(); model != drawlist.end(); ++model)
		{
			// bind descriptor sets:
			{
				command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_multirendertarget.m_layout, 0, 1, (*model)->GetDescriptoSet(),
												  0, nullptr);
			}

			const ModelData& model_data = (*model)->GetModelData();
			for(ModelData::Meshes::const_iterator mesh = model_data.m_meshes.begin(); mesh != model_data.m_meshes.end(); ++mesh)
			{
				const vk::Buffer vertex_buffers[1]{mesh->m_vertex_buffer.m_buffer};
				const vk::DeviceSize vertex_offset[1] = {0};
				command_buffer.bindVertexBuffers(0, 1, vertex_buffers, vertex_offset);
				command_buffer.bindIndexBuffer(mesh->m_index_buffer.m_buffer, 0, static_cast<vk::IndexType>(mesh->m_index_type));

				command_buffer.drawIndexed(mesh->m_index_count, 1, 0, 0, 0);
			}
		}
	}

	// End deferred generation
	command_buffer.endRenderPass();

	// draw the rest (custom shader and transparent stuff)

	// draw multirendertarget on to the frame
	{
		std::array<vk::ClearValue, 2> clear_value;
		clear_value[0].color.setFloat32({{1.0f, 1.0f, 1.0f, 0.0f}});
		clear_value[1].depthStencil.depth = 1.0f;
		clear_value[1].depthStencil.stencil = 0;

		vk::RenderPassBeginInfo renderpass_begin;
		renderpass_begin.renderPass = vulkan->GetRenderPass();
		renderpass_begin.framebuffer = vulkan->GetCurrentFramebuffer();
		renderpass_begin.renderArea.extent = vulkan->GetSwapchainExtent();
		renderpass_begin.clearValueCount = clear_value.size();
		renderpass_begin.pClearValues = clear_value.data();

		command_buffer.beginRenderPass(&renderpass_begin, vk::SubpassContents::eInline);

		// viewport and scissor
		vk::Viewport viewport;
		{
			viewport.width = vulkan->GetSwapchainExtent().width;
			viewport.height = vulkan->GetSwapchainExtent().height;
			viewport.minDepth = 0.0f;
			viewport.maxDepth = 1.0f;
			command_buffer.setViewport(0, 1, &viewport);

			vk::Rect2D scissor(vk::Offset2D(0, 0), vk::Extent2D(static_cast<uint32_t>(viewport.width), static_cast<uint32_t>(viewport.height)));
			command_buffer.setScissor(0, 1, &scissor);
		}

		command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_deferred.m_layout, 0, 1, &m_deferred_descriptor_set, 0, nullptr);

		// if(debugDisplay)
		{
			command_buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, m_debug.m_pipeline);
			command_buffer.draw(3, 1, 0, 0);

			// Move viewport to display final composition in lower right corner
			viewport.x = viewport.width * 0.5f;
			viewport.y = viewport.height * 0.5f;
			viewport.width = viewport.width * 0.5f;
			viewport.height = viewport.height * 0.5f;
			command_buffer.setViewport(0, 1, &viewport);
		}

		command_buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, m_deferred.m_pipeline);

		command_buffer.draw(3, 1, 0, 0);
	}

	// draw gui

	// end drawing
	command_buffer.endRenderPass();
}

void Renderer::UpdateUniformBuffers()
{
	// White
	m_light_buffer.lights[0].position = glm::vec4(0.0f, 0.0f, 1.0f, 0.0f);
	m_light_buffer.lights[0].color = glm::vec3(1.5f);
	m_light_buffer.lights[0].radius = 15.0f * 0.25f;
	// Red
	m_light_buffer.lights[1].position = glm::vec4(-2.0f, 0.0f, 0.0f, 0.0f);
	m_light_buffer.lights[1].color = glm::vec3(1.0f, 0.0f, 0.0f);
	m_light_buffer.lights[1].radius = 15.0f;
	// Blue
	m_light_buffer.lights[2].position = glm::vec4(2.0f, 1.0f, 0.0f, 0.0f);
	m_light_buffer.lights[2].color = glm::vec3(0.0f, 0.0f, 2.5f);
	m_light_buffer.lights[2].radius = 5.0f;
	// Yellow
	m_light_buffer.lights[3].position = glm::vec4(0.0f, 0.9f, 0.5f, 0.0f);
	m_light_buffer.lights[3].color = glm::vec3(1.0f, 1.0f, 0.0f);
	m_light_buffer.lights[3].radius = 2.0f;
	// Green
	m_light_buffer.lights[4].position = glm::vec4(0.0f, 0.5f, 0.0f, 0.0f);
	m_light_buffer.lights[4].color = glm::vec3(0.0f, 1.0f, 0.2f);
	m_light_buffer.lights[4].radius = 5.0f;
	// Yellow
	m_light_buffer.lights[5].position = glm::vec4(0.0f, 1.0f, 0.0f, 0.0f);
	m_light_buffer.lights[5].color = glm::vec3(1.0f, 0.7f, 0.3f);
	m_light_buffer.lights[5].radius = 25.0f;

	static float timer = 0.0f;
	timer += 0.0166f;

	constexpr float half_pi = kPI * 0.5f;
	constexpr float quarter_pi = kPI * 0.25f;

	/*m_light_buffer.lights[0].position.x = sin(k2PI * timer) * 5.0f;
	m_light_buffer.lights[0].position.z = cos(k2PI * timer) * 5.0f;

	m_light_buffer.lights[1].position.x = -4.0f + sin(k2PI * timer + quarter_pi) * 2.0f;
	m_light_buffer.lights[1].position.z = 0.0f + cos(k2PI * timer + quarter_pi) * 2.0f;

	m_light_buffer.lights[2].position.x = 4.0f + sin(k2PI * timer) * 2.0f;
	m_light_buffer.lights[2].position.z = 0.0f + cos(k2PI * timer) * 2.0f;

	m_light_buffer.lights[4].position.x = 0.0f + sin(k2PI * timer + half_pi) * 5.0f;
	m_light_buffer.lights[4].position.z = 0.0f - cos(k2PI * timer + quarter_pi) * 5.0f;

	m_light_buffer.lights[5].position.x = 0.0f + sin(-k2PI * timer + (quarter_pi + half_pi)) * 10.0f;
	m_light_buffer.lights[5].position.z = 0.0f - cos(-k2PI * timer - quarter_pi) * 10.0f;*/

	// Current view position
	m_light_buffer.viewPos = m_camera->GetView()[3] * glm::vec4(-1.0f, 1.0f, -1.0f, 1.0f);

	if(!m_lights_buffer.m_mapped_address)
	{
		if(!m_lights_buffer.Map()) return;
	}

	memcpy(m_lights_buffer.m_mapped_address, &m_light_buffer, sizeof(m_light_buffer));

	m_matrix_buffer.m_projection = m_camera->GetProjection();
	m_matrix_buffer.m_view = m_camera->GetView();
	m_matrix_buffer.m_model = glm::mat4(1.0f);
	m_matrix_buffer.m_view_model = m_matrix_buffer.m_view * m_matrix_buffer.m_model;

	if(!m_deferred_buffer.m_mapped_address)
	{
		if(!m_deferred_buffer.Map()) return;
	}

	memcpy(m_deferred_buffer.m_mapped_address, &m_matrix_buffer, sizeof(m_matrix_buffer));
}

void Renderer::AddModel(Model* model)
{
	RemoveModel(model);
	if(model) m_models.push_back(model);
}

void Renderer::RemoveModel(Model* model)
{
	if(model && !m_models.empty()) m_models.erase(std::remove(m_models.begin(), m_models.end(), model), m_models.end());
}

bool Renderer::CreateMultiRenderTargets()
{
	Vulkan* vulkan = Game::GetInstance()->GetVulkan();
	m_multirendertarget_size = vulkan->GetSwapchainExtent();

	if(!CreateAttachment(m_multirendertarget_attachments[static_cast<uint32_t>(Attachments::kPosition)], vk::Format::eR16G16B16A16Sfloat,
						 vk::ImageUsageFlagBits::eColorAttachment))
	{
		LogError("Failed to create position multirendertarget attachment!\n");
		return false;
	}

	if(!CreateAttachment(m_multirendertarget_attachments[static_cast<uint32_t>(Attachments::kNormal)], vk::Format::eR16G16B16A16Sfloat,
						 vk::ImageUsageFlagBits::eColorAttachment))
	{
		LogError("Failed to create normal multirendertarget attachment!\n");
		return false;
	}

	if(!CreateAttachment(m_multirendertarget_attachments[static_cast<uint32_t>(Attachments::kAlbedo)], vk::Format::eR8G8B8A8Unorm,
						 vk::ImageUsageFlagBits::eColorAttachment))
	{
		LogError("Failed to create color multirendertarget attachment!\n");
		return false;
	}

	if(!CreateAttachment(m_multirendertarget_attachments[static_cast<uint32_t>(Attachments::kDepth)], vulkan->GetDepthFormat(),
						 vk::ImageUsageFlagBits::eDepthStencilAttachment))
	{
		LogError("Failed to create depth multirendertarget attachment!\n");
		return false;
	}

	std::array<vk::AttachmentDescription, static_cast<uint32_t>(Attachments::kCount)> attachment_descs;
	for(uint32_t i = 0; i < attachment_descs.size(); ++i)
	{
		attachment_descs[i].samples = vk::SampleCountFlagBits::e8;
		attachment_descs[i].loadOp = vk::AttachmentLoadOp::eClear;
		attachment_descs[i].storeOp = vk::AttachmentStoreOp::eStore;
		attachment_descs[i].stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
		attachment_descs[i].stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
		attachment_descs[i].initialLayout = vk::ImageLayout::eUndefined;
		if(i == static_cast<uint32_t>(Attachments::kDepth))
		{
			attachment_descs[i].finalLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal;
		}
		else
		{
			attachment_descs[i].finalLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
		}
	}

	attachment_descs[static_cast<uint32_t>(Attachments::kPosition)].format =
		m_multirendertarget_attachments[static_cast<uint32_t>(Attachments::kPosition)].m_format;
	attachment_descs[static_cast<uint32_t>(Attachments::kNormal)].format =
		m_multirendertarget_attachments[static_cast<uint32_t>(Attachments::kNormal)].m_format;
	attachment_descs[static_cast<uint32_t>(Attachments::kAlbedo)].format =
		m_multirendertarget_attachments[static_cast<uint32_t>(Attachments::kAlbedo)].m_format;
	attachment_descs[static_cast<uint32_t>(Attachments::kDepth)].format =
		m_multirendertarget_attachments[static_cast<uint32_t>(Attachments::kDepth)].m_format;

	std::array<vk::AttachmentReference, static_cast<uint32_t>(Attachments::kCount)> references;
	references[static_cast<uint32_t>(Attachments::kPosition)].attachment = static_cast<uint32_t>(Attachments::kPosition);
	references[static_cast<uint32_t>(Attachments::kPosition)].layout = vk::ImageLayout::eColorAttachmentOptimal;
	references[static_cast<uint32_t>(Attachments::kNormal)].attachment = static_cast<uint32_t>(Attachments::kNormal);
	references[static_cast<uint32_t>(Attachments::kNormal)].layout = vk::ImageLayout::eColorAttachmentOptimal;
	references[static_cast<uint32_t>(Attachments::kAlbedo)].attachment = static_cast<uint32_t>(Attachments::kAlbedo);
	references[static_cast<uint32_t>(Attachments::kAlbedo)].layout = vk::ImageLayout::eColorAttachmentOptimal;
	references[static_cast<uint32_t>(Attachments::kDepth)].attachment = static_cast<uint32_t>(Attachments::kDepth);
	references[static_cast<uint32_t>(Attachments::kDepth)].layout = vk::ImageLayout::eDepthStencilAttachmentOptimal;

	vk::SubpassDescription subpass;
	subpass.pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
	subpass.pColorAttachments = references.data();
	subpass.colorAttachmentCount = 3;
	subpass.pDepthStencilAttachment = &references[static_cast<uint32_t>(Attachments::kDepth)];

	std::array<vk::SubpassDependency, 2> dependencies;

	dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
	dependencies[0].dstSubpass = 0;
	dependencies[0].srcStageMask = vk::PipelineStageFlagBits::eBottomOfPipe;
	dependencies[0].dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	dependencies[0].srcAccessMask = vk::AccessFlagBits::eMemoryRead;
	dependencies[0].dstAccessMask = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;
	dependencies[0].dependencyFlags = vk::DependencyFlagBits::eByRegion;

	dependencies[1].srcSubpass = 0;
	dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
	dependencies[1].srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	dependencies[1].dstStageMask = vk::PipelineStageFlagBits::eBottomOfPipe;
	dependencies[1].srcAccessMask = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;
	dependencies[1].dstAccessMask = vk::AccessFlagBits::eMemoryRead;
	dependencies[1].dependencyFlags = vk::DependencyFlagBits::eByRegion;

	vk::RenderPassCreateInfo render_pass_info;

	render_pass_info.pAttachments = attachment_descs.data();
	render_pass_info.attachmentCount = attachment_descs.size();
	render_pass_info.subpassCount = 1;
	render_pass_info.pSubpasses = &subpass;
	render_pass_info.pDependencies = dependencies.data();
	render_pass_info.dependencyCount = dependencies.size();

	vk::Device& device = vulkan->GetDevice();
	vk::Result result = device.createRenderPass(&render_pass_info, nullptr, &m_multirendertarget_pass);
	if(vk::Result::eSuccess != result)
	{
		LogError("createRenderPass: %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}

	std::array<vk::ImageView, static_cast<uint32_t>(Attachments::kCount)> attachments;
	for(uint32_t i = 0; i < attachments.size(); ++i)
	{
		attachments[i] = m_multirendertarget_attachments[i].m_view;
	}

	vk::FramebufferCreateInfo frame_buffer;
	frame_buffer.renderPass = m_multirendertarget_pass;
	frame_buffer.pAttachments = attachments.data();
	frame_buffer.attachmentCount = attachments.size();
	frame_buffer.width = m_multirendertarget_size.width;
	frame_buffer.height = m_multirendertarget_size.height;
	frame_buffer.layers = 1;

	result = device.createFramebuffer(&frame_buffer, nullptr, &m_multirendertarget_frame);
	if(vk::Result::eSuccess != result)
	{
		LogError("createFrameBuffer: %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}

	vk::SamplerCreateInfo sampler;
	sampler.maxAnisotropy = 1.0f;
	sampler.magFilter = vk::Filter::eNearest;
	sampler.minFilter = vk::Filter::eNearest;
	sampler.mipmapMode = vk::SamplerMipmapMode::eLinear;
	sampler.addressModeU = vk::SamplerAddressMode::eClampToEdge;
	sampler.addressModeV = sampler.addressModeU;
	sampler.addressModeW = sampler.addressModeU;
	sampler.mipLodBias = 0.0f;
	sampler.maxAnisotropy = 1.0f;
	sampler.minLod = 0.0f;
	sampler.maxLod = 1.0f;
	sampler.borderColor = vk::BorderColor::eFloatOpaqueWhite;

	result = device.createSampler(&sampler, nullptr, &m_multirendertarget_sampler);
	if(vk::Result::eSuccess != result)
	{
		LogError("createSampler: %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}

	return true;
}

bool Renderer::CreateAttachment(FrameBufferAttachment& attachment, vk::Format format, vk::ImageUsageFlags usage)
{
	attachment.m_format = format;

	vk::ImageAspectFlags aspect_mask;
	vk::ImageLayout image_layout;

	if(usage & vk::ImageUsageFlagBits::eColorAttachment)
	{
		aspect_mask = vk::ImageAspectFlagBits::eColor;
		image_layout = vk::ImageLayout::eColorAttachmentOptimal;
	}

	if(usage & vk::ImageUsageFlagBits::eDepthStencilAttachment)
	{
		aspect_mask = vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil;
		image_layout = vk::ImageLayout::eDepthStencilAttachmentOptimal;
	}

	vk::ImageCreateInfo image;
	image.imageType = vk::ImageType::e2D;
	image.format = format;
	image.extent.width = m_multirendertarget_size.width;
	image.extent.height = m_multirendertarget_size.height;
	image.extent.depth = 1;
	image.mipLevels = 1;
	image.arrayLayers = 1;
	image.samples = vk::SampleCountFlagBits::e8;
	image.tiling = vk::ImageTiling::eOptimal;
	image.usage = usage | vk::ImageUsageFlagBits::eSampled;

	vk::Device& device = Game::GetInstance()->GetVulkan()->GetDevice();
	vk::Result result = device.createImage(&image, nullptr, &attachment.m_image);
	if(vk::Result::eSuccess != result)
	{
		LogError("createImage: %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}

	vk::MemoryRequirements requirements = device.getImageMemoryRequirements(attachment.m_image);

	vk::MemoryAllocateInfo memory_alloc;
	memory_alloc.allocationSize = requirements.size;
	memory_alloc.memoryTypeIndex =
		Game::GetInstance()->GetVulkan()->GetMemoryType(requirements.memoryTypeBits, vk::MemoryPropertyFlagBits::eDeviceLocal);
	result = device.allocateMemory(&memory_alloc, nullptr, &attachment.m_memory);
	if(vk::Result::eSuccess != result)
	{
		LogError("allocateMemory: %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}

	device.bindImageMemory(attachment.m_image, attachment.m_memory, 0);

	vk::ImageViewCreateInfo image_view;
	image_view.viewType = vk::ImageViewType::e2D;
	image_view.format = format;
	image_view.subresourceRange.aspectMask = aspect_mask;
	image_view.subresourceRange.baseMipLevel = 0;
	image_view.subresourceRange.levelCount = 1;
	image_view.subresourceRange.baseArrayLayer = 0;
	image_view.subresourceRange.layerCount = 1;
	image_view.image = attachment.m_image;

	result = device.createImageView(&image_view, nullptr, &attachment.m_view);
	if(vk::Result::eSuccess != result)
	{
		LogError("createImageView: %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}

	return true;
}

bool Renderer::CreatePipelineLayouts()
{
	// Deferred shading layout
	std::vector<vk::DescriptorSetLayoutBinding> set_layout_bindings;
	set_layout_bindings.resize(5);

	set_layout_bindings[0].descriptorType = vk::DescriptorType::eUniformBuffer;
	set_layout_bindings[0].stageFlags = vk::ShaderStageFlagBits::eVertex;
	set_layout_bindings[0].binding = 0;
	set_layout_bindings[0].descriptorCount = 1;

	set_layout_bindings[1].descriptorType = vk::DescriptorType::eCombinedImageSampler;
	set_layout_bindings[1].stageFlags = vk::ShaderStageFlagBits::eFragment;
	set_layout_bindings[1].binding = 1;
	set_layout_bindings[1].descriptorCount = 1;

	set_layout_bindings[2].descriptorType = vk::DescriptorType::eCombinedImageSampler;
	set_layout_bindings[2].stageFlags = vk::ShaderStageFlagBits::eFragment;
	set_layout_bindings[2].binding = 2;
	set_layout_bindings[2].descriptorCount = 1;

	set_layout_bindings[3].descriptorType = vk::DescriptorType::eCombinedImageSampler;
	set_layout_bindings[3].stageFlags = vk::ShaderStageFlagBits::eFragment;
	set_layout_bindings[3].binding = 3;
	set_layout_bindings[3].descriptorCount = 1;

	set_layout_bindings[4].descriptorType = vk::DescriptorType::eUniformBuffer;
	set_layout_bindings[4].stageFlags = vk::ShaderStageFlagBits::eFragment;
	set_layout_bindings[4].binding = 4;
	set_layout_bindings[4].descriptorCount = 1;

	vk::DescriptorSetLayoutCreateInfo descriptor_layout;
	descriptor_layout.pBindings = set_layout_bindings.data();
	descriptor_layout.bindingCount = static_cast<uint32_t>(set_layout_bindings.size());

	vk::Device& device = Game::GetInstance()->GetVulkan()->GetDevice();
	vk::Result result = device.createDescriptorSetLayout(&descriptor_layout, nullptr, &m_deferred.m_descriptor_layout);
	if(vk::Result::eSuccess != result)
	{
		LogError("createDescriptorSetLayout: %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}

	vk::PipelineLayoutCreateInfo pipeline_layout_create_info;
	pipeline_layout_create_info.pSetLayouts = &m_deferred.m_descriptor_layout;
	pipeline_layout_create_info.setLayoutCount = 1;

	result = device.createPipelineLayout(&pipeline_layout_create_info, nullptr, &m_deferred.m_layout);
	if(vk::Result::eSuccess != result)
	{
		LogError("createPipelineLayout: %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}

	descriptor_layout.bindingCount = 3;
	result = device.createDescriptorSetLayout(&descriptor_layout, nullptr, &m_multirendertarget.m_descriptor_layout);
	if(vk::Result::eSuccess != result)
	{
		LogError("createDescriptorSetLayout: %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}

	pipeline_layout_create_info.pSetLayouts = &m_multirendertarget.m_descriptor_layout;
	result = device.createPipelineLayout(&pipeline_layout_create_info, nullptr, &m_multirendertarget.m_layout);
	if(vk::Result::eSuccess != result)
	{
		LogError("createPipelineLayout: %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}

	return true;
}

bool Renderer::CreatePipelines()
{
	vk::PipelineInputAssemblyStateCreateInfo input_assembly_state;
	input_assembly_state.topology = vk::PrimitiveTopology::eTriangleList;
	input_assembly_state.primitiveRestartEnable = false;

	vk::PipelineRasterizationStateCreateInfo rasterization_state;
	rasterization_state.polygonMode = vk::PolygonMode::eFill;
	rasterization_state.cullMode = vk::CullModeFlagBits::eBack;
	rasterization_state.frontFace = vk::FrontFace::eClockwise;
	rasterization_state.lineWidth = 1.0f;

	vk::PipelineColorBlendAttachmentState blend_attachment_state;
	blend_attachment_state.colorWriteMask = static_cast<vk::ColorComponentFlagBits>(0xf);
	blend_attachment_state.blendEnable = false;

	vk::PipelineColorBlendStateCreateInfo color_blend_state;
	color_blend_state.attachmentCount = 1;
	color_blend_state.pAttachments = &blend_attachment_state;

	vk::PipelineDepthStencilStateCreateInfo depth_stencil_state;
	depth_stencil_state.depthTestEnable = true;
	depth_stencil_state.depthWriteEnable = true;
	depth_stencil_state.depthCompareOp = vk::CompareOp::eLessOrEqual;

	vk::PipelineViewportStateCreateInfo viewport_state;
	viewport_state.viewportCount = 1;
	viewport_state.scissorCount = 1;

	vk::PipelineMultisampleStateCreateInfo multisample_state;
	multisample_state.rasterizationSamples = vk::SampleCountFlagBits::e1;

	std::array<vk::DynamicState, 2> dynamic_state_enables = {{vk::DynamicState::eViewport, vk::DynamicState::eScissor}};

	vk::PipelineDynamicStateCreateInfo dynamic_state;
	dynamic_state.dynamicStateCount = dynamic_state_enables.size();
	dynamic_state.pDynamicStates = dynamic_state_enables.data();

	std::array<vk::PipelineShaderStageCreateInfo, 2> shader_stages;

	vk::GraphicsPipelineCreateInfo pipeline_create_info;
	pipeline_create_info.layout = m_deferred.m_layout;
	pipeline_create_info.renderPass = Game::GetInstance()->GetVulkan()->GetRenderPass();

	pipeline_create_info.pInputAssemblyState = &input_assembly_state;
	pipeline_create_info.pRasterizationState = &rasterization_state;
	pipeline_create_info.pColorBlendState = &color_blend_state;
	pipeline_create_info.pMultisampleState = &multisample_state;
	pipeline_create_info.pViewportState = &viewport_state;
	pipeline_create_info.pDepthStencilState = &depth_stencil_state;
	pipeline_create_info.pDynamicState = &dynamic_state;
	pipeline_create_info.stageCount = shader_stages.size();
	pipeline_create_info.pStages = shader_stages.data();

	vk::PipelineVertexInputStateCreateInfo empty_input_state;
	pipeline_create_info.pVertexInputState = &empty_input_state;
	pipeline_create_info.layout = m_deferred.m_layout;

	vk::SpecializationMapEntry specialization_entry;
	specialization_entry.constantID = 0;
	specialization_entry.offset = 0;
	specialization_entry.size = sizeof(uint32_t);

	uint32_t specialization_data = static_cast<uint32_t>(vk::SampleCountFlagBits::e8);

	vk::SpecializationInfo specialization_info;
	specialization_info.mapEntryCount = 1;
	specialization_info.pMapEntries = &specialization_entry;
	specialization_info.dataSize = sizeof(specialization_data);
	specialization_info.pData = &specialization_data;

	shader_stages[0].stage = vk::ShaderStageFlagBits::eVertex;
	shader_stages[0].module = Game::GetInstance()->GetAssets()->LoadShader("shaders/deferred.vert").m_shader_module;
	shader_stages[0].pName = "main";
	shader_stages[1].stage = vk::ShaderStageFlagBits::eFragment;
	shader_stages[1].module = Game::GetInstance()->GetAssets()->LoadShader("shaders/deferred.frag").m_shader_module;
	shader_stages[1].pName = "main";
	shader_stages[1].pSpecializationInfo = &specialization_info;

	if(!shader_stages[0].module || !shader_stages[1].module)
	{
		LogError("Failed to load deferred shader!\n");
		return false;
	}

	vk::Device& device = Game::GetInstance()->GetVulkan()->GetDevice();
	m_deferred.m_pipeline = device.createGraphicsPipeline(nullptr, pipeline_create_info, nullptr);
	if(!m_deferred.m_pipeline)
	{
		LogError("createGraphicsPipeline failed\n");
		return false;
	}

	// specialization_data = static_cast<uint32_t>(vk::SampleCountFlagBits::e1);

	shader_stages[0].module = Game::GetInstance()->GetAssets()->LoadShader("shaders/debug.vert").m_shader_module;
	shader_stages[1].module = Game::GetInstance()->GetAssets()->LoadShader("shaders/debug.frag").m_shader_module;
	if(!shader_stages[0].module || !shader_stages[1].module)
	{
		LogError("Failed to load debug shader!\n");
		return false;
	}

	m_debug.m_pipeline = device.createGraphicsPipeline(nullptr, pipeline_create_info, nullptr);
	if(!m_debug.m_pipeline)
	{
		LogError("createGraphicsPipeline failed\n");
		return false;
	}

	pipeline_create_info.pVertexInputState = &m_vertex_input_state;

	shader_stages[0].module = Game::GetInstance()->GetAssets()->LoadShader("shaders/multirendertarget.vert").m_shader_module;
	shader_stages[1].module = Game::GetInstance()->GetAssets()->LoadShader("shaders/multirendertarget.frag").m_shader_module;
	if(!shader_stages[0].module || !shader_stages[1].module)
	{
		LogError("Failed to load multirendertarget shader!\n");
		return false;
	}

	// rasterizationState.polygonMode = VK_POLYGON_MODE_LINE;
	// rasterizationState.lineWidth = 2.0f;
	multisample_state.rasterizationSamples = static_cast<vk::SampleCountFlagBits>(specialization_data);
	multisample_state.alphaToCoverageEnable = VK_TRUE;

	// Separate render pass
	pipeline_create_info.renderPass = m_multirendertarget_pass;

	// Separate layout
	pipeline_create_info.layout = m_multirendertarget.m_layout;

	// Blend attachment states required for all color attachments
	// This is important, as color write mask will otherwise be 0x0 and you
	// won't see anything rendered to the attachment
	std::array<vk::PipelineColorBlendAttachmentState, 3> blend_attachment_states;
	blend_attachment_states[0].colorWriteMask = static_cast<vk::ColorComponentFlagBits>(0xf);
	blend_attachment_states[0].blendEnable = false;
	blend_attachment_states[2] = blend_attachment_states[1] = blend_attachment_states[0];

	color_blend_state.attachmentCount = static_cast<uint32_t>(blend_attachment_states.size());
	color_blend_state.pAttachments = blend_attachment_states.data();

	multisample_state.sampleShadingEnable = true;
	multisample_state.minSampleShading = 0.25f;
	m_multirendertarget.m_pipeline = device.createGraphicsPipeline(nullptr, pipeline_create_info, nullptr);
	if(!m_deferred.m_pipeline)
	{
		LogError("createGraphicsPipeline failed\n");
		return false;
	}

	return true;
}

bool Renderer::CreateUniformBuffers()
{
	Vulkan* vulkan = Game::GetInstance()->GetVulkan();

	vk::Result result = vulkan->CreateBuffer(m_deferred_buffer, nullptr, sizeof(DeferredBuffer), vk::BufferUsageFlagBits::eUniformBuffer,
											 vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
	if(vk::Result::eSuccess != result)
	{
		LogError("Failed to create uniform buffer %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}

	result = vulkan->CreateBuffer(m_lights_buffer, nullptr, sizeof(DeferredLight), vk::BufferUsageFlagBits::eUniformBuffer,
								  vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
	if(vk::Result::eSuccess != result)
	{
		LogError("Failed to create uniform buffer %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}

	return true;
}

bool Renderer::CreateDescriptorSet()
{
	Vulkan* vulkan = Game::GetInstance()->GetVulkan();

	vk::DescriptorSetAllocateInfo deferred_set;
	deferred_set.descriptorPool = vulkan->GetDescriptorPool();
	deferred_set.descriptorSetCount = 1;
	deferred_set.pSetLayouts = &m_deferred.m_descriptor_layout;

	vk::Device& device = vulkan->GetDevice();
	vk::Result result = device.allocateDescriptorSets(&deferred_set, &m_deferred_descriptor_set);
	if(vk::Result::eSuccess != result)
	{
		LogError("allocateDescriptorSets: %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}

	std::array<vk::DescriptorImageInfo, 3> multirendertarget_textures;
	multirendertarget_textures[0].imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
	multirendertarget_textures[0].sampler = m_multirendertarget_sampler;
	multirendertarget_textures[2] = multirendertarget_textures[1] = multirendertarget_textures[0];

	multirendertarget_textures[0].imageView = m_multirendertarget_attachments[0].m_view;
	multirendertarget_textures[1].imageView = m_multirendertarget_attachments[1].m_view;
	multirendertarget_textures[2].imageView = m_multirendertarget_attachments[2].m_view;

	std::array<vk::WriteDescriptorSet, 5> write_descs;
	write_descs[4].dstSet = write_descs[3].dstSet = write_descs[2].dstSet = write_descs[1].dstSet = write_descs[0].dstSet = m_deferred_descriptor_set;
	write_descs[4].descriptorCount = write_descs[3].descriptorCount = write_descs[2].descriptorCount = write_descs[1].descriptorCount =
		write_descs[0].descriptorCount = 1;

	write_descs[0].descriptorType = vk::DescriptorType::eUniformBuffer;
	write_descs[0].dstBinding = 0;
	write_descs[0].pBufferInfo = &m_deferred_buffer.m_descriptor;

	write_descs[1].descriptorType = vk::DescriptorType::eCombinedImageSampler;
	write_descs[1].dstBinding = 1;
	write_descs[1].pImageInfo = &multirendertarget_textures[0];

	write_descs[2].descriptorType = vk::DescriptorType::eCombinedImageSampler;
	write_descs[2].dstBinding = 2;
	write_descs[2].pImageInfo = &multirendertarget_textures[1];

	write_descs[3].descriptorType = vk::DescriptorType::eCombinedImageSampler;
	write_descs[3].dstBinding = 3;
	write_descs[3].pImageInfo = &multirendertarget_textures[2];

	write_descs[4].descriptorType = vk::DescriptorType::eUniformBuffer;
	write_descs[4].dstBinding = 4;
	write_descs[4].pBufferInfo = &m_lights_buffer.m_descriptor;

	device.updateDescriptorSets(write_descs.size(), write_descs.data(), 0, nullptr);

	return true;
}

#include "platform.h"
#include "config.h"
#include "game.h"
#include "io/file.h"
#include "io/xml_reader.h"

#include <string>
#include <string.h>

namespace
{
const char* kConfigFileName = "config.xml";

int constexpr StringLength(const char* str) { return *str ? 1 + StringLength(str + 1) : 0; }
static_assert(StringLength("str") == 3, "");  // just to confirm that the constexpr is working
}

Config::DesktopSettings::DesktopSettings() : m_screen_width(1920), m_screen_height(1080), m_screen_index(1), m_screen_mode(ScreenMode::kWindowed) {}

Config::Config() {}

bool Config::Initialize()
{
	std::string config_path = Game::GetInstance()->GetAssetsFolder();
	config_path += kConfigFileName;

	if(!File::FileExists(config_path.c_str()))
	{
		LogInfo("Could not find config. Using default values");
		return true;
	}

	struct ParseData
	{
		Config* config;
	};

	ParseData data{this};

	bool config_load_results = XmlReader::ReadXML(
		config_path.c_str(), &data,
		[](void* user_data, const char* name, const char** atts) {
			ParseData* pd = reinterpret_cast<ParseData*>(user_data);
			if(strncmp(name, "Settings", 9) == 0)
			{
				for(std::uint32_t i = 0; atts[i]; i += 2)
				{
					if(strncmp(atts[i], "version", 8) == 0)
					{
						LogInfo("Loading config with version: %s", atts[i + 1]);
						if(strncmp(GAME_VERSION, atts[i + 1], StringLength(GAME_VERSION)))
						{
							LogInfo("Config version different %s  -->  %s\tAutomatic upgrade/downgrade when shutdown", atts[i + 1], GAME_VERSION);
							// TODO: Maybe keep a backup?
						}
					}
				}
			}
			else if(strncmp(name, "DesktopSettings", 16) == 0)
			{
				for(std::uint32_t i = 0; atts[i]; i += 2)
				{
					if(strncmp(atts[i], "screen_width", 13) == 0)
					{
						pd->config->m_desktop_settings.m_screen_width = static_cast<uint32_t>(strtol(atts[i + 1], nullptr, 0));
					}
					else if(strncmp(atts[i], "screen_height", 14) == 0)
					{
						pd->config->m_desktop_settings.m_screen_height = static_cast<uint32_t>(strtol(atts[i + 1], nullptr, 0));
					}
					else if(strncmp(atts[i], "screen_index", 13) == 0)
					{
						pd->config->m_desktop_settings.m_screen_index = static_cast<uint32_t>(strtol(atts[i + 1], nullptr, 0));
					}
					else if(strncmp(atts[i], "screen_mode", 12) == 0)
					{
						uint8_t screen_mode = static_cast<uint8_t>(strtol(atts[i + 1], nullptr, 0));
						if(screen_mode <= 2) pd->config->m_desktop_settings.m_screen_mode = static_cast<ScreenMode>(screen_mode);
					}
				}
			}
		},
		[](void* user_data, const char* name) { ParseData* pd = reinterpret_cast<ParseData*>(user_data); });

	if(!config_load_results)
	{
		LogInfo("Failed to load the config file. Using default");
		m_desktop_settings = DesktopSettings();
	}

	return true;
}

void Config::Release()
{
	std::string config_path = Game::GetInstance()->GetAssetsFolder();
	config_path += kConfigFileName;

	FILE* file = fopen(config_path.c_str(), "w");
	if(file == nullptr)
	{
		LogInfo("Failed to open file for writing: %s", config_path.c_str());
		return;
	}

	File::Write(file, "<Settings version=\"%s\">", GAME_VERSION);
	{
		File::Write(file, "<DesktopSettings screen_width=\"%u\" screen_height=\"%u\" screen_index=\"%u\" screen_mode=\"%u\"/>",
					m_desktop_settings.m_screen_width, m_desktop_settings.m_screen_height, m_desktop_settings.m_screen_index,
					static_cast<uint8_t>(m_desktop_settings.m_screen_mode));
	}
	File::Write(file, "</Settings>");
}

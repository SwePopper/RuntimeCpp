#include "platform.h"
#include "frustum.h"

Frustum::Frustum(const glm::mat4& mat, bool normalize_planes)
{
	// create non-normalized clipping planes
	m_planes[static_cast<uint8_t>(Plane::kLeft)] = mat[3] + mat[0];
	m_planes[static_cast<uint8_t>(Plane::kRight)] = mat[3] - mat[0];
	m_planes[static_cast<uint8_t>(Plane::kTop)] = mat[3] - mat[1];
	m_planes[static_cast<uint8_t>(Plane::kBottom)] = mat[3] + mat[1];
	m_planes[static_cast<uint8_t>(Plane::kNear)] = mat[3] + mat[2];
	m_planes[static_cast<uint8_t>(Plane::kFar)] = mat[3] - mat[2];

	// normalize the plane equations, if requested
	if(normalize_planes)
	{
		for(int i = 0; i < 6; i++)
		{
			float n = glm::length(glm::vec3(m_planes[i].x, m_planes[i].y, m_planes[i].z));
			m_planes[i] /= n;
		}
	}
}

bool Frustum::IsInside(const Bounds& bounds) const
{
	// Sphere
	for(uint32_t i = 0; i < 6; ++i)
	{
		if(Distance(bounds.m_center, i) <= -bounds.radius) return false;
	}

	// Box
	for(uint32_t i = 0; i < 6; ++i)
	{
		if(Distance(bounds.m_min, i) > 0) continue;
		if(Distance(glm::vec3(bounds.m_max.x, bounds.m_min.y, bounds.m_min.z), i) > 0) continue;
		if(Distance(glm::vec3(bounds.m_min.x, bounds.m_max.y, bounds.m_min.z), i) > 0) continue;
		if(Distance(glm::vec3(bounds.m_max.x, bounds.m_max.y, bounds.m_min.z), i) > 0) continue;
		if(Distance(glm::vec3(bounds.m_min.x, bounds.m_min.y, bounds.m_max.z), i) > 0) continue;
		if(Distance(glm::vec3(bounds.m_max.x, bounds.m_min.y, bounds.m_max.z), i) > 0) continue;
		if(Distance(glm::vec3(bounds.m_min.x, bounds.m_max.y, bounds.m_max.z), i) > 0) continue;
		if(Distance(bounds.m_max, i) > 0) continue;
		return false;
	}

	return true;
}

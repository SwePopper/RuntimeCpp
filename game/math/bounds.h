#ifndef BOUNDS_H
#define BOUNDS_H

#include <glm/glm.hpp>

struct Bounds
{
	glm::vec3 m_min;
	glm::vec3 m_max;
	glm::vec3 m_center;
	float radius;
};

#endif

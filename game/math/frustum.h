#ifndef FRUSTUM_H
#define FRUSTUM_H

#include <glm/glm.hpp>
#include "bounds.h"

// https://pastebin.com/1Wsk8LgU
class Frustum
{
public:
	// if extracted from projection matrix only, planes will be in eye-space
	// if extracted from view*projection, planes will be in world space
	// if extracted from model*view*projection planes will be in model space
	Frustum(const glm::mat4& mat, bool normalize_planes = true);

	bool IsInside(const Bounds& bounds) const;

private:
	enum class Plane : uint8_t
	{
		kFar,
		kNear,
		kRight,
		kLeft,
		kTop,
		kBottom
	};

	inline float Distance(const glm::vec3& point, const uint32_t& plane) const
	{
		return m_planes[plane].x * point.x + m_planes[plane].y * point.y + m_planes[plane].z * point.z + m_planes[plane].w;
	}

	glm::vec4 m_planes[6];
};

#endif

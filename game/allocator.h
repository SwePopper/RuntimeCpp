#ifndef ALLOCATOR_H
#define ALLOCATOR_H

#include <new>

class Allocator
{
public:
	using AllocateFunction = void* (*)(std::size_t size, std::size_t align, const char* file, int line);
	using DeallocateFunction = void (*)(void* mem);

	Allocator() : m_allocate_function(nullptr), m_deallocate_function(nullptr) { SetupDefaultAllocFunctions(); }

	void Initialize(AllocateFunction allocate_function, DeallocateFunction deallocate_function)
	{
		m_allocate_function = allocate_function;
		m_deallocate_function = deallocate_function;
	}

	void* Allocate(std::size_t size, std::size_t align, const char* file = nullptr, int line = 0)
	{
		return m_allocate_function(size, align, file, line);
	}

	void Deallocate(void* mem) { m_deallocate_function(mem); }

	template <typename T>
	T* NewArray(std::size_t count, const char* file = nullptr, int line = 0)
	{
		std::size_t size = (sizeof(T) * count) + sizeof(std::size_t);

		void* mem = Allocate(size, alignof(T), file, line);
		*((std::size_t*)mem) = count;

		T* data = (T*)((std::uintptr_t)mem + sizeof(std::size_t));
		for(std::size_t i = 0; i < count; ++i)
		{
			new(&data[i]) T;
		}
		return data;
	}

	template <typename T>
	void Delete(T* obj)
	{
		if(obj != nullptr)
		{
			obj->~T();
			Deallocate(obj);
		}
	}

	template <typename T>
	void DeleteArray(T* array)
	{
		if(array != nullptr)
		{
			void* mem = (void*)((std::uintptr_t)array - sizeof(std::size_t));
			std::size_t count = *(std::size_t*)mem;
			for(std::size_t i = 0; i < count; ++i)
			{
				array[i].~T();
			}

			Deallocate(mem);
			array = nullptr;
		}
	}

	static Allocator sMainAllocator;

private:
	void SetupDefaultAllocFunctions();

	AllocateFunction m_allocate_function;
	DeallocateFunction m_deallocate_function;
};

#define AllocateMem(size) Allocator::sMainAllocator.Allocate(size, 8, __FILE__, __LINE__)
#define AllocateMemAligned(size, align) Allocator::sMainAllocator.Allocate(size, align, __FILE__, __LINE__)
#define FreeMem(mem) Allocator::sMainAllocator.Deallocate(mem)

#define New(Type) new(Allocator::sMainAllocator.Allocate(sizeof(Type), alignof(Type), __FILE__, __LINE__))
#define NewArray(Type, Count) Allocator::sMainAllocator.NewArray<Type>(Count, __FILE__, __LINE__)

template <typename T>
void Delete(T* obj)
{
	Allocator::sMainAllocator.Delete(obj);
}

template <typename T>
void DeleteArray(T* array)
{
	Allocator::sMainAllocator.DeleteArray(array);
}

#endif

#include "platform.h"
#include "entity/components.h"
#include "entity/entity.h"
#include "scripts.h"
#include "sharedlibrary/sharedlibrary.h"
#include "game.h"
#include "vulkan/vulkan_renderer.h"
#include "io/xml_reader.h"

#include <cstdlib>
#include <cstring>
#include <memory>
#include <string>
#include <algorithm>

namespace
{
#ifdef PLATFORM_LINUX
const char* kLibraryName = "libgamescript";
#else
#ifdef BUILD_DEBUG
const char* kLibraryName = "libgamescriptd";
#else
const char* kLibraryName = "gamescript";
#endif
#endif
}

Scripts::Scripts()
	: m_shared_library(nullptr)
	, m_initialize(nullptr)
	, m_release(nullptr)
	, m_update(nullptr)
	, m_draw(nullptr)
	, m_serialize(nullptr)
	, m_deserialize(nullptr)
{
}

bool Scripts::Initialize()
{
	std::string library_path = Game::GetInstance()->GetRootFolder();
	if(!library_path.empty()) library_path += "/";
	library_path += kLibraryName;

	m_shared_library = SharedLibrary::Load(library_path.c_str());
	if(m_shared_library == nullptr)
	{
		LogError("Failed to load game scripts!");
		return false;
	}

	return InitializeLibrary();
}

void Scripts::Release()
{
	if(m_release) m_release();
	delete m_shared_library;
}

bool Scripts::InitializeLibrary()
{
	m_initialize = reinterpret_cast<InitializeFunction>(m_shared_library->GetAddress("Initialize"));
	m_release = reinterpret_cast<ReleaseFunction>(m_shared_library->GetAddress("Release"));
	m_update = reinterpret_cast<UpdateFunction>(m_shared_library->GetAddress("Update"));
	m_draw = reinterpret_cast<UpdateFunction>(m_shared_library->GetAddress("Draw"));
	m_serialize = reinterpret_cast<SerializeFunction>(m_shared_library->GetAddress("Serialize"));
	m_deserialize = reinterpret_cast<DeserializeFunction>(m_shared_library->GetAddress("Deserialize"));

	try
	{
		if(m_initialize)
		{
			m_initialize([](std::size_t size, std::size_t align, const char* file,
							int line) { return Allocator::sMainAllocator.Allocate(size, align, file, line); },
						 [](void* mem) { Allocator::sMainAllocator.Deallocate(mem); });
		}
		return true;
	}
	catch(const std::exception& ex)
	{
		printf("Failed to initialize game: %s\n", ex.what());
	}
	catch(const std::string& ex)
	{
		printf("Failed to initialize game: %s\n", ex.c_str());
	}
	catch(...)
	{
		printf("Failed to initialize game: Unknown error!\n");
	}
	return false;
}

void Scripts::Update(float dt)
{
	if(m_shared_library->ShouldReload())
	{
		std::string path = Game::GetInstance()->GetSavePath();
		path += "backup";

		Game::GetInstance()->GetVulkan()->FlushAllFrames();
		if(m_release)
		{
			if(m_serialize)
			{
				m_serialize(path.c_str());
			}

			m_release();
		}

		m_initialize = nullptr;
		m_release = nullptr;
		m_update = nullptr;
		m_draw = nullptr;
		m_serialize = nullptr;

		if(m_shared_library->Reload())
		{
			InitializeLibrary();
			if(m_deserialize) m_deserialize(path.c_str());
			if(File::FileExists(path.c_str()))
			{
				remove(path.c_str());
			}
		}
	}
	else if(m_update != nullptr)
	{
		m_update(dt);
	}
}

void Scripts::Draw(float dt)
{
	if(m_draw) m_draw(dt);
}

void Scripts::Deserialize() {}

#ifndef SCRIPTS_H
#define SCRIPTS_H

#include <cstdio>
#include <cstdint>

class SharedLibrary;

class Scripts
{
public:
	using AllocateFunction = void* (*)(std::size_t size, std::size_t align, const char* file, int line);
	using DeallocateFunction = void (*)(void* mem);
	using InitializeFunction = bool (*)(AllocateFunction allocate_function, DeallocateFunction deallocate_function);
	using ReleaseFunction = void (*)();
	using UpdateFunction = void (*)(float dt);
	using DrawFunction = void (*)(float dt);
	using SerializeFunction = void (*)(const char* file_path);
	using DeserializeFunction = void (*)(const char* file_path);

	Scripts();
	~Scripts() { Release(); }

	bool Initialize();
	void Release();

	void Update(float dt);
	void Draw(float dt);

private:
	bool InitializeLibrary();
	void Deserialize();

	SharedLibrary* m_shared_library;
	InitializeFunction m_initialize;
	ReleaseFunction m_release;
	UpdateFunction m_update;
	DrawFunction m_draw;
	SerializeFunction m_serialize;
	DeserializeFunction m_deserialize;
};

#endif

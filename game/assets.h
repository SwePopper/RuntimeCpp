#ifndef ASSET_CACHE_H
#define ASSET_CACHE_H

#include <vector>
#include "vulkan/mesh.h"
#include "vulkan/shader_module.h"

struct aiNode;
struct aiScene;

class Assets
{
public:
	Assets();
	~Assets() { Release(); }

	bool Initialize();
	void Release();

	Vulkan::Texture LoadTexture(const char* path);
	void UnloadTexture(Vulkan::Texture& texture);

	ModelData LoadModel(const char* path);
	void UnloadModel(ModelData model);

	ShaderModule LoadShader(const char* path);
	void UnloadShader(ShaderModule shader);

private:
	template <typename T>
	struct Asset
	{
		Asset() : m_path(nullptr), m_path_length(0), m_ref_count(0) {}

		char* m_path;
		uint32_t m_path_length;
		uint32_t m_ref_count;
		T m_asset;
	};

	using Textures = std::vector<Asset<Vulkan::Texture>>;
	using ModelDatas = std::vector<Asset<ModelData>>;
	using Shaders = std::vector<Asset<ShaderModule>>;

	void ProcessNode(const aiNode* node, const aiScene* scene, ModelData& model, std::string_view directory);
	void ProcessMaterial(uint32_t material_index, const aiScene* scene, Vulkan::Mesh& mesh, std::string_view directory);

	Textures m_textures;
	ModelDatas m_model_datas;
	Shaders m_shaders;
};

#endif

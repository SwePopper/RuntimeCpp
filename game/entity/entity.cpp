#include "platform.h"
#include "entity.h"
#include "components/transform.h"

#include <cstring>
#include <random>

Entity::Entity(std::string_view name, uint32_t uid, bool temporary)
	: m_uid(uid), m_components(nullptr), m_prev(nullptr), m_next(nullptr), m_temp(temporary)
{
	m_name_length = name.size();
	m_name = NewArray(char, m_name_length + 1);
	name.copy(m_name, m_name_length);
	m_name[m_name_length] = '\0';

	if(uid == 0)
	{
		static std::random_device rd;
		static std::mt19937_64 gen(rd());
		static std::uniform_int_distribution<uint32_t> dis;

		m_uid = dis(gen);
	}

	CreateComponent<Transform>();
}

Entity::~Entity()
{
	Unlink();

	DeleteArray(m_name);
	m_name = nullptr;
	m_name_length = 0;

	while(m_components)
	{
		Component* comp = m_components;
		m_components = m_components->GetNext();
		Delete(comp);
	}
}

void Entity::SetPrev(Entity* entity)
{
	if(entity)
	{
		entity->Unlink();

		if(m_prev)
		{
			m_prev->m_next = entity;
		}

		entity->m_prev = m_prev;
		entity->m_next = this;

		m_prev = entity;
	}
}

void Entity::SetNext(Entity* entity)
{
	if(entity)
	{
		Entity* last = entity;
		while(last->GetNext()) last = last->GetNext();

		if(m_next)
		{
			m_next->m_prev = last;
		}
		last->m_next = m_next;

		entity->m_prev = this;
		m_next = entity;
	}
}

void Entity::Unlink()
{
	if(m_prev) m_prev->m_next = m_next;
	if(m_next) m_next->m_prev = m_prev;
	m_next = m_prev = nullptr;
}

Component* Entity::CreateComponent(const ComponentID& id)
{
	Component* comp = GetComponent(id);
	if(comp) return comp;

	comp = Component::Create(id, this);
	if(m_components)
	{
		Component* last = m_components;
		while(last)
		{
			if(last->GetNext() == nullptr) break;
			last = last->GetNext();
		}

		last->SetNext(comp);
		comp->SetPrev(last);
	}
	else
	{
		m_components = comp;
	}

	return comp;
}

Component* Entity::GetComponent(const ComponentID& id) const
{
	Component* comp = m_components;
	while(comp)
	{
		if(comp->GetID() == id) return comp;
		comp = comp->GetNext();
	}
	return nullptr;
}

void Entity::Serialize(FILE* out)
{
	Component* comp = m_components;
	while(comp)
	{
		File::Write(out, "<Component id=\"%u\">", static_cast<std::uint32_t>(comp->GetID()));
		comp->Serialize(out);
		File::Write(out, "</Component>");

		comp = comp->GetNext();
	}
}

void Entity::Deserialize(const ComponentID& id, std::string_view name, const char** atts)
{
	Component* comp = GetComponent(id);
	if(comp)
	{
		comp->Deserialize(name, atts);
	}
}

void Entity::DeserializeDone()
{
	Component* comp = m_components;
	while(comp)
	{
		comp->DeserializeDone();
		comp = comp->GetNext();
	}
}

void Entity::Update(float dt)
{
	Component* comp = m_components;
	while(comp)
	{
		comp->Update(dt);
		comp = comp->GetNext();
	}
}

#ifndef COMPONENTS_H
#define COMPONENTS_H

#include <cstdio>
#include <cstdint>
#include <vector>

using ComponentID = uint32_t;
class Entity;

class Component
{
public:
	using CreateFunction = Component* (*)(Entity*);
	static ComponentID RegisterComponent(const char* type_name, const ComponentID& id, CreateFunction create_function);
	static void UnregisterComponent(const ComponentID& id);
	static const char* GetTypeName(const ComponentID& id);

	static Component* Create(const ComponentID& id, Entity* entity);
	virtual ~Component();

	void Unlink();

	const ComponentID& GetID() const { return m_id; }
	Entity* GetEntity() const { return m_entity; }
	Component* GetNext() const { return m_next; }
	void SetNext(Component* next) { m_next = next; }
	Component* GetPrev() const { return m_prev; }
	void SetPrev(Component* prev) { m_prev = prev; }

	virtual void Update(float dt);
	virtual void Serialize(FILE* out);
	virtual void Deserialize(std::string_view name, const char** attributes);
	virtual void DeserializeDone();
	virtual void DrawImGui();

protected:
	Component(ComponentID component_id, Entity* entity) : m_id(component_id), m_entity(entity), m_prev(nullptr), m_next(nullptr) {}

	ComponentID m_id;

	Entity* m_entity;
	Component* m_prev;
	Component* m_next;

private:
	struct ComponentCreator
	{
		const char* type_name;
		ComponentID id;
		CreateFunction Create;
	};
	using ComponentCreators = std::vector<ComponentCreator>;

	static std::uint32_t GetIndexFromID(const ComponentID id);

	static ComponentCreators sComponentCreators;
};

#endif

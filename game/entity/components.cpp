#include "platform.h"
#include "components.h"

#include <algorithm>

Component::ComponentCreators Component::sComponentCreators;

ComponentID Component::RegisterComponent(const char* type_name, const ComponentID& id, CreateFunction create_function)
{
	ComponentCreators::iterator it =
		std::find_if(sComponentCreators.begin(), sComponentCreators.end(), [id](ComponentCreator x) { return x.id == id; });
	if(it != sComponentCreators.end()) return it->id;
	sComponentCreators.push_back({type_name, id, create_function});
	return id;
}

void Component::UnregisterComponent(const ComponentID& id)
{
	if(!sComponentCreators.empty())
	{
		sComponentCreators.erase(
			std::remove_if(sComponentCreators.begin(), sComponentCreators.end(), [id](const ComponentCreator& x) { return x.id == id; }),
			sComponentCreators.end());
	}
}
const char* Component::GetTypeName(const ComponentID& id)
{
	uint32_t index = GetIndexFromID(id);
	return index < sComponentCreators.size() ? sComponentCreators[index].type_name : "";
}

Component* Component::Create(const ComponentID& id, Entity* entity)
{
	uint32_t index = GetIndexFromID(id);
	return index < sComponentCreators.size() ? sComponentCreators[index].Create(entity) : nullptr;
}

std::uint32_t Component::GetIndexFromID(const ComponentID id)
{
	ComponentCreators::size_type size = sComponentCreators.size();
	for(std::uint32_t i = 0; i < size; ++i)
		if(id == sComponentCreators[i].id) return i;
	return std::numeric_limits<uint32_t>::max();
}

Component::~Component() { Unlink(); }

void Component::Unlink()
{
	if(m_prev) m_prev->m_next = m_next;
	if(m_next) m_next->m_prev = m_prev;
	m_next = m_prev = nullptr;
}

void Component::Update(float /*dt*/) {}
void Component::Serialize(FILE* /*out*/) {}
void Component::Deserialize(std::string_view /*name*/, const char** /*attributes*/) {}
void Component::DeserializeDone() {}
void Component::DrawImGui() {}

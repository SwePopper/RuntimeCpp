#ifndef ENTITY_H
#define ENTITY_H

#include "components.h"

class Entity
{
public:
	Entity(std::string_view name, uint32_t uid, bool temporary);
	~Entity();

	const char* GetName() const { return m_name; }
	const uint32_t& GetUID() const { return m_uid; }

	Component* GetComponents() const { return m_components; }

	template <typename T>
	T* GetComponent() const
	{
		Component* component = GetComponent(T::id);
		if(component == nullptr) return nullptr;
		return reinterpret_cast<T*>(component);
	}

	template <typename T>
	T* CreateComponent()
	{
		Component* component = CreateComponent(T::id);
		if(component == nullptr) return nullptr;
		return reinterpret_cast<T*>(component);
	}

	Component* CreateComponent(const ComponentID& id);
	void DeserializeDone();

	void Serialize(FILE* out);
	void Deserialize(const ComponentID& id, std::string_view name, const char** atts);

	void Update(float dt);

	Entity* GetPrev() const { return m_prev; }
	void SetPrev(Entity* entity);

	Entity* GetNext() const { return m_next; }
	void SetNext(Entity* entity);

	void Unlink();

	const bool& IsTemporary() const { return m_temp; }
	void SetTemporary(const bool& temp) { m_temp = temp; }

private:
	Component* GetComponent(const ComponentID& id) const;

	char* m_name;
	std::string_view::size_type m_name_length;
	uint32_t m_uid;

	Component* m_components;

	Entity* m_prev;
	Entity* m_next;
	bool m_temp;
};

#endif

#ifndef PHYSICS_ACTOR_H
#define PHYSICS_ACTOR_H

class Transform;
class b2Body;

#include "entity/components.h"

class PhysicsActor : public Component
{
	COMPONENT_DATA(PhysicsActor);

public:
	PhysicsActor(Entity* entity);
	virtual ~PhysicsActor();

	void Initialize(float mass, float width, float height, bool static_body = true);
	void Release();

	virtual void Update(float dt);
	virtual void Serialize(FILE* out);
	virtual void Deserialize(std::string_view name, const char** attributes);
	virtual void DeserializeDone();
	virtual void DrawImGui();

	b2Body* GetBody() const { return m_body; }

private:
	Transform* m_transform;
	b2Body* m_body;

	float m_mass;
	bool m_static;
	float m_width;
	float m_height;

	friend void SerializePhysicsActor(FILE* out, Component* component);
	friend void DeserializePhysicsActor(Component* component, const char* name, const char** atts);
	friend void DeserializeDonePhysicsActor(Component* component);
};

#endif

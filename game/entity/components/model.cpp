#include "platform.h"
#include "entity/components.h"
#include "entity/entity.h"
#include "model.h"
#include "transform.h"
#include "camera.h"
#include "game.h"
#include "assets.h"
#include "vulkan/vulkan_renderer.h"
#include "renderer.h"

#include <string>
#include <glm/gtc/matrix_transform.hpp>

struct UniformBuffer
{
	glm::mat4 m_projection;
	glm::mat4 m_view;
	glm::mat4 m_model;
	glm::mat4 m_view_model;
};

Model::Model(Entity* entity)
	: Component(id, entity), m_path(nullptr), m_ignore_camera(false), m_hidden(false), m_transform(m_entity->GetComponent<Transform>())
{
}

Model::~Model() { Release(); }

bool Model::Initialize(const char* path)
{
	Release();

	if(path)
	{
		size_t size = strlen(path);
		m_path = NewArray(char, size + 1);
		strncpy(m_path, path, size);
		m_path[size] = 0;

		m_model_data = Game::GetInstance()->GetAssets()->LoadModel(m_path);
	}

	if(m_model_data.m_meshes.empty())
	{
		LogError("Failed to load model data: %s\n", path);
		return false;
	}

	Vulkan* vulkan = Game::GetInstance()->GetVulkan();
	vk::Device& device = vulkan->GetDevice();

	vk::DescriptorSetAllocateInfo alloc_info;
	alloc_info.descriptorPool = vulkan->GetDescriptorPool();
	alloc_info.descriptorSetCount = 1;
	alloc_info.pSetLayouts = Game::GetInstance()->GetRenderer()->GetDescriptorSetLayout();
	vk::Result result = device.allocateDescriptorSets(&alloc_info, &m_descriptor_set);
	if(vk::Result::eSuccess != result)
	{
		LogError("vkAllocateDescriptorSets(): %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}

	result = vulkan->CreateBuffer(m_uniform_buffer, nullptr, sizeof(UniformBuffer), vk::BufferUsageFlagBits::eUniformBuffer,
								  vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
	if(vk::Result::eSuccess != result)
	{
		LogError("Failed to create uniform buffer: %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}

	std::array<vk::WriteDescriptorSet, 3> uniform_descriptor_set;
	uniform_descriptor_set[0].dstSet = m_descriptor_set;
	uniform_descriptor_set[0].descriptorType = vk::DescriptorType::eUniformBuffer;
	uniform_descriptor_set[0].dstBinding = 0;
	uniform_descriptor_set[0].pBufferInfo = &m_uniform_buffer.m_descriptor;
	uniform_descriptor_set[0].descriptorCount = 1;

	uniform_descriptor_set[1].dstSet = m_descriptor_set;
	uniform_descriptor_set[1].descriptorType = vk::DescriptorType::eCombinedImageSampler;
	uniform_descriptor_set[1].dstBinding = 1;
	uniform_descriptor_set[1].pImageInfo = &m_model_data.m_meshes[0].m_albedo.m_descriptor;
	uniform_descriptor_set[1].descriptorCount = 1;

	uniform_descriptor_set[2].dstSet = m_descriptor_set;
	uniform_descriptor_set[2].descriptorType = vk::DescriptorType::eCombinedImageSampler;
	uniform_descriptor_set[2].dstBinding = 2;
	uniform_descriptor_set[2].pImageInfo = &m_model_data.m_meshes[0].m_normal.m_descriptor;
	uniform_descriptor_set[2].descriptorCount = 1;

	device.updateDescriptorSets(uniform_descriptor_set.size(), uniform_descriptor_set.data(), 0, nullptr);

	Game::GetInstance()->GetRenderer()->AddModel(this);

	return true;
}

void Model::Release()
{
	Game::GetInstance()->GetRenderer()->RemoveModel(this);

	DeleteArray(m_path);
	m_path = nullptr;

	Game::GetInstance()->GetAssets()->UnloadModel(m_model_data);
	m_model_data = ModelData();

	Vulkan* vulkan = Game::GetInstance()->GetVulkan();
	vk::Device& device = vulkan->GetDevice();

	vulkan->DestroyBuffer(m_uniform_buffer);

	if(m_descriptor_set)
	{
		device.freeDescriptorSets(vulkan->GetDescriptorPool(), 1, &m_descriptor_set);
		m_descriptor_set = nullptr;
	}
}

void Model::Update(float /*dt*/)
{
	if(!m_hidden)
	{
		Camera* camera = Game::GetInstance()->GetRenderer()->GetCamera();
		if(camera == nullptr) return;

		vk::Device& device = Game::GetInstance()->GetVulkan()->GetDevice();

		// Setup Uniform buffer
		{
			Transform* transform = m_entity->GetComponent<Transform>();

			UniformBuffer uniforms;
			uniforms.m_projection = camera->GetProjection();
			uniforms.m_view = camera->GetView();
			uniforms.m_model = transform->GetMatrix();
			uniforms.m_view_model = uniforms.m_view * transform->GetMatrix();

			void* data = nullptr;
			vk::Result result = device.mapMemory(m_uniform_buffer.m_memory, 0, sizeof(uniforms), vk::MemoryMapFlags(), &data);
			if(vk::Result::eSuccess == result)
			{
				memcpy(data, &uniforms, sizeof(uniforms));
				device.unmapMemory(m_uniform_buffer.m_memory);
			}
		}
	}
}

void Model::Serialize(FILE* out)
{
	if(m_path) File::Write(out, "<path p=\"%s\" />", m_path);
	File::Write(out, "<options ignore_camera=\"%u\" />", m_ignore_camera);
}

void Model::Deserialize(std::string_view name, const char** atts)
{
	if(name == "path")
	{
		DeleteArray(m_path);
		m_path = nullptr;

		for(std::uint32_t i = 0; atts[i]; i += 2)
		{
			if(strncmp(atts[i], "p", 2) == 0)
			{
				size_t size = strlen(atts[i + 1]);
				m_path = NewArray(char, size + 1);
				strncpy(m_path, atts[i + 1], size);
				m_path[size] = 0;
			}
		}
	}
	else if(name == "options")
	{
		for(std::uint32_t i = 0; atts[i]; i += 2)
		{
			std::uint8_t value = static_cast<std::uint8_t>(strtoul(atts[i + 1], nullptr, 0));
			if(strncmp(atts[i], "ignore_camera", 14) == 0) m_ignore_camera = value;
		}
	}
}

void Model::DeserializeDone()
{
	std::string path = "";
	if(m_path)
	{
		path = m_path;
	}

	if(!Initialize(path.c_str())) LogError("DeserializeDoneModel failed");
}

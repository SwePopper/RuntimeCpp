#ifndef CAMERA_H
#define CAMERA_H

class Transform;

#include "entity/components.h"

class Camera : public Component
{
	COMPONENT_DATA(Camera);

public:
	Camera(Entity* entity);
	virtual ~Camera();

	const glm::mat4& GetProjection()
	{
		if(m_dirty) UpdateMatrices();
		return m_projection;
	}

	glm::mat4 GetView() const;

	virtual void Serialize(FILE* out);
	virtual void Deserialize(std::string_view name, const char** attributes);
	virtual void DrawImGui();

private:
	void UpdateMatrices();

	Transform* m_transform;
	bool m_dirty;
	glm::mat4 m_projection;
	float m_fov;
	float m_aspect;
	float m_near;
	float m_far;
};

#endif

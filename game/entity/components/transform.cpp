#include "platform.h"
#include "transform.h"
#include "entity/entity.h"
#include "entity/components.h"
#include "game.h"

#include <glm/gtx/euler_angles.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <imgui/imgui.h>
#include <cstdlib>

Transform::Transform(Entity* entity)
	: Component(id, entity)
	, m_position()
	, m_rotation()
	, m_scale(1.0f, 1.0f, 1.0f)
	, m_dirty(true)
	, m_transform()
	, m_parent(nullptr)
	, m_deserialize_parent_id(0)
{
}

Transform::~Transform() {}

glm::mat4 Transform::GetMatrix()
{
	// if(m_dirty)
	{
		m_transform = glm::translate(glm::scale(glm::mat4(1.0f), m_scale) * glm::mat4_cast(m_rotation), m_position);
		// m_transform = glm::scale(glm::translate(glm::mat4(), m_position) * glm::mat4_cast(m_rotation), m_scale);
		m_dirty = false;
	}

	if(m_parent)
	{
		return m_parent->GetMatrix() * m_transform;
	}
	return m_transform;
}

void Transform::SetRotation(const glm::vec3& value)
{
	m_dirty = true;
	m_rotation = glm::quat(value);
}

void Transform::Serialize(FILE* out)
{
	glm::vec3 euler_angles = glm::eulerAngles(m_rotation);

	File::Write(out, "<pos x=\"%f\" y=\"%f\" z=\"%f\" />", static_cast<double>(m_position.x), static_cast<double>(m_position.y),
				static_cast<double>(m_position.z));
	File::Write(out, "<rot x=\"%f\" y=\"%f\" z=\"%f\" />", static_cast<double>(euler_angles.x), static_cast<double>(euler_angles.y),
				static_cast<double>(euler_angles.z));
	File::Write(out, "<sca x=\"%f\" y=\"%f\" z=\"%f\" />", static_cast<double>(m_scale.x), static_cast<double>(m_scale.y),
				static_cast<double>(m_scale.z));

	if(m_parent)
	{
		File::Write(out, "<parent entity_id=\"%u\" />", m_parent->GetEntity()->GetUID());
	}
}

void Transform::Deserialize(std::string_view name, const char** atts)
{
	if(name == "pos")
	{
		for(std::uint32_t i = 0; atts[i]; i += 2)
		{
			float value = strtof(atts[i + 1], nullptr);
			if(atts[i][0] == 'x')
				m_position.x = value;
			else if(atts[i][0] == 'y')
				m_position.y = value;
			else if(atts[i][0] == 'z')
				m_position.z = value;
		}
	}
	else if(name == "rot")
	{
		glm::vec3 euler_angles;
		for(std::uint32_t i = 0; atts[i]; i += 2)
		{
			float value = strtof(atts[i + 1], nullptr);
			if(atts[i][0] == 'x')
				euler_angles.x = value;
			else if(atts[i][0] == 'y')
				euler_angles.y = value;
			else if(atts[i][0] == 'z')
				euler_angles.z = value;
		}
		m_rotation = glm::quat(euler_angles);
	}
	else if(name == "sca")
	{
		for(std::uint32_t i = 0; atts[i]; i += 2)
		{
			float value = strtof(atts[i + 1], nullptr);
			if(atts[i][0] == 'x')
				m_scale.x = value;
			else if(atts[i][0] == 'y')
				m_scale.y = value;
			else if(atts[i][0] == 'z')
				m_scale.z = value;
		}
	}
	else if(name == "parent")
	{
		m_deserialize_parent_id = 0;
		for(std::uint32_t i = 0; atts[i]; i += 2)
		{
			std::string_view attribute = atts[i];
			if(attribute == "entity_id") m_deserialize_parent_id = static_cast<uint32_t>(strtoul(atts[i + 1], nullptr, 0));
		}
	}
}

void Transform::DeserializeDone()
{
	Entity* entity = Game::GetInstance()->GetEntity(m_deserialize_parent_id);
	if(entity) SetParent(entity->GetComponent<Transform>());
}

void Transform::DrawImGui()
{
	ImGui::AlignFirstTextHeightToWidgets();

	glm::mat mat = GetMatrix();

	// Position
	{
		ImGui::Text("Position (%f %f %f)", mat[3].x, mat[3].y, mat[3].z);
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();
		ImGui::InputFloat3("xyz", &m_position[0], 3, 0);
	}

	ImGui::NextColumn();
	ImGui::AlignFirstTextHeightToWidgets();

	// Rotation
	{
		ImGui::Text("Rotation");
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();

		glm::vec3 euler_angles = glm::eulerAngles(m_rotation);
		ImGui::InputFloat3("xyz", &euler_angles[0], 3, 0);
		m_rotation = glm::quat(euler_angles);
	}

	ImGui::NextColumn();
	ImGui::AlignFirstTextHeightToWidgets();

	// Scale
	{
		ImGui::Text("Scale");
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();
		ImGui::InputFloat3("xyz", &m_scale[0], 3, 0);
	}

	ImGui::NextColumn();
}

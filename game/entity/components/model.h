#ifndef MODEL_H
#define MODEL_H

class Transform;

#include "entity/components.h"
#include "vulkan/mesh.h"
#include "vulkan/vulkan_renderer.h"

class Model : public Component
{
	COMPONENT_DATA(Model);

public:
	Model(Entity* entity);
	virtual ~Model();

	bool Initialize(const char* path);
	void Release();

	virtual void Update(float dt);
	virtual void Serialize(FILE* out);
	virtual void Deserialize(std::string_view name, const char** attributes);
	virtual void DeserializeDone();

	Transform* GetTransform() const { return m_transform; }
	const ModelData& GetModelData() const { return m_model_data; }
	vk::DescriptorSet* GetDescriptoSet() { return &m_descriptor_set; }

private:
	char* m_path;
	bool m_ignore_camera;
	bool m_hidden;
	ModelData m_model_data;
	Transform* m_transform;

	vk::DescriptorSet m_descriptor_set;
	Vulkan::Buffer m_uniform_buffer;

	friend void SerializeModel(FILE* out, Component* component);
	friend void DeserializeModel(Component* component, const char* name, const char** atts);
	friend void DeserializeDoneModel(Component* component);
};

#endif

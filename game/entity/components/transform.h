#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "entity/components.h"

class Transform : public Component
{
	COMPONENT_DATA(Transform);

public:
	Transform(Entity* entity);
	virtual ~Transform();

	Transform* GetParent() const { return m_parent; }
	void SetParent(Transform* parent)
	{
		m_parent = parent;
		m_dirty = true;
	}

	glm::vec3 GetPosition() const
	{
		glm::vec3 position = m_position;
		if(m_parent) position += m_parent->GetPosition();
		return position;
	}

	glm::quat GetRotation() const
	{
		glm::quat rotation = m_rotation;
		if(m_parent) rotation *= m_parent->GetRotation();
		return rotation;
	}

	glm::vec3 GetScale() const
	{
		glm::vec3 scale = m_scale;
		if(m_parent) scale *= m_parent->GetScale();
		return scale;
	}

	glm::mat4 GetMatrix();

	void SetPosition(const glm::vec3& value)
	{
		m_dirty = m_dirty || m_position != value;
		m_position = value;
	}

	void SetRotation(const glm::quat& value)
	{
		m_dirty = m_dirty || m_rotation != value;
		m_rotation = value;
	}

	void SetRotation(const glm::vec3& value);

	void SetScale(const glm::vec3& value)
	{
		m_dirty = m_dirty || m_scale != value;
		m_scale = value;
	}

	virtual void Serialize(FILE* out);
	virtual void Deserialize(std::string_view name, const char** attributes);
	virtual void DeserializeDone();
	virtual void DrawImGui();

private:
	glm::vec3 m_position;
	glm::quat m_rotation;
	glm::vec3 m_scale;

	bool m_dirty;
	glm::mat4 m_transform;

	Transform* m_parent;
	uint32_t m_deserialize_parent_id;
};

#endif

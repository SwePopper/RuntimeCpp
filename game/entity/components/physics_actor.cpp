#include "platform.h"
#include "physics_actor.h"
#include "entity/components.h"
#include "entity/entity.h"
#include "transform.h"
#include "game.h"

#include <atomic>
#include <imgui/imgui.h>
#include <Box2D/Box2D.h>

class ContactListener : public b2ContactListener
{
public:
	/// Called when two fixtures begin to touch.
	virtual void BeginContact(b2Contact* /*contact*/) {}
	/// Called when two fixtures cease to touch.
	virtual void EndContact(b2Contact* /*contact*/) {}
};

namespace
{
ContactListener contact_listener;
std::atomic_bool initialized(false);
}

PhysicsActor::PhysicsActor(Entity* entity)
	: Component(id, entity)
	, m_transform(m_entity->GetComponent<Transform>())
	, m_body(nullptr)
	, m_mass(0.0f)
	, m_static(true)
	, m_width(0.0f)
	, m_height(0.0f)
{
	if(!initialized.exchange(true))
	{
		Game::GetInstance()->GetWorld()->SetContactListener(&contact_listener);
	}
}

PhysicsActor::~PhysicsActor() { Release(); }

void PhysicsActor::Initialize(float mass, float width, float height, bool static_body)
{
	Release();

	m_mass = mass;
	m_static = static_body;
	m_width = width;
	m_height = height;

	b2BodyDef body_def;
	body_def.position.Set(m_transform->GetPosition().x, m_transform->GetPosition().z);
	body_def.angle = 0.0f;
	body_def.type = static_body ? b2_staticBody : b2_dynamicBody;
	body_def.fixedRotation = true;
	body_def.linearDamping = 7.0f;
	m_body = Game::GetInstance()->GetWorld()->CreateBody(&body_def);

	b2PolygonShape body_shape;
	body_shape.SetAsBox(width * 0.5f, height * 0.5f);

	b2FixtureDef fixture_def;
	fixture_def.shape = &body_shape;
	fixture_def.density = static_body ? 0.0f : mass / (width * height);
	fixture_def.friction = 0.0f;
	fixture_def.restitution = 0.0f;

	m_body->CreateFixture(&fixture_def);

	m_body->SetUserData(this);
}

void PhysicsActor::Release()
{
	if(m_body)
	{
		b2World* world = Game::GetInstance()->GetWorld();
		if(world)
		{
			world->DestroyBody(m_body);
		}
		m_body = nullptr;
	}
}

void PhysicsActor::Update(float /*dt*/)
{
	if(m_body)
	{
		b2Vec2 pos = m_body->GetPosition();
		glm::vec3 position = m_transform->GetPosition();
		position.x = pos.x;
		position.z = pos.y;

		m_transform->SetPosition(position);
		// m_transform->m_rotation = m_body->GetAngle();
	}
}

void PhysicsActor::Serialize(FILE* out)
{
	File::Write(out, "<PhysicsActor mass=\"%f\" static=\"%s\" width=\"%f\" height=\"%f\" />", static_cast<double>(m_mass), m_static ? "1" : "0",
				static_cast<double>(m_width), static_cast<double>(m_height));
}

void PhysicsActor::Deserialize(std::string_view name, const char** atts)
{
	if(name == "PhysicsActor")
	{
		for(std::uint32_t i = 0; atts[i]; i += 2)
		{
			if(strncmp(atts[i], "mass", 5) == 0)
				m_mass = strtof(atts[i + 1], nullptr);
			else if(strncmp(atts[i], "static", 7) == 0)
				m_static = strtoul(atts[i + 1], nullptr, 0) != 0;
			else if(strncmp(atts[i], "width", 6) == 0)
				m_width = strtof(atts[i + 1], nullptr);
			else if(strncmp(atts[i], "height", 7) == 0)
				m_height = strtof(atts[i + 1], nullptr);
		}
	}
}

void PhysicsActor::DeserializeDone() { Initialize(m_mass, m_width, m_height, m_static); }

void PhysicsActor::DrawImGui()
{
	bool bool_value = false;
	float float_value = 0.0f;

	// Active
	{
		ImGui::AlignFirstTextHeightToWidgets();
		ImGui::Text("Active");
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();

		bool_value = GetBody()->IsActive();
		ImGui::Checkbox("", &bool_value);
		GetBody()->SetActive(bool_value);
	}

	// Awake
	{
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();
		ImGui::Text("Awake");
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();

		bool_value = GetBody()->IsAwake();
		ImGui::Checkbox("", &bool_value);
		GetBody()->SetAwake(bool_value);
	}

	// Bullet
	{
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();
		ImGui::Text("Bullet");
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();

		bool_value = GetBody()->IsBullet();
		ImGui::Checkbox("", &bool_value);
		GetBody()->SetBullet(bool_value);
	}

	// Fixed Rotation
	{
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();
		ImGui::Text("Fixed Rotation");
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();

		bool_value = GetBody()->IsFixedRotation();
		ImGui::Checkbox("Fixed Rotation", &bool_value);
		GetBody()->SetFixedRotation(bool_value);
	}

	// Sleeping Allowed
	{
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();
		ImGui::Text("Sleeping Allowed");
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();

		bool_value = GetBody()->IsSleepingAllowed();
		ImGui::Checkbox("", &bool_value);
		GetBody()->SetSleepingAllowed(bool_value);
	}

	// Angular Damping
	{
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();
		ImGui::Text("Angular Damping");
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();

		float_value = GetBody()->GetAngularDamping();
		ImGui::InputFloat("", &float_value, 1.0f / 180.0f, 1.0f / 360.0f, 3, 0);
		GetBody()->SetAngularDamping(float_value);
	}

	// Angular Velocity
	{
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();
		ImGui::Text("Angular Velocity");
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();

		float_value = GetBody()->GetAngularVelocity();
		ImGui::InputFloat("", &float_value, 1.0f / 180.0f, 1.0f / 360.0f, 3, 0);
		GetBody()->SetAngularVelocity(float_value);
	}

	// Linear Damping
	{
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();
		ImGui::Text("Linear Damping");
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();

		float_value = GetBody()->GetLinearDamping();
		ImGui::InputFloat("", &float_value, 1.0f / 180.0f, 1.0f / 360.0f, 3, 0);
		GetBody()->SetLinearDamping(float_value);
	}

	// Linear Velocity
	{
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();
		ImGui::Text("Linear Velocity");
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();

		float float2_value[]{GetBody()->GetLinearVelocity().x, GetBody()->GetLinearVelocity().y};
		ImGui::InputFloat2("", float2_value, 3, 0);
		GetBody()->SetLinearVelocity(b2Vec2(float2_value[0], float2_value[1]));
	}

	// Gravity Scale
	{
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();
		ImGui::Text("Gravity Scale");
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();

		float_value = GetBody()->GetGravityScale();
		ImGui::InputFloat("", &float_value, 1.0f / 180.0f, 1.0f / 360.0f, 3, 0);
		GetBody()->SetGravityScale(float_value);
	}

	ImGui::NextColumn();
}

#include "platform.h"
#include "camera.h"
#include "entity/components.h"
#include "entity/entity.h"
#include "transform.h"
#include "game.h"
#include "renderer.h"

#include <imgui/imgui.h>
#include <glm/gtc/matrix_transform.hpp>

Camera::Camera(Entity* entity)
	: Component(id, entity)
	, m_transform(m_entity->GetComponent<Transform>())
	, m_dirty(true)
	, m_projection()
	, m_fov(90.0f)
	, m_aspect(1920.0f / 1080.0f)
	, m_near(0.1f)
	, m_far(100.0f)
{
	Game::GetInstance()->GetRenderer()->SetCamera(this);
}

Camera::~Camera() { Game::GetInstance()->GetRenderer()->SetCamera(nullptr); }

glm::mat4 Camera::GetView() const
{
	glm::mat4 view = m_transform->GetMatrix();
	// return glm::inverse(view);
	return view;
}

void Camera::UpdateMatrices()
{
	m_projection = glm::perspective(glm::radians(m_fov), m_aspect, m_near, m_far);
	m_dirty = false;
}

void Camera::Serialize(FILE* out) { File::Write(out, "<values fov=\"%f\" aspect=\"%f\" near=\"%f\" far=\"%f\" />", m_fov, m_aspect, m_near, m_far); }

void Camera::Deserialize(std::string_view name, const char** atts)
{
	if(name == "values")
	{
		for(std::uint32_t i = 0; atts[i]; i += 2)
		{
			std::string_view attribute = atts[i];

			float value = strtof(atts[i + 1], nullptr);
			if(attribute == "fov")
				m_fov = value;
			else if(attribute == "aspect")
				m_aspect = value;
			else if(attribute == "near")
				m_near = value;
			else if(attribute == "far")
				m_far = value;
		}
	}
	m_dirty = true;
}

void Camera::DrawImGui()
{
	ImGui::AlignFirstTextHeightToWidgets();

	// Field of View
	{
		ImGui::Text("FOV");
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();
		float change_check = m_fov;
		ImGui::InputFloat("", &m_fov, 1, 0);
		m_dirty = m_dirty || std::abs(change_check - m_fov) > 0.001f;
	}

	ImGui::NextColumn();
	ImGui::AlignFirstTextHeightToWidgets();

	// Aspect Ratio
	{
		ImGui::Text("Aspect");
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();
		float change_check = m_aspect;
		ImGui::InputFloat("", &m_aspect, 1, 0);
		m_dirty = m_dirty || std::abs(change_check - m_aspect) > 0.001f;
	}

	ImGui::NextColumn();
	ImGui::AlignFirstTextHeightToWidgets();

	// Near Plane
	{
		ImGui::Text("Near Plane");
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();
		float change_check = m_near;
		ImGui::InputFloat("", &m_near, 1, 0);
		if(m_near >= m_far) m_near = m_far - 0.1f;
		m_dirty = m_dirty || std::abs(change_check - m_near) > 0.001f;
	}

	ImGui::NextColumn();
	ImGui::AlignFirstTextHeightToWidgets();

	// Far Plane
	{
		ImGui::Text("Far Plane");
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();
		float change_check = m_far;
		ImGui::InputFloat("", &m_far, 1, 0);
		if(m_far <= m_near) m_far = m_near + 0.1f;
		m_dirty = m_dirty || std::abs(change_check - m_far) > 0.001f;
	}

	ImGui::NextColumn();
	ImGui::AlignFirstTextHeightToWidgets();

	glm::mat4 view = GetView();

	// View
	{
		ImGui::Text("View");
		ImGui::NextColumn();
		ImGui::AlignFirstTextHeightToWidgets();
		ImGui::Text("%f %f %f", view[3].x, view[3].y, view[3].z);
	}

	// ImGui::NextColumn();
}

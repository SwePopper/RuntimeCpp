#include "platform.h"
#include "game.h"
#include "config.h"
#include "entity/entity.h"
#include "entity/components.h"
#include "imgui/imgui_renderer.h"
#include "input.h"
#include "io/xml_reader.h"
#include "scripts.h"
#include "assets.h"
#include "vulkan/vulkan_renderer.h"
#include "renderer.h"

// Components
#include "entity/components/transform.h"
#include "entity/components/physics_actor.h"
#include "entity/components/model.h"
#include "entity/components/camera.h"

#include <string>
#include <imgui/imgui.h>

#include <nfd.h>

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#include <Box2D/Dynamics/b2World.h>
#include "box2d_drawer.h"

Game* Game::s_instance = nullptr;

Game::Game()
	: m_root_path(nullptr)
	, m_assets_folder(nullptr)
	, m_save_path(nullptr)
	, m_window(nullptr)
	, m_vulkan(nullptr)
	, m_config(nullptr)
	, m_scripts(nullptr)
	, m_assets(nullptr)
	, m_renderer(nullptr)
	, m_world(nullptr)
	, m_debug_draw_box2d(true)
	, m_entity(nullptr)
	, m_input(nullptr)
	, m_imgui_renderer(nullptr)
	, m_play(true)
	, m_step(true)
{
}

bool Game::Initialize(const char* root_path)
{
	if(s_instance)
	{
		LogError("Game allready initialized\n");
		return false;
	}
	s_instance = this;

	if(!SetupSignalHandling())
	{
		LogError("Failed to setup signal handling!");
		return false;
	}

	m_root_path = root_path;

	std::string assets_path = m_root_path;
	if(strlen(m_root_path) > 0) assets_path += "/";
	assets_path += "assets/";

	m_assets_folder = NewArray(char, assets_path.size() + 1);
	strncpy(m_assets_folder, assets_path.c_str(), assets_path.size());
	m_assets_folder[assets_path.size()] = '\0';

	m_config = New(Config) Config;
	if(!m_config || !m_config->Initialize())
	{
		LogError("Failed to initialize config!");
		return false;
	}

	m_world = New(b2World) b2World(b2Vec2(0.0f, 0.0f));

	Box2DDrawer::Initialize(m_world);

	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER) < 0)
	{
		LogFatal("SDL_Init failed: %s\n", SDL_GetError());
		return false;
	}

	int sdl_image_flags = IMG_INIT_PNG;
	if(IMG_Init(sdl_image_flags) != sdl_image_flags)
	{
		LogFatal("IMG_Init failed %s\n", IMG_GetError());
		return false;
	}

	if(TTF_Init() == -1)
	{
		LogFatal("TTF_Init failed %s\n", TTF_GetError());
		return false;
	}

	uint32_t display = static_cast<uint32_t>(SDL_GetNumVideoDisplays());
	display = std::clamp(m_config->GetDesktopSettings().m_screen_index, 0u, display);

	m_window =
		SDL_CreateWindow(GAME_NAME, static_cast<int>(SDL_WINDOWPOS_CENTERED_DISPLAY(display)),
						 static_cast<int>(SDL_WINDOWPOS_CENTERED_DISPLAY(display)), static_cast<int>(m_config->GetDesktopSettings().m_screen_width),
						 static_cast<int>(m_config->GetDesktopSettings().m_screen_height), SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN);
	if(m_window == nullptr)
	{
		LogFatal("Failed to create window: %s\n", SDL_GetError());
		return false;
	}

	std::string application_icon = assets_path;
	application_icon += "icon.png";  // TODO: use different images depending on system?
	if(File::FileExists(application_icon.c_str()))
	{
		SDL_Surface* icon = IMG_Load(application_icon.c_str());
		if(icon)
		{
			SDL_SetWindowIcon(m_window, icon);
		}
		else
		{
			LogError("Failed to load window icon: %s  %s", SDL_GetError(), application_icon.c_str());
		}
	}
	else
	{
		LogWarning("Could not find window icon: %s", application_icon.c_str());
	}

	constexpr uint32_t fullscreen_flags[]{0, SDL_WINDOW_FULLSCREEN, SDL_WINDOW_FULLSCREEN_DESKTOP};
	SDL_SetWindowFullscreen(m_window, fullscreen_flags[static_cast<uint8_t>(m_config->GetDesktopSettings().m_screen_mode)]);

	m_window_size = glm::vec2(m_config->GetDesktopSettings().m_screen_width, m_config->GetDesktopSettings().m_screen_height);

	m_vulkan = New(Vulkan) Vulkan;
	if(!m_vulkan->Initialize(m_window))
	{
		LogError("Failed to initialize vulkan");
		return false;
	}

	{
		char* path = SDL_GetPrefPath(GAME_COMPANY, GAME_NAME);
		if(path)
		{
			LogInfo("Game save path: %s\n", path);
			size_t path_len = strlen(path);
			m_save_path = NewArray(char, path_len + 1);
			strncpy(m_save_path, path, path_len);
			m_save_path[path_len] = '\0';

			SDL_free(path);
		}
		else
		{
			m_save_path = NewArray(char, 1);
			m_save_path[0] = '\0';
		}
	}

	m_assets = New(Assets) Assets;
	if(!m_assets->Initialize())
	{
		LogFatal("Failed to Initialize Assets\n");
		SDL_DestroyWindow(m_window);
		m_window = nullptr;
		return false;
	}

	m_renderer = New(Renderer) Renderer;
	if(!m_renderer->Initialize())
	{
		LogError("Failed to initialize renderer");
		return false;
	}

	m_input = New(Input) Input;
	if(!m_input->Initialize())
	{
		LogFatal("Failed to Initialize Input\n");
		SDL_DestroyWindow(m_window);
		m_window = nullptr;
		return false;
	}

	m_imgui_renderer = New(ImGuiRenderer) ImGuiRenderer;
	if(!m_imgui_renderer->Initialize(m_vulkan->GetFrameCount()))
	{
		LogFatal("Failed to Initialize Imgui Renderer\n");
		SDL_DestroyWindow(m_window);
		m_window = nullptr;
		return false;
	}

	Component::RegisterComponent(Transform::name, Transform::id,
								 [](Entity* entity) { return reinterpret_cast<Component*>(New(Transform) Transform(entity)); });
	Component::RegisterComponent(PhysicsActor::name, PhysicsActor::id,
								 [](Entity* entity) { return reinterpret_cast<Component*>(New(PhysicsActor) PhysicsActor(entity)); });
	Component::RegisterComponent(Model::name, Model::id, [](Entity* entity) { return reinterpret_cast<Component*>(New(Model) Model(entity)); });
	Component::RegisterComponent(Camera::name, Camera::id, [](Entity* entity) { return reinterpret_cast<Component*>(New(Camera) Camera(entity)); });

	m_last_frame_timestamp = SDL_GetTicks();

	m_scripts = New(Scripts) Scripts;
	if(!m_scripts->Initialize())
	{
		LogFatal("Failed to Initialize Scripts\n");
		SDL_DestroyWindow(m_window);
		m_window = nullptr;
		return false;
	}

	return true;
}

void Game::Release()
{
	if(s_instance == nullptr) return;

	if(m_vulkan) m_vulkan->FlushAllFrames();

	Delete(m_scripts);
	m_scripts = nullptr;

	if(m_entity)
	{
		while(m_entity->GetNext()) Delete(m_entity->GetNext());
		Delete(m_entity);
		m_entity = nullptr;
	}

	Delete(m_imgui_renderer);
	m_imgui_renderer = nullptr;

	Delete(m_input);
	m_input = nullptr;

	Delete(m_assets);
	m_assets = nullptr;

	Delete(m_renderer);
	m_renderer = nullptr;

	Delete(m_vulkan);
	m_vulkan = nullptr;

	if(m_window)
	{
		SDL_DestroyWindow(m_window);
		m_window = nullptr;
	}

	Delete(m_world);
	m_world = nullptr;

	Delete(m_config);
	m_config = nullptr;

	DeleteArray(m_save_path);
	DeleteArray(m_assets_folder);
	m_save_path = m_assets_folder = nullptr;
	m_root_path = nullptr;

	TTF_Quit();
	IMG_Quit();
	SDL_Quit();

	s_instance = nullptr;
}

bool Game::Update()
{
	if(m_window == nullptr) return true;

	std::uint32_t ticks = SDL_GetTicks();
	float dt = static_cast<float>(ticks - m_last_frame_timestamp) / 1000.0f;
	m_last_frame_timestamp = ticks;

	SDL_Event event;
	while(SDL_PollEvent(&event))
	{
		m_imgui_renderer->ProcessEvent(&event);

		switch(event.type)
		{
		case SDL_QUIT: return false;
		default: break;
		}
	}

	int width = 0, height = 0;
	SDL_GetWindowSize(m_window, &width, &height);

	m_window_size = glm::vec2(static_cast<float>(width), static_cast<float>(height));

	m_world->Step(0.016f, 8, 3);

	m_imgui_renderer->StartFrame(dt);

	m_scripts->Update(dt);

	if(m_vulkan->BeginFrame(m_window))
	{
		m_input->Update();

		if(m_play || m_step)
		{
			m_step = false;
			Entity* entity = m_entity;
			while(entity)
			{
				entity->Update(dt);
				entity = entity->GetNext();
			}
		}

		m_renderer->Draw();

		if(m_debug_draw_box2d) m_world->DrawDebugData();

		m_scripts->Draw(dt);

		m_imgui_renderer->EndFrame();

		m_vulkan->EndFrame(m_window);
	}

	return true;
}

void Game::SetRootEntity(Entity* root)
{
	if(m_entity)
	{
		while(m_entity->GetNext()) Delete(m_entity->GetNext());
		Delete(m_entity);
	}
	m_entity = root;
}

Entity* Game::GetEntity(const uint32_t& entity_id) const
{
	Entity* current = m_entity;
	while(current)
	{
		if(current->GetUID() == entity_id) return current;
		current = current->GetNext();
	}
	return nullptr;
}

void Game::Quit()
{
	SDL_Event event;
	event.type = SDL_QUIT;
	SDL_PushEvent(&event);
}

#ifdef __linux

#include <signal.h>
#include <errno.h>

void SigHandler(int sig)
{
	// Reset the signal handler
	signal(sig, SigHandler);
	printf("SIGNAL: %i\n", sig);
}

bool Game::SetupSignalHandling()
{
	/*for(int i = SIGHUP; i <= SIGSYS; ++i)
	{
		if(i != SIGKILL && i != SIGSTOP && SIG_ERR == signal(i, SigHandler))
		{
			printf("failed to set signal %i  %i\n", i, errno);
			return false;
		}
	}*/

	return true;
}

#else

bool Game::SetupSignalHandling() { return true; }

#endif

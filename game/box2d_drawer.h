#ifndef BOX2D_DRAWER_H
#define BOX2D_DRAWER_H

class b2World;

namespace Box2DDrawer
{
void Initialize(b2World* world);
}

#endif

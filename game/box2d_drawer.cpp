#include "platform.h"
#include "box2d_drawer.h"
#include "game.h"
#include "entity/components/camera.h"

#include <Box2D/Dynamics/b2World.h>
#include <Box2D/Common/b2Draw.h>
#include <SDL.h>

class PhysicsDrawer : public b2Draw
{
public:
	/// Draw a closed polygon provided in CCW order.
	virtual void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);

	/// Draw a solid closed polygon provided in CCW order.
	virtual void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);

	/// Draw a circle.
	virtual void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color);

	/// Draw a solid circle.
	virtual void DrawSolidCircle(const b2Vec2& /*center*/, float32 /*radius*/, const b2Vec2& /*axis*/, const b2Color& color);

	/// Draw a line segment.
	virtual void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color);

	/// Draw a transform. Choose your own length scale.
	/// @param xf a transform.
	virtual void DrawTransform(const b2Transform& /*xf*/);

	void SetColor(const b2Color& color);
};

void PhysicsDrawer::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) { DrawSolidPolygon(vertices, vertexCount, color); }

void PhysicsDrawer::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
	/*glm::vec2 position(Game::GetInstance()->GetCamera()->GetCamera().x, Game::GetInstance()->GetCamera()->GetCamera().z);
	SetColor(color);
	for(int32_t i = 0; i < vertexCount; ++i)
	{
		int p1x = static_cast<int>(vertices[i].x + position.x);
		int p1y = static_cast<int>(vertices[i].y + position.y);
		int p2x = static_cast<int>(vertices[0].x + position.x), p2y = static_cast<int>(vertices[0].y + position.y);
		if(i + 1 < vertexCount)
		{
			p2x = static_cast<int>(vertices[i + 1].x + position.x);
			p2y = static_cast<int>(vertices[i + 1].y + position.y);
		}

		// SDL_RenderDrawLine(Game::GetInstance()->GetRenderer(), p1x, p1y, p2x, p2y);
	}*/
}

void PhysicsDrawer::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color)
{
	DrawSolidCircle(center, radius, b2Vec2(1.0f, 0.0f), color);
}

void PhysicsDrawer::DrawSolidCircle(const b2Vec2& /*center*/, float32 /*radius*/, const b2Vec2& /*axis*/, const b2Color& color) { SetColor(color); }

void PhysicsDrawer::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color)
{
	SetColor(color);
	// glm::vec2 position(Game::GetInstance()->GetCamera()->GetCamera().x, Game::GetInstance()->GetCamera()->GetCamera().z);
	// SDL_RenderDrawLine(Game::GetInstance()->GetRenderer(), static_cast<int>(p1.x + position.x), static_cast<int>(p1.y + position.y),
	//				   static_cast<int>(p2.x + position.x), static_cast<int>(p2.y + position.y));
}

void PhysicsDrawer::DrawTransform(const b2Transform& /*xf*/) {}

void PhysicsDrawer::SetColor(const b2Color& color)
{
	// SDL_SetRenderDrawColor(Game::GetInstance()->GetRenderer(), static_cast<std::uint8_t>(color.r * 256), static_cast<std::uint8_t>(color.g * 256),
	//					   static_cast<std::uint8_t>(color.b * 256), static_cast<std::uint8_t>(color.a * 256));
}

namespace Box2DDrawer
{
static PhysicsDrawer physics_drawer;

void Initialize(b2World* world)
{
	world->SetDebugDraw(&physics_drawer);
	physics_drawer.SetFlags(b2Draw::e_shapeBit);
}
}

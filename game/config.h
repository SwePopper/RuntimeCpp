#ifndef CONFIG_H
#define CONFIG_H

#include <cstdint>

enum class ScreenMode : uint8_t
{
	kWindowed,
	kFullscreen,
	kFakeFullscreen
};

class Config
{
public:
	struct DesktopSettings
	{
		DesktopSettings();

		uint32_t m_screen_width, m_screen_height, m_screen_index;
		ScreenMode m_screen_mode;
	};

	Config();
	~Config() { Release(); }

	bool Initialize();
	void Release();

	const DesktopSettings& GetDesktopSettings() const { return m_desktop_settings; }

private:
	DesktopSettings m_desktop_settings;
};

#endif

#include "platform.h"
#include "allocator.h"

#include <cstdlib>

Allocator Allocator::sMainAllocator;

#ifdef PLATFORM_WINDOWS
#define aligned_alloc(align, size) _aligned_malloc(size, align)
#define aligned_free(mem) _aligned_free(mem)
#else
#define aligned_free(mem) free(mem)
#endif

namespace Functions
{
void* Allocate(std::size_t size, std::size_t align, const char* /*file*/, int /*line*/) { return aligned_alloc(align, size); }
void Deallocate(void* mem) { aligned_free(mem); }
}

void Allocator::SetupDefaultAllocFunctions()
{
	m_allocate_function = Functions::Allocate;
	m_deallocate_function = Functions::Deallocate;
}

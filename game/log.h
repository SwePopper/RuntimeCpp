#ifndef LOG_H
#define LOG_H

namespace Log
{
enum class LogSeverity : std::uint8_t
{
	kInfo,
	kDebug,
	kWarning,
	kError,
	kFatal,
	kCount
};

using LogFunction = void (*)(void* user_data, const char* msg);

std::uint32_t AddLogger(void* user_data, LogFunction func);
void RemoveLogger(std::uint32_t id);

void LogMessage(const char* file, int line, LogSeverity severity, const char* format, ...);
}

#define LogInfo(...) Log::LogMessage(__FILE__, __LINE__, Log::LogSeverity::kInfo, __VA_ARGS__)
#define LogDebug(...) Log::LogMessage(__FILE__, __LINE__, Log::LogSeverity::kDebug, __VA_ARGS__)
#define LogWarning(...) Log::LogMessage(__FILE__, __LINE__, Log::LogSeverity::kWarning, __VA_ARGS__)
#define LogError(...) Log::LogMessage(__FILE__, __LINE__, Log::LogSeverity::kError, __VA_ARGS__)
#define LogFatal(...) Log::LogMessage(__FILE__, __LINE__, Log::LogSeverity::kFatal, __VA_ARGS__)

#endif

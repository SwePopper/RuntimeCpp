#ifndef INPUT_H
#define INPUT_H

#include "input_buttons.h"

#include <vector>
#include <string>

typedef struct _SDL_GameController SDL_GameController;

struct InputState
{
	static constexpr float kDeadZone = 0.05f;

	InputState() : value(0.0f), prev(0.0f) {}

	float value;
	float prev;

	inline bool Up() const { return value < kDeadZone; }
	inline bool PrevUp() const { return prev < kDeadZone; }
	inline bool Down() const { return value >= kDeadZone; }
	inline bool PrevDown() const { return prev >= kDeadZone; }

	inline bool Pressed() const { return Down() && PrevUp(); }
	inline bool Released() const { return Up() && PrevDown(); }
};

class Input
{
public:
	enum class InputData : std::uint32_t
	{
		kMaxControllers = 16
	};

	Input();
	~Input() { Release(); }

	bool Initialize();
	void Release();

	void Update();

	bool HasMouseInput() const;

	std::uint32_t RegisterAction(const char* action_name, const InputButton& default_button);
	const InputState& GetAction(const std::uint32_t& action_name_hash, std::uint32_t controller = 0);

private:
	using Buttons = std::vector<InputButton>;
	using InputStates = std::vector<InputState>;
	struct Action
	{
		Action() : m_action_name(""), m_action_name_hash(0) { m_action_states.resize(1); }

		std::string m_action_name;
		Buttons m_action_buttons;
		InputStates m_action_states;
		std::uint32_t m_action_name_hash;
		std::uint32_t padding;
	};
	using ActionContainer = std::vector<Action>;

	SDL_GameController* m_controllers[static_cast<std::uint32_t>(InputData::kMaxControllers)];
	ActionContainer m_actions;
};

#endif

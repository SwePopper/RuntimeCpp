#ifndef IMGUI_RENDERER_H
#define IMGUI_RENDERER_H

struct ImDrawData;
union SDL_Event;

#include "vulkan/vulkan_renderer.h"

class ImGuiRenderer
{
public:
	ImGuiRenderer();
	~ImGuiRenderer() { Release(); }

	bool Initialize(uint32_t frame_count);
	void Release();

	void ProcessEvent(SDL_Event* event);

	void StartFrame(float dt);
	void EndFrame();

	void RenderDrawLists(ImDrawData* draw_data);

private:
	void CreateRenderObjects();
	void CreateFonts();

	float m_mouse_wheel;
	bool m_mouse_pressed[3];

	uint32_t m_frame_index;

	std::vector<vk::DeviceMemory> m_vertex_buffer_memory;
	std::vector<vk::DeviceMemory> m_index_buffer_memory;
	std::vector<size_t> m_vertex_buffer_size;
	std::vector<size_t> m_index_buffer_size;
	std::vector<vk::Buffer> m_vertex_buffer;
	std::vector<vk::Buffer> m_index_buffer;

	vk::Sampler m_font_sampler;
	vk::DescriptorSetLayout m_descriptor_set_layout;
	vk::DescriptorSet m_descriptor_set;
	vk::PipelineLayout m_pipeline_layout;
	vk::Pipeline m_pipeline;

	vk::Image m_font_image;
	vk::DeviceMemory m_font_memory;
	vk::ImageView m_font_view;

	vk::Buffer m_upload_buffer;
	vk::DeviceMemory m_upload_buffer_memory;
};

#endif

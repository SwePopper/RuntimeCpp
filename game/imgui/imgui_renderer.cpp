#include "platform.h"
#include "imgui_renderer.h"
#include "game.h"

#include <imgui/imgui.h>
#include <SDL.h>

namespace
{
constexpr uint32_t glsl_shader_vert_spv[] = {
	0x07230203, 0x00010000, 0x00080001, 0x0000002e, 0x00000000, 0x00020011, 0x00000001, 0x0006000b, 0x00000001, 0x4c534c47, 0x6474732e, 0x3035342e,
	0x00000000, 0x0003000e, 0x00000000, 0x00000001, 0x000a000f, 0x00000000, 0x00000004, 0x6e69616d, 0x00000000, 0x0000000b, 0x0000000f, 0x00000015,
	0x0000001b, 0x0000001c, 0x00030003, 0x00000002, 0x000001c2, 0x00040005, 0x00000004, 0x6e69616d, 0x00000000, 0x00030005, 0x00000009, 0x00000000,
	0x00050006, 0x00000009, 0x00000000, 0x6f6c6f43, 0x00000072, 0x00040006, 0x00000009, 0x00000001, 0x00005655, 0x00030005, 0x0000000b, 0x0074754f,
	0x00040005, 0x0000000f, 0x6c6f4361, 0x0000726f, 0x00030005, 0x00000015, 0x00565561, 0x00060005, 0x00000019, 0x505f6c67, 0x65567265, 0x78657472,
	0x00000000, 0x00060006, 0x00000019, 0x00000000, 0x505f6c67, 0x7469736f, 0x006e6f69, 0x00030005, 0x0000001b, 0x00000000, 0x00040005, 0x0000001c,
	0x736f5061, 0x00000000, 0x00060005, 0x0000001e, 0x73755075, 0x6e6f4368, 0x6e617473, 0x00000074, 0x00050006, 0x0000001e, 0x00000000, 0x61635375,
	0x0000656c, 0x00060006, 0x0000001e, 0x00000001, 0x61725475, 0x616c736e, 0x00006574, 0x00030005, 0x00000020, 0x00006370, 0x00040047, 0x0000000b,
	0x0000001e, 0x00000000, 0x00040047, 0x0000000f, 0x0000001e, 0x00000002, 0x00040047, 0x00000015, 0x0000001e, 0x00000001, 0x00050048, 0x00000019,
	0x00000000, 0x0000000b, 0x00000000, 0x00030047, 0x00000019, 0x00000002, 0x00040047, 0x0000001c, 0x0000001e, 0x00000000, 0x00050048, 0x0000001e,
	0x00000000, 0x00000023, 0x00000000, 0x00050048, 0x0000001e, 0x00000001, 0x00000023, 0x00000008, 0x00030047, 0x0000001e, 0x00000002, 0x00020013,
	0x00000002, 0x00030021, 0x00000003, 0x00000002, 0x00030016, 0x00000006, 0x00000020, 0x00040017, 0x00000007, 0x00000006, 0x00000004, 0x00040017,
	0x00000008, 0x00000006, 0x00000002, 0x0004001e, 0x00000009, 0x00000007, 0x00000008, 0x00040020, 0x0000000a, 0x00000003, 0x00000009, 0x0004003b,
	0x0000000a, 0x0000000b, 0x00000003, 0x00040015, 0x0000000c, 0x00000020, 0x00000001, 0x0004002b, 0x0000000c, 0x0000000d, 0x00000000, 0x00040020,
	0x0000000e, 0x00000001, 0x00000007, 0x0004003b, 0x0000000e, 0x0000000f, 0x00000001, 0x00040020, 0x00000011, 0x00000003, 0x00000007, 0x0004002b,
	0x0000000c, 0x00000013, 0x00000001, 0x00040020, 0x00000014, 0x00000001, 0x00000008, 0x0004003b, 0x00000014, 0x00000015, 0x00000001, 0x00040020,
	0x00000017, 0x00000003, 0x00000008, 0x0003001e, 0x00000019, 0x00000007, 0x00040020, 0x0000001a, 0x00000003, 0x00000019, 0x0004003b, 0x0000001a,
	0x0000001b, 0x00000003, 0x0004003b, 0x00000014, 0x0000001c, 0x00000001, 0x0004001e, 0x0000001e, 0x00000008, 0x00000008, 0x00040020, 0x0000001f,
	0x00000009, 0x0000001e, 0x0004003b, 0x0000001f, 0x00000020, 0x00000009, 0x00040020, 0x00000021, 0x00000009, 0x00000008, 0x0004002b, 0x00000006,
	0x00000028, 0x00000000, 0x0004002b, 0x00000006, 0x00000029, 0x3f800000, 0x00050036, 0x00000002, 0x00000004, 0x00000000, 0x00000003, 0x000200f8,
	0x00000005, 0x0004003d, 0x00000007, 0x00000010, 0x0000000f, 0x00050041, 0x00000011, 0x00000012, 0x0000000b, 0x0000000d, 0x0003003e, 0x00000012,
	0x00000010, 0x0004003d, 0x00000008, 0x00000016, 0x00000015, 0x00050041, 0x00000017, 0x00000018, 0x0000000b, 0x00000013, 0x0003003e, 0x00000018,
	0x00000016, 0x0004003d, 0x00000008, 0x0000001d, 0x0000001c, 0x00050041, 0x00000021, 0x00000022, 0x00000020, 0x0000000d, 0x0004003d, 0x00000008,
	0x00000023, 0x00000022, 0x00050085, 0x00000008, 0x00000024, 0x0000001d, 0x00000023, 0x00050041, 0x00000021, 0x00000025, 0x00000020, 0x00000013,
	0x0004003d, 0x00000008, 0x00000026, 0x00000025, 0x00050081, 0x00000008, 0x00000027, 0x00000024, 0x00000026, 0x00050051, 0x00000006, 0x0000002a,
	0x00000027, 0x00000000, 0x00050051, 0x00000006, 0x0000002b, 0x00000027, 0x00000001, 0x00070050, 0x00000007, 0x0000002c, 0x0000002a, 0x0000002b,
	0x00000028, 0x00000029, 0x00050041, 0x00000011, 0x0000002d, 0x0000001b, 0x0000000d, 0x0003003e, 0x0000002d, 0x0000002c, 0x000100fd, 0x00010038};

constexpr uint32_t glsl_shader_frag_spv[] = {
	0x07230203, 0x00010000, 0x00080001, 0x0000001e, 0x00000000, 0x00020011, 0x00000001, 0x0006000b, 0x00000001, 0x4c534c47, 0x6474732e, 0x3035342e,
	0x00000000, 0x0003000e, 0x00000000, 0x00000001, 0x0007000f, 0x00000004, 0x00000004, 0x6e69616d, 0x00000000, 0x00000009, 0x0000000d, 0x00030010,
	0x00000004, 0x00000007, 0x00030003, 0x00000002, 0x000001c2, 0x00040005, 0x00000004, 0x6e69616d, 0x00000000, 0x00040005, 0x00000009, 0x6c6f4366,
	0x0000726f, 0x00030005, 0x0000000b, 0x00000000, 0x00050006, 0x0000000b, 0x00000000, 0x6f6c6f43, 0x00000072, 0x00040006, 0x0000000b, 0x00000001,
	0x00005655, 0x00030005, 0x0000000d, 0x00006e49, 0x00050005, 0x00000016, 0x78655473, 0x65727574, 0x00000000, 0x00040047, 0x00000009, 0x0000001e,
	0x00000000, 0x00040047, 0x0000000d, 0x0000001e, 0x00000000, 0x00040047, 0x00000016, 0x00000022, 0x00000000, 0x00040047, 0x00000016, 0x00000021,
	0x00000000, 0x00020013, 0x00000002, 0x00030021, 0x00000003, 0x00000002, 0x00030016, 0x00000006, 0x00000020, 0x00040017, 0x00000007, 0x00000006,
	0x00000004, 0x00040020, 0x00000008, 0x00000003, 0x00000007, 0x0004003b, 0x00000008, 0x00000009, 0x00000003, 0x00040017, 0x0000000a, 0x00000006,
	0x00000002, 0x0004001e, 0x0000000b, 0x00000007, 0x0000000a, 0x00040020, 0x0000000c, 0x00000001, 0x0000000b, 0x0004003b, 0x0000000c, 0x0000000d,
	0x00000001, 0x00040015, 0x0000000e, 0x00000020, 0x00000001, 0x0004002b, 0x0000000e, 0x0000000f, 0x00000000, 0x00040020, 0x00000010, 0x00000001,
	0x00000007, 0x00090019, 0x00000013, 0x00000006, 0x00000001, 0x00000000, 0x00000000, 0x00000000, 0x00000001, 0x00000000, 0x0003001b, 0x00000014,
	0x00000013, 0x00040020, 0x00000015, 0x00000000, 0x00000014, 0x0004003b, 0x00000015, 0x00000016, 0x00000000, 0x0004002b, 0x0000000e, 0x00000018,
	0x00000001, 0x00040020, 0x00000019, 0x00000001, 0x0000000a, 0x00050036, 0x00000002, 0x00000004, 0x00000000, 0x00000003, 0x000200f8, 0x00000005,
	0x00050041, 0x00000010, 0x00000011, 0x0000000d, 0x0000000f, 0x0004003d, 0x00000007, 0x00000012, 0x00000011, 0x0004003d, 0x00000014, 0x00000017,
	0x00000016, 0x00050041, 0x00000019, 0x0000001a, 0x0000000d, 0x00000018, 0x0004003d, 0x0000000a, 0x0000001b, 0x0000001a, 0x00050057, 0x00000007,
	0x0000001c, 0x00000017, 0x0000001b, 0x00050085, 0x00000007, 0x0000001d, 0x00000012, 0x0000001c, 0x0003003e, 0x00000009, 0x0000001d, 0x000100fd,
	0x00010038};
}

ImGuiRenderer::ImGuiRenderer()
	: m_mouse_wheel(0.0f)
	, m_frame_index(0)
	, m_vertex_buffer_memory()
	, m_index_buffer_memory()
	, m_vertex_buffer_size()
	, m_index_buffer_size()
	, m_vertex_buffer()
	, m_index_buffer()
	, m_font_sampler()
	, m_descriptor_set_layout()
	, m_descriptor_set()
	, m_pipeline_layout()
	, m_pipeline()
	, m_font_image()
	, m_font_memory()
	, m_font_view()
	, m_upload_buffer()
	, m_upload_buffer_memory()
{
	memset(m_mouse_pressed, 0, sizeof(m_mouse_pressed));
}

bool ImGuiRenderer::Initialize(uint32_t frame_count)
{
	ImGuiIO &io = ImGui::GetIO();
	io.RenderDrawListsFn = [](ImDrawData *draw_data) { Game::GetInstance()->GetImGuiRenderer()->RenderDrawLists(draw_data); };
	io.SetClipboardTextFn = nullptr;
	io.GetClipboardTextFn = nullptr;
	io.ClipboardUserData = nullptr;

	// Keyboard mapping. ImGui will use those indices to peek into the io.KeyDown[] array.
	io.KeyMap[ImGuiKey_Tab] = SDLK_TAB;
	io.KeyMap[ImGuiKey_LeftArrow] = SDL_SCANCODE_LEFT;
	io.KeyMap[ImGuiKey_RightArrow] = SDL_SCANCODE_RIGHT;
	io.KeyMap[ImGuiKey_UpArrow] = SDL_SCANCODE_UP;
	io.KeyMap[ImGuiKey_DownArrow] = SDL_SCANCODE_DOWN;
	io.KeyMap[ImGuiKey_PageUp] = SDL_SCANCODE_PAGEUP;
	io.KeyMap[ImGuiKey_PageDown] = SDL_SCANCODE_PAGEDOWN;
	io.KeyMap[ImGuiKey_Home] = SDL_SCANCODE_HOME;
	io.KeyMap[ImGuiKey_End] = SDL_SCANCODE_END;
	io.KeyMap[ImGuiKey_Delete] = SDLK_DELETE;
	io.KeyMap[ImGuiKey_Backspace] = SDLK_BACKSPACE;
	io.KeyMap[ImGuiKey_Enter] = SDLK_RETURN;
	io.KeyMap[ImGuiKey_Escape] = SDLK_ESCAPE;
	io.KeyMap[ImGuiKey_A] = SDLK_a;
	io.KeyMap[ImGuiKey_C] = SDLK_c;
	io.KeyMap[ImGuiKey_V] = SDLK_v;
	io.KeyMap[ImGuiKey_X] = SDLK_x;
	io.KeyMap[ImGuiKey_Y] = SDLK_y;
	io.KeyMap[ImGuiKey_Z] = SDLK_z;

	CreateRenderObjects();

	m_vertex_buffer_memory.resize(frame_count);
	m_index_buffer_memory.resize(frame_count);
	m_vertex_buffer_size.resize(frame_count);
	m_index_buffer_size.resize(frame_count);
	m_vertex_buffer.resize(frame_count);
	m_index_buffer.resize(frame_count);

	return true;
}

void ImGuiRenderer::Release()
{
	Vulkan *vulkan = Game::GetInstance()->GetVulkan();
	vk::Device &device = vulkan->GetDevice();
	vk::AllocationCallbacks *allocator = nullptr;

	if(m_upload_buffer)
	{
		device.destroyBuffer(m_upload_buffer, allocator);
		m_upload_buffer = nullptr;
	}

	if(m_upload_buffer_memory)
	{
		device.freeMemory(m_upload_buffer_memory, allocator);
		m_upload_buffer_memory = nullptr;
	}

	for(uint32_t i = 0; i < m_vertex_buffer.size(); ++i)
	{
		if(m_vertex_buffer[i])
		{
			device.destroyBuffer(m_vertex_buffer[i], allocator);
		}
		if(m_vertex_buffer_memory[i])
		{
			device.freeMemory(m_vertex_buffer_memory[i], allocator);
		}
		if(m_index_buffer[i])
		{
			device.destroyBuffer(m_index_buffer[i], allocator);
		}
		if(m_index_buffer_memory[i])
		{
			device.freeMemory(m_index_buffer_memory[i], allocator);
		}
	}
	m_vertex_buffer.clear();
	m_vertex_buffer_memory.clear();
	m_index_buffer.clear();
	m_index_buffer_memory.clear();

	if(m_font_view)
	{
		device.destroyImageView(m_font_view, allocator);
		m_font_view = nullptr;
	}

	if(m_font_image)
	{
		device.destroyImage(m_font_image, allocator);
		m_font_image = nullptr;
	}

	if(m_font_memory)
	{
		device.freeMemory(m_font_memory, allocator);
		m_font_memory = nullptr;
	}

	if(m_font_sampler)
	{
		device.destroySampler(m_font_sampler, allocator);
		m_font_sampler = nullptr;
	}

	if(m_descriptor_set)
	{
		device.freeDescriptorSets(vulkan->GetDescriptorPool(), 1, &m_descriptor_set);
		m_descriptor_set = nullptr;
	}

	if(m_descriptor_set_layout)
	{
		device.destroyDescriptorSetLayout(m_descriptor_set_layout, allocator);
		m_descriptor_set_layout = nullptr;
	}

	if(m_pipeline)
	{
		device.destroyPipeline(m_pipeline, nullptr);
		m_pipeline = nullptr;
	}

	if(m_pipeline_layout)
	{
		device.destroyPipelineLayout(m_pipeline_layout, allocator);
		m_pipeline_layout = nullptr;
	}
	ImGui::Shutdown();
}

void ImGuiRenderer::ProcessEvent(SDL_Event *event)
{
	ImGuiIO &io = ImGui::GetIO();
	switch(event->type)
	{
	case SDL_MOUSEWHEEL:
	{
		if(event->wheel.y > 0) m_mouse_wheel = 1;
		if(event->wheel.y < 0) m_mouse_wheel = -1;
		return;
	}
	case SDL_MOUSEBUTTONDOWN:
	{
		if(event->button.button == SDL_BUTTON_LEFT) m_mouse_pressed[0] = true;
		if(event->button.button == SDL_BUTTON_RIGHT) m_mouse_pressed[1] = true;
		if(event->button.button == SDL_BUTTON_MIDDLE) m_mouse_pressed[2] = true;
		return;
	}
	case SDL_TEXTINPUT:
	{
		io.AddInputCharactersUTF8(event->text.text);
		return;
	}
	case SDL_KEYDOWN:
	case SDL_KEYUP:
	{
		int key = event->key.keysym.sym & ~SDLK_SCANCODE_MASK;
		io.KeysDown[key] = (event->type == SDL_KEYDOWN);
		io.KeyShift = ((SDL_GetModState() & KMOD_SHIFT) != 0);
		io.KeyCtrl = ((SDL_GetModState() & KMOD_CTRL) != 0);
		io.KeyAlt = ((SDL_GetModState() & KMOD_ALT) != 0);
		io.KeySuper = ((SDL_GetModState() & KMOD_GUI) != 0);
		return;
	}
	}
}

void ImGuiRenderer::StartFrame(float dt)
{
	if(!m_font_image) CreateFonts();

	ImGuiIO &io = ImGui::GetIO();

	// Setup display size (every frame to accommodate for window resizing)
	int w, h;
	int display_w, display_h;
	SDL_GetWindowSize(Game::GetInstance()->GetWindow(), &w, &h);
	SDL_GL_GetDrawableSize(Game::GetInstance()->GetWindow(), &display_w, &display_h);
	io.DisplaySize = ImVec2(static_cast<float>(w), static_cast<float>(h));
	io.DisplayFramebufferScale =
		ImVec2(w > 0 ? (static_cast<float>(display_w) / io.DisplaySize.x) : 0, h > 0 ? (static_cast<float>(display_h) / io.DisplaySize.y) : 0);

	// Setup time step
	io.DeltaTime = dt;

	// Setup inputs
	int mx, my;
	std::uint32_t mouse_mask = SDL_GetMouseState(&mx, &my);
	if(SDL_GetWindowFlags(Game::GetInstance()->GetWindow()) & SDL_WINDOW_MOUSE_FOCUS)
	{
		// Mouse position, in pixels (set to -1,-1 if no mouse / on another screen, etc.)
		io.MousePos = ImVec2(static_cast<float>(mx), static_cast<float>(my));
	}
	else
	{
		io.MousePos = ImVec2(-1, -1);
	}

	io.MouseDown[0] = m_mouse_pressed[0] || (mouse_mask & SDL_BUTTON(SDL_BUTTON_LEFT)) != 0;
	io.MouseDown[1] = m_mouse_pressed[1] || (mouse_mask & SDL_BUTTON(SDL_BUTTON_RIGHT)) != 0;
	io.MouseDown[2] = m_mouse_pressed[2] || (mouse_mask & SDL_BUTTON(SDL_BUTTON_MIDDLE)) != 0;
	m_mouse_pressed[0] = m_mouse_pressed[1] = m_mouse_pressed[2] = false;

	io.MouseWheel = m_mouse_wheel;
	m_mouse_wheel = 0.0f;

	// Start the frame
	ImGui::NewFrame();
}

void ImGuiRenderer::EndFrame()
{
	ImGui::Render();
	m_frame_index = (m_frame_index + 1) % m_vertex_buffer_memory.size();
}

void ImGuiRenderer::CreateRenderObjects()
{
	Vulkan *vulkan = Game::GetInstance()->GetVulkan();
	vk::Device &device = vulkan->GetDevice();
	vk::AllocationCallbacks *allocator = nullptr;

	vk::Result result = vk::Result::eSuccess;
	vk::ShaderModule vert_module;
	vk::ShaderModule frag_module;

	// Create The Shader Modules:
	{
		vk::ShaderModuleCreateInfo vert_info;
		vert_info.codeSize = sizeof(glsl_shader_vert_spv);
		vert_info.pCode = reinterpret_cast<const uint32_t *>(glsl_shader_vert_spv);
		result = device.createShaderModule(&vert_info, allocator, &vert_module);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkCreateShaderModule(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}

		vk::ShaderModuleCreateInfo frag_info;
		frag_info.codeSize = sizeof(glsl_shader_frag_spv);
		frag_info.pCode = reinterpret_cast<const uint32_t *>(glsl_shader_frag_spv);
		result = device.createShaderModule(&frag_info, allocator, &frag_module);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkCreateShaderModule(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}
	}

	if(!m_font_sampler)
	{
		vk::SamplerCreateInfo info;
		info.magFilter = vk::Filter::eLinear;
		info.minFilter = vk::Filter::eLinear;
		info.mipmapMode = vk::SamplerMipmapMode::eLinear;
		info.addressModeU = vk::SamplerAddressMode::eRepeat;
		info.addressModeV = vk::SamplerAddressMode::eRepeat;
		info.addressModeW = vk::SamplerAddressMode::eRepeat;
		info.minLod = -1000;
		info.maxLod = 1000;
		info.maxAnisotropy = 1.0f;
		result = device.createSampler(&info, allocator, &m_font_sampler);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkCreateSampler(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}
	}

	if(!m_descriptor_set_layout)
	{
		vk::Sampler sampler[1] = {m_font_sampler};

		vk::DescriptorSetLayoutBinding binding[1];
		binding[0].descriptorCount = 1;
		binding[0].stageFlags = vk::ShaderStageFlagBits::eFragment;
		binding[0].pImmutableSamplers = sampler;
		binding[0].descriptorType = vk::DescriptorType::eCombinedImageSampler;

		vk::DescriptorSetLayoutCreateInfo info = {};
		info.bindingCount = 1;
		info.pBindings = binding;
		result = device.createDescriptorSetLayout(&info, allocator, &m_descriptor_set_layout);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkCreateDescriptorSetLayout(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}
	}

	// Create Descriptor Set:
	{
		vk::DescriptorSetAllocateInfo alloc_info;
		alloc_info.descriptorPool = vulkan->GetDescriptorPool();
		alloc_info.descriptorSetCount = 1;
		alloc_info.pSetLayouts = &m_descriptor_set_layout;
		result = device.allocateDescriptorSets(&alloc_info, &m_descriptor_set);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkAllocateDescriptorSets(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}
	}

	if(!m_pipeline_layout)
	{
		vk::PushConstantRange push_constants[1];
		push_constants[0].stageFlags = vk::ShaderStageFlagBits::eVertex;
		push_constants[0].offset = sizeof(float) * 0;
		push_constants[0].size = sizeof(float) * 4;

		vk::DescriptorSetLayout set_layout[1] = {m_descriptor_set_layout};

		vk::PipelineLayoutCreateInfo layout_info;
		layout_info.setLayoutCount = 1;
		layout_info.pSetLayouts = set_layout;
		layout_info.pushConstantRangeCount = 1;
		layout_info.pPushConstantRanges = push_constants;
		result = device.createPipelineLayout(&layout_info, allocator, &m_pipeline_layout);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkCreatePipelineLayout(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}
	}

	vk::PipelineShaderStageCreateInfo stage[2];
	stage[0].stage = vk::ShaderStageFlagBits::eVertex;
	stage[0].module = vert_module;
	stage[0].pName = "main";
	stage[1].stage = vk::ShaderStageFlagBits::eFragment;
	stage[1].module = frag_module;
	stage[1].pName = "main";

	vk::VertexInputBindingDescription binding_desc[1];
	binding_desc[0].stride = sizeof(ImDrawVert);
	binding_desc[0].inputRate = vk::VertexInputRate::eVertex;

	vk::VertexInputAttributeDescription attribute_desc[3];
	attribute_desc[0].location = 0;
	attribute_desc[0].binding = binding_desc[0].binding;
	attribute_desc[0].format = vk::Format::eR32G32Sfloat;
	attribute_desc[0].offset = reinterpret_cast<size_t>(&(reinterpret_cast<ImDrawVert *>(0)->pos));
	attribute_desc[1].location = 1;
	attribute_desc[1].binding = binding_desc[0].binding;
	attribute_desc[1].format = vk::Format::eR32G32Sfloat;
	attribute_desc[1].offset = reinterpret_cast<size_t>(&(reinterpret_cast<ImDrawVert *>(0)->uv));
	attribute_desc[2].location = 2;
	attribute_desc[2].binding = binding_desc[0].binding;
	attribute_desc[2].format = vk::Format::eR8G8B8A8Unorm;
	attribute_desc[2].offset = reinterpret_cast<size_t>(&(reinterpret_cast<ImDrawVert *>(0)->col));

	vk::PipelineVertexInputStateCreateInfo vertex_info;
	vertex_info.vertexBindingDescriptionCount = 1;
	vertex_info.pVertexBindingDescriptions = binding_desc;
	vertex_info.vertexAttributeDescriptionCount = 3;
	vertex_info.pVertexAttributeDescriptions = attribute_desc;

	vk::PipelineInputAssemblyStateCreateInfo ia_info;
	ia_info.topology = vk::PrimitiveTopology::eTriangleList;

	vk::PipelineViewportStateCreateInfo viewport_info;
	viewport_info.viewportCount = 1;
	viewport_info.scissorCount = 1;

	vk::PipelineRasterizationStateCreateInfo raster_info;
	raster_info.polygonMode = vk::PolygonMode::eFill;
	raster_info.cullMode = vk::CullModeFlagBits::eNone;
	raster_info.frontFace = vk::FrontFace::eCounterClockwise;
	raster_info.lineWidth = 1.0f;

	vk::PipelineMultisampleStateCreateInfo ms_info;
	ms_info.rasterizationSamples = vk::SampleCountFlagBits::e1;

	vk::PipelineColorBlendAttachmentState color_attachment[1];
	color_attachment[0].blendEnable = VK_TRUE;
	color_attachment[0].srcColorBlendFactor = vk::BlendFactor::eSrcAlpha;
	color_attachment[0].dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha;
	color_attachment[0].colorBlendOp = vk::BlendOp::eAdd;
	color_attachment[0].srcAlphaBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha;
	color_attachment[0].dstAlphaBlendFactor = vk::BlendFactor::eZero;
	color_attachment[0].alphaBlendOp = vk::BlendOp::eAdd;
	color_attachment[0].colorWriteMask =
		vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;

	vk::PipelineDepthStencilStateCreateInfo depth_info;

	vk::PipelineColorBlendStateCreateInfo blend_info;
	blend_info.attachmentCount = 1;
	blend_info.pAttachments = color_attachment;

	vk::DynamicState dynamic_states[2] = {vk::DynamicState::eViewport, vk::DynamicState::eScissor};
	vk::PipelineDynamicStateCreateInfo dynamic_state;
	dynamic_state.dynamicStateCount = 2;
	dynamic_state.pDynamicStates = dynamic_states;

	vk::PipelineCreateFlags pipeline_create_flags;

	vk::GraphicsPipelineCreateInfo info;
	info.flags = pipeline_create_flags;
	info.stageCount = 2;
	info.pStages = stage;
	info.pVertexInputState = &vertex_info;
	info.pInputAssemblyState = &ia_info;
	info.pViewportState = &viewport_info;
	info.pRasterizationState = &raster_info;
	info.pMultisampleState = &ms_info;
	info.pDepthStencilState = &depth_info;
	info.pColorBlendState = &blend_info;
	info.pDynamicState = &dynamic_state;
	info.layout = m_pipeline_layout;
	info.renderPass = vulkan->GetRenderPass();

	result = device.createGraphicsPipelines(nullptr, 1, &info, allocator, &m_pipeline);
	if(vk::Result::eSuccess != result)
	{
		LogError("vkCreateGraphicsPipelines(): %s\n", Vulkan::GetVulkanResultString(result));
		return;
	}

	device.destroyShaderModule(vert_module, allocator);
	device.destroyShaderModule(frag_module, allocator);
}

void ImGuiRenderer::CreateFonts()
{
	ImGuiIO &io = ImGui::GetIO();
	Vulkan *vulkan = Game::GetInstance()->GetVulkan();
	vk::Device &device = vulkan->GetDevice();
	vk::AllocationCallbacks *allocator = nullptr;

	vk::CommandBuffer command_buffer = vulkan->CreateCommandBuffer(vk::CommandBufferLevel::ePrimary);

	unsigned char *pixels;
	int width, height;
	io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);
	size_t upload_size = static_cast<size_t>(width * height) * 4 * sizeof(char);

	vk::Result result = vk::Result::eSuccess;

	// Create the Image:
	{
		vk::ImageCreateInfo info;
		info.imageType = vk::ImageType::e2D;
		info.format = vk::Format::eR8G8B8A8Unorm;
		info.extent.width = static_cast<uint32_t>(width);
		info.extent.height = static_cast<uint32_t>(height);
		info.extent.depth = 1;
		info.mipLevels = 1;
		info.arrayLayers = 1;
		info.samples = vk::SampleCountFlagBits::e1;
		info.tiling = vk::ImageTiling::eOptimal;
		info.usage = vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eTransferDst;
		info.sharingMode = vk::SharingMode::eExclusive;
		info.initialLayout = vk::ImageLayout::eUndefined;

		result = device.createImage(&info, allocator, &m_font_image);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkCreateImage(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}

		vk::MemoryRequirements req = device.getImageMemoryRequirements(m_font_image);

		vk::MemoryAllocateInfo alloc_info;
		alloc_info.allocationSize = req.size;
		alloc_info.memoryTypeIndex = vulkan->GetMemoryType(req.memoryTypeBits, vk::MemoryPropertyFlagBits::eDeviceLocal);

		result = device.allocateMemory(&alloc_info, allocator, &m_font_memory);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkAllocateMemory(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}

		device.bindImageMemory(m_font_image, m_font_memory, 0);
	}

	// Create the Image View:
	{
		vk::ImageViewCreateInfo info;
		info.image = m_font_image;
		info.viewType = vk::ImageViewType::e2D;
		info.format = vk::Format::eR8G8B8A8Unorm;
		info.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
		info.subresourceRange.levelCount = 1;
		info.subresourceRange.layerCount = 1;

		result = device.createImageView(&info, allocator, &m_font_view);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkCreateImageView(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}
	}

	// Update the Descriptor Set:
	{
		vk::DescriptorImageInfo desc_image[1];
		desc_image[0].sampler = m_font_sampler;
		desc_image[0].imageView = m_font_view;
		desc_image[0].imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;

		vk::WriteDescriptorSet write_desc[1];
		write_desc[0].dstSet = m_descriptor_set;
		write_desc[0].descriptorCount = 1;
		write_desc[0].descriptorType = vk::DescriptorType::eCombinedImageSampler;
		write_desc[0].pImageInfo = desc_image;

		device.updateDescriptorSets(1, write_desc, 0, nullptr);
	}

	// Create the Upload Buffer:
	{
		vk::BufferCreateInfo buffer_info;
		buffer_info.size = upload_size;
		buffer_info.usage = vk::BufferUsageFlagBits::eTransferSrc;
		buffer_info.sharingMode = vk::SharingMode::eExclusive;

		result = device.createBuffer(&buffer_info, allocator, &m_upload_buffer);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkCreateBuffer(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}

		vk::MemoryRequirements req = device.getBufferMemoryRequirements(m_upload_buffer);

		vk::MemoryAllocateInfo alloc_info;
		alloc_info.allocationSize = req.size;
		alloc_info.memoryTypeIndex = vulkan->GetMemoryType(req.memoryTypeBits, vk::MemoryPropertyFlagBits::eHostVisible);

		result = device.allocateMemory(&alloc_info, allocator, &m_upload_buffer_memory);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkAllocateMemory(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}
		device.bindBufferMemory(m_upload_buffer, m_upload_buffer_memory, 0);
	}

	// Upload to Buffer:
	{
		vk::MemoryMapFlags flags;
		void *map = nullptr;
		result = device.mapMemory(m_upload_buffer_memory, 0, upload_size, flags, &map);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkMapMemory(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}
		memcpy(map, pixels, upload_size);

		vk::MappedMemoryRange range[1];
		range[0].memory = m_upload_buffer_memory;
		range[0].size = upload_size;
		result = device.flushMappedMemoryRanges(1, range);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkFlushMappedMemoryRanges(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}

		device.unmapMemory(m_upload_buffer_memory);
	}

	// Copy to Image:
	{
		vk::ImageMemoryBarrier copy_barrier[1];
		copy_barrier[0].dstAccessMask = vk::AccessFlagBits::eTransferWrite;
		copy_barrier[0].oldLayout = vk::ImageLayout::eUndefined;
		copy_barrier[0].newLayout = vk::ImageLayout::eTransferDstOptimal;
		copy_barrier[0].srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		copy_barrier[0].dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		copy_barrier[0].image = m_font_image;
		copy_barrier[0].subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
		copy_barrier[0].subresourceRange.levelCount = 1;
		copy_barrier[0].subresourceRange.layerCount = 1;

		vk::DependencyFlags flags;
		command_buffer.pipelineBarrier(vk::PipelineStageFlagBits::eHost, vk::PipelineStageFlagBits::eTransfer, flags, 0, nullptr, 0, nullptr, 1,
									   copy_barrier);

		vk::BufferImageCopy region;
		region.imageSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
		region.imageSubresource.layerCount = 1;
		region.imageExtent.width = static_cast<uint32_t>(width);
		region.imageExtent.height = static_cast<uint32_t>(height);
		region.imageExtent.depth = 1;

		command_buffer.copyBufferToImage(m_upload_buffer, m_font_image, vk::ImageLayout::eTransferDstOptimal, 1, &region);

		vk::ImageMemoryBarrier use_barrier[1];
		use_barrier[0].srcAccessMask = vk::AccessFlagBits::eTransferWrite;
		use_barrier[0].dstAccessMask = vk::AccessFlagBits::eShaderRead;
		use_barrier[0].oldLayout = vk::ImageLayout::eTransferDstOptimal;
		use_barrier[0].newLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
		use_barrier[0].srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		use_barrier[0].dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		use_barrier[0].image = m_font_image;
		use_barrier[0].subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
		use_barrier[0].subresourceRange.levelCount = 1;
		use_barrier[0].subresourceRange.layerCount = 1;

		command_buffer.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader, flags, 0, nullptr, 0,
									   nullptr, 1, use_barrier);
	}

	vulkan->FlushCommandBuffer(command_buffer, vulkan->GetGraphicsQueue());

	// Store our identifier
	io.Fonts->TexID = reinterpret_cast<void *>(static_cast<VkImage>(m_font_image));
}

void ImGuiRenderer::RenderDrawLists(ImDrawData *draw_data)
{
	ImGuiIO &io = ImGui::GetIO();
	size_t vertex_size = static_cast<size_t>(draw_data->TotalVtxCount) * sizeof(ImDrawVert);

	Vulkan *vulkan = Game::GetInstance()->GetVulkan();
	vk::AllocationCallbacks *allocator = nullptr;
	vk::Device &device = vulkan->GetDevice();
	size_t buffer_memory_alignment = 256;
	vk::Result result = vk::Result::eSuccess;

	if(!m_vertex_buffer[m_frame_index] || m_vertex_buffer_size[m_frame_index] < vertex_size)
	{
		if(m_vertex_buffer[m_frame_index]) device.destroyBuffer(m_vertex_buffer[m_frame_index], allocator);
		if(m_vertex_buffer_memory[m_frame_index]) device.freeMemory(m_vertex_buffer_memory[m_frame_index], allocator);

		size_t vertex_buffer_size = ((vertex_size - 1) / buffer_memory_alignment + 1) * buffer_memory_alignment;
		vk::BufferCreateInfo buffer_info;
		buffer_info.size = vertex_buffer_size;
		buffer_info.usage = vk::BufferUsageFlagBits::eVertexBuffer;
		buffer_info.sharingMode = vk::SharingMode::eExclusive;
		result = device.createBuffer(&buffer_info, allocator, &m_vertex_buffer[m_frame_index]);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkCreateBuffer(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}

		vk::MemoryRequirements req = device.getBufferMemoryRequirements(m_vertex_buffer[m_frame_index]);
		buffer_memory_alignment = (buffer_memory_alignment > req.alignment) ? buffer_memory_alignment : req.alignment;

		vk::MemoryAllocateInfo alloc_info;
		alloc_info.allocationSize = req.size;
		alloc_info.memoryTypeIndex = vulkan->GetMemoryType(req.memoryTypeBits, vk::MemoryPropertyFlagBits::eHostVisible);
		result = device.allocateMemory(&alloc_info, allocator, &m_vertex_buffer_memory[m_frame_index]);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkAllocateMemory(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}
		device.bindBufferMemory(m_vertex_buffer[m_frame_index], m_vertex_buffer_memory[m_frame_index], 0);
		m_vertex_buffer_size[m_frame_index] = vertex_buffer_size;
	}

	// Create the Index Buffer:
	size_t index_size = static_cast<size_t>(draw_data->TotalIdxCount) * sizeof(ImDrawIdx);
	if(!m_index_buffer[m_frame_index] || m_index_buffer_size[m_frame_index] < index_size)
	{
		if(m_index_buffer[m_frame_index]) device.destroyBuffer(m_index_buffer[m_frame_index], allocator);
		if(m_index_buffer_memory[m_frame_index]) device.freeMemory(m_index_buffer_memory[m_frame_index], allocator);

		size_t index_buffer_size = ((index_size - 1) / buffer_memory_alignment + 1) * buffer_memory_alignment;
		vk::BufferCreateInfo buffer_info = {};
		buffer_info.size = index_buffer_size;
		buffer_info.usage = vk::BufferUsageFlagBits::eIndexBuffer;
		buffer_info.sharingMode = vk::SharingMode::eExclusive;
		result = device.createBuffer(&buffer_info, allocator, &m_index_buffer[m_frame_index]);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkCreateBuffer(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}

		vk::MemoryRequirements req = device.getBufferMemoryRequirements(m_index_buffer[m_frame_index]);
		buffer_memory_alignment = (buffer_memory_alignment > req.alignment) ? buffer_memory_alignment : req.alignment;

		vk::MemoryAllocateInfo alloc_info;
		alloc_info.allocationSize = req.size;
		alloc_info.memoryTypeIndex = vulkan->GetMemoryType(req.memoryTypeBits, vk::MemoryPropertyFlagBits::eHostVisible);
		result = device.allocateMemory(&alloc_info, allocator, &m_index_buffer_memory[m_frame_index]);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkAllocateMemory(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}
		device.bindBufferMemory(m_index_buffer[m_frame_index], m_index_buffer_memory[m_frame_index], 0);
		m_index_buffer_size[m_frame_index] = index_buffer_size;
	}

	// Upload Vertex and index Data:
	{
		ImDrawVert *vtx_dst;
		ImDrawIdx *idx_dst;
		vk::MemoryMapFlags flags;

		result = device.mapMemory(m_vertex_buffer_memory[m_frame_index], 0, vertex_size, flags, reinterpret_cast<void **>(&vtx_dst));
		if(vk::Result::eSuccess != result)
		{
			LogError("vkMapMemory(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}

		result = device.mapMemory(m_index_buffer_memory[m_frame_index], 0, index_size, flags, reinterpret_cast<void **>(&idx_dst));
		if(vk::Result::eSuccess != result)
		{
			LogError("vkMapMemory(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}

		for(int n = 0; n < draw_data->CmdListsCount; ++n)
		{
			const ImDrawList *cmd_list = draw_data->CmdLists[n];
			memcpy(vtx_dst, cmd_list->VtxBuffer.Data, static_cast<size_t>(cmd_list->VtxBuffer.Size) * sizeof(ImDrawVert));
			memcpy(idx_dst, cmd_list->IdxBuffer.Data, static_cast<size_t>(cmd_list->IdxBuffer.Size) * sizeof(ImDrawIdx));
			vtx_dst += cmd_list->VtxBuffer.Size;
			idx_dst += cmd_list->IdxBuffer.Size;
		}

		vk::MappedMemoryRange range[2];
		range[0].memory = m_vertex_buffer_memory[m_frame_index];
		range[0].size = VK_WHOLE_SIZE;
		range[1].memory = m_index_buffer_memory[m_frame_index];
		range[1].size = VK_WHOLE_SIZE;

		result = device.flushMappedMemoryRanges(2, range);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkMapMemory(): %s\n", Vulkan::GetVulkanResultString(result));
			return;
		}

		device.unmapMemory(m_vertex_buffer_memory[m_frame_index]);
		device.unmapMemory(m_index_buffer_memory[m_frame_index]);
	}

	vk::CommandBuffer &command_buffer = vulkan->GetCurrentCommandBuffer();

	std::array<vk::ClearValue, 2> clear_value;
	clear_value[0].color.setFloat32({{1.0f, 1.0f, 1.0f, 0.0f}});
	clear_value[1].depthStencil.depth = 1.0f;
	clear_value[1].depthStencil.stencil = 0;

	vk::RenderPassBeginInfo info;
	info.renderPass = vulkan->GetRenderPass();
	info.framebuffer = vulkan->GetCurrentFramebuffer();
	info.renderArea.extent.width = vulkan->GetSwapchainExtent().width;
	info.renderArea.extent.height = vulkan->GetSwapchainExtent().height;
	info.clearValueCount = clear_value.size();
	info.pClearValues = clear_value.data();

	command_buffer.beginRenderPass(&info, vk::SubpassContents::eInline);

	// Bind pipeline and descriptor sets:
	{
		command_buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, m_pipeline);
		vk::DescriptorSet desc_set[1] = {m_descriptor_set};
		command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_pipeline_layout, 0, 1, desc_set, 0, nullptr);
	}

	// Bind Vertex And Index Buffer:
	{
		vk::Buffer vertex_buffers[1] = {m_vertex_buffer[m_frame_index]};
		vk::DeviceSize vertex_offset[1] = {0};
		command_buffer.bindVertexBuffers(0, 1, vertex_buffers, vertex_offset);
		command_buffer.bindIndexBuffer(m_index_buffer[m_frame_index], 0, vk::IndexType::eUint16);
	}

	// Setup viewport:
	{
		vk::Viewport viewport;
		viewport.x = 0;
		viewport.y = 0;
		viewport.width = ImGui::GetIO().DisplaySize.x;
		viewport.height = ImGui::GetIO().DisplaySize.y;
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;
		command_buffer.setViewport(0, 1, &viewport);
	}

	// Setup scale and translation:
	{
		float scale[2];
		scale[0] = 2.0f / io.DisplaySize.x;
		scale[1] = 2.0f / io.DisplaySize.y;
		float translate[2];
		translate[0] = -1.0f;
		translate[1] = -1.0f;

		command_buffer.pushConstants(m_pipeline_layout, vk::ShaderStageFlagBits::eVertex, sizeof(float) * 0, sizeof(float) * 2, scale);
		command_buffer.pushConstants(m_pipeline_layout, vk::ShaderStageFlagBits::eVertex, sizeof(float) * 2, sizeof(float) * 2, translate);
	}

	// Render the command lists:
	int vtx_offset = 0;
	uint32_t idx_offset = 0;
	for(int n = 0; n < draw_data->CmdListsCount; ++n)
	{
		const ImDrawList *cmd_list = draw_data->CmdLists[n];
		for(int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
		{
			const ImDrawCmd *pcmd = &cmd_list->CmdBuffer[cmd_i];
			if(pcmd->UserCallback)
			{
				pcmd->UserCallback(cmd_list, pcmd);
			}
			else
			{
				vk::Rect2D scissor;
				scissor.offset.x = static_cast<int32_t>(pcmd->ClipRect.x) > 0 ? static_cast<int32_t>(pcmd->ClipRect.x) : 0;
				scissor.offset.y = static_cast<int32_t>(pcmd->ClipRect.y) > 0 ? static_cast<int32_t>(pcmd->ClipRect.y) : 0;
				scissor.extent.width = static_cast<uint32_t>(pcmd->ClipRect.z - pcmd->ClipRect.x);
				scissor.extent.height = static_cast<uint32_t>(pcmd->ClipRect.w - pcmd->ClipRect.y + 1);  // FIXME: Why +1 here?
				command_buffer.setScissor(0, 1, &scissor);
				command_buffer.drawIndexed(pcmd->ElemCount, 1, idx_offset, vtx_offset, 0);
			}
			idx_offset += pcmd->ElemCount;
		}
		vtx_offset += cmd_list->VtxBuffer.Size;
	}

	command_buffer.endRenderPass();
}

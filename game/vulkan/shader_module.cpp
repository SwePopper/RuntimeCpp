#include "platform.h"
#include "shader_module.h"

#include "game.h"
#include "vulkan_renderer.h"

constexpr uint32_t kSpirVMagicNumber = 0x07230203;

bool ShaderModule::Create(const char* code, const size_t& size)
{
	vk::ShaderModule module;

	vk::ShaderModuleCreateInfo vert_info;
	vert_info.codeSize = size;
	vert_info.pCode = reinterpret_cast<const uint32_t*>(code);
	vk::Result result = Game::GetInstance()->GetVulkan()->GetDevice().createShaderModule(&vert_info, nullptr, &module);

	if(vk::Result::eSuccess != result)
	{
		LogError("vkCreateShaderModule(): %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}

	m_shader_module = module;
	return true;

	// https://www.khronos.org/registry/spir-v/specs/1.2/SPIRV.html#_a_id_physicallayout_a_physical_layout_of_a_spir_v_module_and_instruction
	// https://www.khronos.org/registry/spir-v/specs/1.2/SPIRV.html#Magic

	/*size_t count = size / sizeof(uint32_t);
	if(count < 5)
	{
		LogWarning("Invalid shader code. Could not reflect\n");
		return true;
	}

	size_t index = 0;
	const uint32_t* data = reinterpret_cast<const uint32_t*>(code);
	if(data[index++] != kSpirVMagicNumber)
	{
		LogWarning("Invalid magic number in shader code\n");
		return true;
	}

	uint32_t version = data[index++];
	uint32_t generator = data[index++];
	uint32_t bound = data[index++];
	uint32_t schema = data[index++];

	while(index < count)
	{
		size_t start = index;

		uint32_t first_word = data[index];
		uint16_t word_count = first_word >> 16;
		uint32_t opcode = first_word & 0xffff;

		size_t next_instruction = index + count;
		++index;

		if(next_instruction > count)
		{
			LogWarning("Invalid instruction!\n");
			break;
		}

		uint32_t op_count = count - 1;
	}
	return true;*/
}

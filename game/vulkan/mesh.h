#ifndef MESH_H
#define MESH_H

#include <vector>

#include "vulkan_renderer.h"
#include "math/bounds.h"

struct Vertex
{
	float m_position[3];
	float m_normal[3];
	float m_uv[2];
	float m_color[3];
	float m_tangent[3];
};

class ModelData
{
public:
	using Meshes = std::vector<Vulkan::Mesh>;

	void Release();

	bool operator==(const ModelData& rhs) const;

	void AddMesh(const Vulkan::Mesh& mesh);

	Meshes m_meshes;
};

#endif

#include "platform.h"
#include "mesh.h"
#include "game.h"

void ModelData::Release()
{
	Vulkan* vulkan = Game::GetInstance()->GetVulkan();
	for(Meshes::iterator it = m_meshes.begin(); it != m_meshes.end(); ++it)
	{
		vulkan->DestroyMesh(*it);
	}
	m_meshes.clear();
}

bool ModelData::operator==(const ModelData& rhs) const
{
	if(rhs.m_meshes.size() != m_meshes.size()) return false;
	return m_meshes.empty() || m_meshes[0].m_vertex_buffer.m_buffer == rhs.m_meshes[0].m_vertex_buffer.m_buffer;
}

void ModelData::AddMesh(const Vulkan::Mesh& mesh) { m_meshes.push_back(mesh); }

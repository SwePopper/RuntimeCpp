#ifndef SHADER_MODULE_H
#define SHADER_MODULE_H

struct VkShaderModule_T;

struct ShaderModule
{
	ShaderModule() : m_shader_module(nullptr) {}

	bool Create(const char* code, const size_t& size);

	VkShaderModule_T* m_shader_module;
};

#endif

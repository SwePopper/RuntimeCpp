#ifndef SHADER_H
#define SHADER_H

#include "shader_module.h"
#include "vulkan/vulkan_renderer.h"

class Shader
{
public:
	Shader();
	~Shader() { Release(); }

	bool Initialize(ShaderModule* vertex_shader, ShaderModule* fragment_shader);
	void Release();

private:
	std::vector<vk::PipelineShaderStageCreateInfo> m_shader_stages;

	std::vector<vk::VertexInputBindingDescription> m_bindings;
	std::vector<vk::VertexInputAttributeDescription> m_attributes;
	vk::PipelineVertexInputStateCreateInfo m_input_state;
};

#endif

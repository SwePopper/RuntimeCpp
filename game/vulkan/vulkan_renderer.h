#ifndef VULKAN_H
#define VULKAN_H

typedef struct SDL_Window SDL_Window;

#include <vulkan/vulkan.hpp>
#include "math/bounds.h"

class Vulkan
{
public:
	struct Buffer
	{
		Buffer() : m_mapped_address(nullptr) {}

		vk::Buffer m_buffer;
		vk::DeviceMemory m_memory;
		vk::DescriptorBufferInfo m_descriptor;

		void* m_mapped_address;

		bool Map();
		void Unmap();
	};

	struct Texture
	{
		vk::Image m_image;
		vk::DeviceMemory m_image_memory;
		vk::Extent2D m_size;
		vk::DescriptorImageInfo m_descriptor;
	};

	struct Mesh
	{
		Buffer m_vertex_buffer;
		Buffer m_index_buffer;
		uint32_t m_index_type;
		uint32_t m_index_count;

		Bounds m_bounds;

		Texture m_albedo;
		Texture m_normal;
	};

	Vulkan();
	~Vulkan() { Release(); }

	bool Initialize(SDL_Window* window);
	void Release();

	bool BeginFrame(SDL_Window* window);
	void EndFrame(SDL_Window* window);

	void FlushAllFrames();

	vk::Device& GetDevice() { return m_device; }
	vk::DescriptorPool& GetDescriptorPool() { return m_descriptor_pool; }
	vk::RenderPass& GetRenderPass() { return m_render_pass; }
	uint32_t GetFrameCount() const { return static_cast<uint32_t>(m_swapchain_images.size()); }
	vk::CommandBuffer& GetCurrentCommandBuffer() { return m_command_buffers[m_current_frame]; }
	vk::Framebuffer& GetCurrentFramebuffer() { return m_frame_buffers[m_current_frame]; }

	vk::CommandBuffer CreateCommandBuffer(vk::CommandBufferLevel level, bool begin = true);
	void FlushCommandBuffer(vk::CommandBuffer commandBuffer, vk::Queue queue, bool free = true);

	vk::Queue& GetGraphicsQueue() { return m_graphics_queue; }

	const vk::Extent2D& GetSwapchainExtent() const { return m_swapchain_size; }

	Texture CreateImage(vk::ImageCreateInfo* info, void* data, vk::DeviceSize& data_size, uint8_t& bits_per_pixel);
	void DestroyImage(Texture& image);

	Mesh CreateMesh(void* vertex_data, uint32_t vertex_data_size, void* index_data, uint32_t index_data_size);
	void DestroyMesh(Mesh mesh);

	vk::Result CreateBuffer(Buffer& buffer, void* data, vk::DeviceSize size, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags memory_properties);
	void DestroyBuffer(Buffer& buffer);

	uint32_t GetMemoryType(uint32_t type_bits, vk::MemoryPropertyFlags properies);

	static const char* GetVulkanResultString(const vk::Result& result);

	const vk::Format& GetDepthFormat() const { return m_depth_format; }

private:
	bool CreateInstance(SDL_Window* window);
	bool CreateDevice();
	bool CreateSwapchain(SDL_Window* window);
	bool CreateSemaphore(vk::Semaphore* semaphore);
	void CreateCommandPool();
	void CreateCommandBuffers();
	void CreateFences();
	void CreateFramebuffers();
	void CreateRenderPass();
	void CreateDepthStencil();

	void DestroyRenderPass();
	void DestroyFramebuffers();
	void DestroyFences();
	void DestroyCommandBuffers();
	void DestroyCommandPool();
	void DestroySwapchain();
	void DestroySwapchain(bool destroy_swapchain);

	bool RecreateSwapchain(SDL_Window* window);

	bool GetPhysicalDevice();
	void GetQueues();
	void GetSurfaceCaps();

	void SetImageLayout(vk::CommandBuffer cmdBuffer, vk::Image image, vk::ImageAspectFlags aspectMask, vk::ImageLayout oldImageLayout,
						vk::ImageLayout newImageLayout, vk::ImageSubresourceRange subresourceRange,
						vk::PipelineStageFlags srcStageMask = vk::PipelineStageFlagBits::eAllCommands,
						vk::PipelineStageFlags dstStageMask = vk::PipelineStageFlagBits::eAllCommands);

	vk::Instance m_instance;
	vk::Device m_device;
	vk::SurfaceKHR m_surface;
	vk::SwapchainKHR m_swapchain;
	vk::PhysicalDeviceProperties m_physical_device_properties;
	vk::PhysicalDeviceFeatures m_physical_device_features;
	vk::PhysicalDeviceMemoryProperties m_physical_device_memory_properies;
	uint32_t m_graphics_queue_family_index;
	uint32_t m_present_queue_family_index;
	vk::PhysicalDevice m_physical_device;
	vk::Queue m_graphics_queue;
	vk::Queue m_present_queue;
	vk::Semaphore m_image_available_semaphore;
	vk::Semaphore m_rendering_finished_semaphore;
	vk::SurfaceCapabilitiesKHR m_surface_capabilities;
	std::vector<vk::SurfaceFormatKHR> m_surface_formats;
	uint32_t m_swapchain_desired_image_count;
	vk::SurfaceFormatKHR m_surface_format;
	vk::Extent2D m_swapchain_size;
	std::vector<vk::CommandPool> m_command_pools;
	std::vector<vk::Image> m_swapchain_images;
	std::vector<vk::ImageView> m_swapchain_image_views;
	std::vector<vk::Framebuffer> m_frame_buffers;
	std::vector<vk::CommandBuffer> m_command_buffers;
	std::vector<vk::Fence> m_fences;
	vk::DescriptorPool m_descriptor_pool;
	vk::RenderPass m_render_pass;

	vk::Image m_depth_stencil;
	vk::ImageView m_depth_stencil_view;
	vk::DeviceMemory m_depth_stencil_memory;
	vk::Format m_depth_format;

	uint32_t m_current_frame;
};

#endif

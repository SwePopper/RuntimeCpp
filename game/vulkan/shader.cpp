#include "platform.h"
#include "shader.h"

Shader::Shader() {}

bool Shader::Initialize(ShaderModule* vertex_shader, ShaderModule* fragment_shader)
{
	/*if(vertex_shader == nullptr || fragment_shader == nullptr) return false;

	m_shader_stages[0].stage = vk::ShaderStageFlagBits::eVertex;
	m_shader_stages[0].module = vertex_shader->m_shader_module;
	m_shader_stages[0].pName = "main";
	m_shader_stages[1].stage = vk::ShaderStageFlagBits::eFragment;
	m_shader_stages[1].module = fragment_shader->m_shader_module;
	m_shader_stages[1].pName = "main";

	m_bindings.resize(1);
	m_bindings[0].inputRate = vk::VertexInputRate::eVertex;
	m_bindings[0].binding = 0;
	m_bindings[0].stride = sizeof(Vertex);

	m_attributes.resize(4);
	m_attributes[0].binding = 0;
	m_attributes[0].format = vk::Format::eR32G32B32Sfloat;
	m_attributes[0].location = 0;
	m_attributes[0].offset = offsetof(Vertex, m_position);

	m_attributes[1].binding = 0;
	m_attributes[1].format = vk::Format::eR32G32B32Sfloat;
	m_attributes[1].location = 1;
	m_attributes[1].offset = offsetof(Vertex, m_normal);

	m_attributes[2].binding = 0;
	m_attributes[2].format = vk::Format::eR32G32Sfloat;
	m_attributes[2].location = 2;
	m_attributes[2].offset = offsetof(Vertex, m_uv);

	m_attributes[3].binding = 0;
	m_attributes[3].format = vk::Format::eR32G32B32A32Sfloat;
	m_attributes[3].location = 3;
	m_attributes[3].offset = offsetof(Vertex, m_color);

	m_input_state.vertexBindingDescriptionCount = static_cast<uint32_t>(m_bindings.size());
	m_input_state.pVertexBindingDescriptions = m_bindings.data();
	m_input_state.vertexAttributeDescriptionCount = static_cast<uint32_t>(m_attributes.size());
	m_input_state.pVertexAttributeDescriptions = m_attributes.data();

	std::vector<vk::DescriptorSetLayoutBinding> layout_bindings;
	layout_bindings.resize(2);

	layout_bindings[0].descriptorType = vk::DescriptorType::eUniformBuffer;
	layout_bindings[0].stageFlags = vk::ShaderStageFlagBits::eVertex;
	layout_bindings[0].binding = 0;
	layout_bindings[0].descriptorCount = 1;

	layout_bindings[1].descriptorType = vk::DescriptorType::eCombinedImageSampler;
	layout_bindings[1].stageFlags = vk::ShaderStageFlagBits::eFragment;
	layout_bindings[1].binding = 1;
	layout_bindings[1].descriptorCount = 1;

	vk::DescriptorSetLayoutCreateInfo descriptor_create;
	descriptor_create.pBindings = layout_bindings.data();
	descriptor_create.bindingCount = static_cast<uint32_t>(layout_bindings.size());

	Vulkan* vulkan = Game::GetInstance()->GetVulkan();
	vk::Device& device = vulkan->GetDevice();
	vk::Result result = device.createDescriptorSetLayout(&descriptor_create, nullptr, &m_descriptor_set_layout);
	if(vk::Result::eSuccess != result)
	{
		LogError("vkCreateDescriptorSetLayout: %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}

	vk::PipelineLayoutCreateInfo pipeline_create;
	pipeline_create.pSetLayouts = &m_descriptor_set_layout;
	pipeline_create.setLayoutCount = 1;

	result = device.createPipelineLayout(&pipeline_create, nullptr, &m_pipeline_layout);
	if(vk::Result::eSuccess != result)
	{
		LogError("vkCreatePipelineLayout: %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}

	vk::DescriptorSetAllocateInfo alloc_info;
	alloc_info.descriptorPool = vulkan->GetDescriptorPool();
	alloc_info.descriptorSetCount = 1;
	alloc_info.pSetLayouts = &m_descriptor_set_layout;
	result = device.allocateDescriptorSets(&alloc_info, &m_descriptor_set);
	if(vk::Result::eSuccess != result)
	{
		LogError("vkAllocateDescriptorSets(): %s\n", Vulkan::GetVulkanResultString(result));
		return false;
	}*/

	return true;
}

void Shader::Release() {}

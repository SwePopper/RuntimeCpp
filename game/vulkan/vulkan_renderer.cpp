#include "platform.h"
#include "vulkan_renderer.h"
#include "game.h"
#include "assets.h"

#include <SDL2/SDL_vulkan.h>

bool Vulkan::Buffer::Map()
{
	vk::MemoryMapFlags flags;
	vk::Device& device = Game::GetInstance()->GetVulkan()->GetDevice();
	vk::Result result = device.mapMemory(m_memory, 0, VK_WHOLE_SIZE, flags, &m_mapped_address);
	if(vk::Result::eSuccess != result) LogError("Failed to map memory %s\n", Vulkan::GetVulkanResultString(result));
	return vk::Result::eSuccess == result;
}

void Vulkan::Buffer::Unmap()
{
	if(m_mapped_address)
	{
		vk::Device& device = Game::GetInstance()->GetVulkan()->GetDevice();
		device.unmapMemory(m_memory);
		m_mapped_address = nullptr;
	}
}

const char* Vulkan::GetVulkanResultString(const vk::Result& result)
{
#define RESULT_CASE(res) \
	case res: return #res

	switch(result)
	{
		RESULT_CASE(vk::Result::eSuccess);
		RESULT_CASE(vk::Result::eNotReady);
		RESULT_CASE(vk::Result::eTimeout);
		RESULT_CASE(vk::Result::eEventSet);
		RESULT_CASE(vk::Result::eEventReset);
		RESULT_CASE(vk::Result::eIncomplete);
		RESULT_CASE(vk::Result::eErrorOutOfHostMemory);
		RESULT_CASE(vk::Result::eErrorOutOfDeviceMemory);
		RESULT_CASE(vk::Result::eErrorInitializationFailed);
		RESULT_CASE(vk::Result::eErrorDeviceLost);
		RESULT_CASE(vk::Result::eErrorMemoryMapFailed);
		RESULT_CASE(vk::Result::eErrorLayerNotPresent);
		RESULT_CASE(vk::Result::eErrorExtensionNotPresent);
		RESULT_CASE(vk::Result::eErrorFeatureNotPresent);
		RESULT_CASE(vk::Result::eErrorIncompatibleDriver);
		RESULT_CASE(vk::Result::eErrorTooManyObjects);
		RESULT_CASE(vk::Result::eErrorFormatNotSupported);
		RESULT_CASE(vk::Result::eErrorFragmentedPool);
		RESULT_CASE(vk::Result::eErrorSurfaceLostKHR);
		RESULT_CASE(vk::Result::eErrorNativeWindowInUseKHR);
		RESULT_CASE(vk::Result::eSuboptimalKHR);
		RESULT_CASE(vk::Result::eErrorOutOfDateKHR);
		RESULT_CASE(vk::Result::eErrorIncompatibleDisplayKHR);
		RESULT_CASE(vk::Result::eErrorValidationFailedEXT);
		RESULT_CASE(vk::Result::eErrorInvalidShaderNV);
		RESULT_CASE(vk::Result::eErrorOutOfPoolMemoryKHR);
#if VK_HEADER_VERSION == 51
		RESULT_CASE(vk::Result::eErrorInvalidExternalHandleKHX);
#elif VK_HEADER_VERSION == 64
		RESULT_CASE(vk::Result::eErrorInvalidExternalHandleKHR);
		RESULT_CASE(vk::Result::eErrorNotPermittedEXT);
#endif
	};
#undef RESULT_CASE
	return "vk::Result::eUnknown";
}

VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t obj, size_t location,
											 int32_t code, const char* layerPrefix, const char* msg, void* userData)
{
	UNUSED(flags);
	UNUSED(objType);
	UNUSED(userData);
	LogError("[VUL - %s] %s  -  obj: %llu  loc: %llu  code: %i\n", layerPrefix, msg, obj, location, code);
	return VK_FALSE;
}

Vulkan::Vulkan()
	: m_instance()
	, m_device()
	, m_surface()
	, m_swapchain()
	, m_physical_device_properties()
	, m_physical_device_features()
	, m_physical_device_memory_properies()
	, m_graphics_queue_family_index(0)
	, m_present_queue_family_index(0)
	, m_physical_device()
	, m_graphics_queue()
	, m_present_queue()
	, m_image_available_semaphore()
	, m_rendering_finished_semaphore()
	, m_surface_capabilities()
	, m_surface_formats()
	, m_swapchain_desired_image_count(0)
	, m_surface_format()
	, m_swapchain_size()
	, m_descriptor_pool()
	, m_render_pass()
	, m_current_frame(0)
{
}

bool Vulkan::Initialize(SDL_Window* window)
{
	if(SDL_Vulkan_LoadLibrary(nullptr) != 0)
	{
		LogError("SDL_Vulkan_LoadLibrary failed\n");
		return false;
	}

	SDL_DisplayMode mode;
	int dw, dh;
	SDL_GetCurrentDisplayMode(0, &mode);
	LogInfo("Screen BPP    : %d\n", SDL_BITSPERPIXEL(mode.format));
	SDL_GetWindowSize(window, &dw, &dh);
	LogInfo("Window Size   : %d,%d\n", dw, dh);
	SDL_Vulkan_GetDrawableSize(window, &dw, &dh);
	LogInfo("Draw Size     : %d,%d\n", dw, dh);
	LogInfo("\n");

	if(!CreateInstance(window))
	{
		LogError("Failed to create vulkan instance!\n");
		return false;
	}

	VkSurfaceKHR surface = nullptr;
	if(!SDL_Vulkan_CreateSurface(window, m_instance, &surface))
	{
		LogError("SDL_Vulkan_CreateSurface: %s\n", SDL_GetError());
		return false;
	}
	m_surface = surface;

	if(!GetPhysicalDevice())
	{
		LogError("Failed to get physical device!");
		return false;
	}

	if(!CreateDevice())
	{
		LogError("Failed to create device!");
		return false;
	}

	GetQueues();

	if(!CreateSemaphore(&m_image_available_semaphore) || !CreateSemaphore(&m_rendering_finished_semaphore))
	{
		LogError("Failed to create semaphores!");
		return false;
	}

	if(!RecreateSwapchain(window))
	{
		LogError("Failed to create swapchain!");
		return false;
	}

	{
		vk::DescriptorPoolSize pool_size[11] = {{vk::DescriptorType::eSampler, 1000},
												{vk::DescriptorType::eCombinedImageSampler, 1000},
												{vk::DescriptorType::eSampledImage, 1000},
												{vk::DescriptorType::eStorageImage, 1000},
												{vk::DescriptorType::eUniformTexelBuffer, 1000},
												{vk::DescriptorType::eStorageTexelBuffer, 1000},
												{vk::DescriptorType::eUniformBuffer, 1000},
												{vk::DescriptorType::eStorageBuffer, 1000},
												{vk::DescriptorType::eUniformBufferDynamic, 1000},
												{vk::DescriptorType::eStorageBufferDynamic, 1000},
												{vk::DescriptorType::eInputAttachment, 1000}};

		vk::DescriptorPoolCreateInfo pool_info;
		pool_info.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;
		pool_info.maxSets = 1000 * 11;
		pool_info.poolSizeCount = 11;
		pool_info.pPoolSizes = pool_size;
		vk::Result result = m_device.createDescriptorPool(&pool_info, nullptr, &m_descriptor_pool);
		if(vk::Result::eSuccess != result)
		{
			LogError("Failed to create descriptor pool: %s\n", GetVulkanResultString(result));
			return false;
		}
	}

	LogInfo("Vulkan initialized\n");
	return true;
}

bool Vulkan::CreateInstance(SDL_Window* window)
{
	vk::ApplicationInfo app_info;
	app_info.apiVersion = VK_API_VERSION_1_0;

	vk::InstanceCreateInfo instance_info;
	instance_info.pApplicationInfo = &app_info;

	uint32_t extension_count = 0;
	if(!SDL_Vulkan_GetInstanceExtensions(window, &extension_count, nullptr))
	{
		LogError("SDL_Vulkan_GetInstanceExtensions: %s\n", SDL_GetError());
		return false;
	}

	std::vector<const char*> extensions;
	extensions.resize(extension_count);

	if(!SDL_Vulkan_GetInstanceExtensions(window, &extension_count, &extensions[0]))
	{
		LogError("SDL_Vulkan_GetInstanceExtensions: %s\n", SDL_GetError());
		return false;
	}

	instance_info.enabledExtensionCount = extension_count;
	instance_info.ppEnabledExtensionNames = &extensions[0];

	// Debug stuff
	typedef std::vector<vk::LayerProperties> LayerProperties;
	LayerProperties layer_props = vk::enumerateInstanceLayerProperties();
	typedef std::vector<const char*> LayerNames;

	LayerNames wanted_layers;
	wanted_layers.push_back("VK_LAYER_LUNARG_standard_validation");
	wanted_layers.push_back("VK_LAYER_LUNARG_core_validation");
	wanted_layers.push_back("VK_LAYER_LUNARG_image");
	wanted_layers.push_back("VK_LAYER_LUNARG_parameter_validation");

	for(LayerNames::iterator wanted = wanted_layers.begin(); wanted != wanted_layers.end();)
	{
		bool found = false;
		for(LayerProperties::iterator it = layer_props.begin(); it != layer_props.end(); ++it)
		{
			if(strncmp(it->layerName, *wanted, VK_MAX_EXTENSION_NAME_SIZE) == 0)
			{
				found = true;
				break;
			}
		}

		if(!found)
		{
			wanted = wanted_layers.erase(wanted);
			if(wanted_layers.empty()) break;
		}
		else
		{
			++wanted;
		}
	}

	instance_info.enabledLayerCount = static_cast<uint32_t>(wanted_layers.size());
	instance_info.ppEnabledLayerNames = &wanted_layers[0];

	vk::Result result = vk::createInstance(&instance_info, nullptr, &m_instance);
	if(vk::Result::eSuccess != result)
	{
		LogError("vk::createInstance: %s\n", GetVulkanResultString(result));
		return false;
	}

	return true;
}

bool Vulkan::GetPhysicalDevice()
{
	uint32_t physical_device_count = 0;
	vk::Result result = m_instance.enumeratePhysicalDevices(&physical_device_count, nullptr);
	if(vk::Result::eSuccess != result)
	{
		LogError("vkEnumeratePhysicalDevices: %s\n", GetVulkanResultString(result));
		return false;
	}

	if(physical_device_count <= 0)
	{
		LogError("No vulkan physical device!\n");
		return false;
	}

	std::vector<vk::PhysicalDevice> physical_devices;
	physical_devices.resize(physical_device_count);

	result = m_instance.enumeratePhysicalDevices(&physical_device_count, &physical_devices[0]);
	if(vk::Result::eSuccess != result)
	{
		LogError("vkEnumeratePhysicalDevices: %s\n", GetVulkanResultString(result));
		return false;
	}

	std::vector<vk::QueueFamilyProperties> queue_families_properties;
	for(uint32_t i = 0; i < physical_device_count; ++i)
	{
		vk::PhysicalDevice& device = physical_devices[i];
		device.getProperties(&m_physical_device_properties);

		if(VK_VERSION_MAJOR(m_physical_device_properties.apiVersion) < 1) continue;

		device.getFeatures(&m_physical_device_features);
		device.getMemoryProperties(&m_physical_device_memory_properies);

		queue_families_properties = device.getQueueFamilyProperties();
		if(queue_families_properties.size() == 0) continue;

		m_present_queue_family_index = m_graphics_queue_family_index = static_cast<uint32_t>(queue_families_properties.size());
		for(uint32_t j = 0; j < queue_families_properties.size(); ++j)
		{
			vk::Bool32 supported = 0;

			if(queue_families_properties[j].queueCount == 0) continue;
			if(queue_families_properties[j].queueFlags & vk::QueueFlagBits::eGraphics) m_graphics_queue_family_index = j;

			result = device.getSurfaceSupportKHR(j, m_surface, &supported);
			if(vk::Result::eSuccess != result)
			{
				LogError("device.getSurfaceSupportKHR: %s\n", GetVulkanResultString(result));
				return false;
			}

			if(supported)
			{
				m_present_queue_family_index = j;
				if(queue_families_properties[j].queueFlags & vk::QueueFlagBits::eGraphics) break;
			}
		}

		if(m_graphics_queue_family_index >= queue_families_properties.size()) continue;
		if(m_present_queue_family_index >= queue_families_properties.size()) continue;

		std::vector<vk::ExtensionProperties> device_extensions = device.enumerateDeviceExtensionProperties();
		if(device_extensions.size() == 0) continue;

		bool has_swapchain = false;
		for(size_t j = 0; j < device_extensions.size(); ++j)
		{
			if(0 == strcmp(device_extensions[j].extensionName, VK_KHR_SWAPCHAIN_EXTENSION_NAME))
			{
				has_swapchain = true;
				break;
			}
		}

		if(!has_swapchain) continue;
		m_physical_device = device;
		break;
	}

	if(!m_physical_device)
	{
		LogError("Failed to get vulkan physical device!");
		return false;
	}

	return true;
}

bool Vulkan::CreateDevice()
{
	constexpr float queue_priority[]{1.0f};
	std::vector<const char*> device_extensions{VK_KHR_SWAPCHAIN_EXTENSION_NAME};

	vk::DeviceQueueCreateInfo device_queue_info;
	device_queue_info.pQueuePriorities = queue_priority;
	device_queue_info.queueCount = 1;
	device_queue_info.queueFamilyIndex = m_graphics_queue_family_index;

	vk::DeviceCreateInfo device_info;
	device_info.queueCreateInfoCount = 1;
	device_info.pQueueCreateInfos = &device_queue_info;
	device_info.enabledExtensionCount = static_cast<uint32_t>(device_extensions.size());
	device_info.ppEnabledExtensionNames = &device_extensions[0];
	device_info.pEnabledFeatures = &m_physical_device_features;

	vk::Result result = m_physical_device.createDevice(&device_info, nullptr, &m_device);
	if(vk::Result::eSuccess != result)
	{
		LogError("Failed to create device: %s\n", GetVulkanResultString(result));
		return false;
	}

	return true;
}

void Vulkan::GetQueues()
{
	m_device.getQueue(m_graphics_queue_family_index, 0, &m_graphics_queue);
	if(m_graphics_queue_family_index != m_present_queue_family_index)
	{
		m_device.getQueue(m_present_queue_family_index, 0, &m_present_queue);
	}
	else
	{
		m_present_queue = m_graphics_queue;
	}
}

bool Vulkan::CreateSwapchain(SDL_Window* window)
{
	// pick an image count
	m_swapchain_desired_image_count = m_surface_capabilities.minImageCount + 1;
	if(m_swapchain_desired_image_count > m_surface_capabilities.maxImageCount && m_surface_capabilities.maxImageCount > 0)
		m_swapchain_desired_image_count = m_surface_capabilities.maxImageCount;

	// pick a format
	if(m_surface_formats.size() == 1 && m_surface_formats[0].format == vk::Format::eUndefined)
	{
		// aren't any preferred formats, so we pick
		m_surface_format.colorSpace = vk::ColorSpaceKHR::eSrgbNonlinear;
		m_surface_format.format = vk::Format::eR8G8B8A8Unorm;
	}
	else
	{
		m_surface_format = m_surface_formats[0];
		for(uint32_t i = 0; i < m_surface_formats.size(); ++i)
		{
			if(m_surface_formats[i].format == vk::Format::eR8G8B8A8Unorm)
			{
				m_surface_format = m_surface_formats[i];
				break;
			}
			else if(m_surface_formats[i].format == vk::Format::eB8G8R8A8Unorm)
			{
				m_surface_format = m_surface_formats[i];
			}
		}
	}

	// get size
	int w, h;
	SDL_Vulkan_GetDrawableSize(window, &w, &h);
	m_swapchain_size.width = static_cast<uint32_t>(w);
	m_swapchain_size.height = static_cast<uint32_t>(h);
	if(w == 0 || h == 0) return false;

	{
		vk::SwapchainCreateInfoKHR info;
		info.surface = m_surface;
		info.minImageCount = m_swapchain_desired_image_count;
		info.imageFormat = m_surface_format.format;
		info.imageColorSpace = m_surface_format.colorSpace;
		info.imageExtent = m_swapchain_size;
		info.imageArrayLayers = 1;
		info.imageUsage = vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eTransferDst;
		info.imageSharingMode = vk::SharingMode::eExclusive;
		info.preTransform = m_surface_capabilities.currentTransform;
		info.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
		info.presentMode = vk::PresentModeKHR::eFifo;
		info.clipped = true;
		info.oldSwapchain = m_swapchain;

		vk::Result result = m_device.createSwapchainKHR(&info, nullptr, &m_swapchain);
		if(info.oldSwapchain) m_device.destroySwapchainKHR(info.oldSwapchain, nullptr);
		if(vk::Result::eSuccess != result)
		{
			m_swapchain = nullptr;
			LogError("vkCreateSwapchainKHR(): %s\n", GetVulkanResultString(result));
			return false;
		}
	}
	m_swapchain_images = m_device.getSwapchainImagesKHR(m_swapchain);

	m_swapchain_image_views.resize(m_swapchain_images.size());
	for(uint32_t i = 0; i < m_swapchain_image_views.size(); ++i)
	{
		vk::ImageViewCreateInfo info;
		info.image = m_swapchain_images[i];
		info.viewType = vk::ImageViewType::e2D;
		info.format = m_surface_format.format;
		info.components.r = vk::ComponentSwizzle::eIdentity;
		info.components.g = vk::ComponentSwizzle::eIdentity;
		info.components.b = vk::ComponentSwizzle::eIdentity;
		info.components.a = vk::ComponentSwizzle::eIdentity;
		info.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
		info.subresourceRange.baseMipLevel = 0;
		info.subresourceRange.levelCount = 1;
		info.subresourceRange.baseArrayLayer = 0;
		info.subresourceRange.layerCount = 1;

		vk::Result result = m_device.createImageView(&info, nullptr, &m_swapchain_image_views[i]);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkCreateImageView(): %s\n", GetVulkanResultString(result));
			return false;
		}
	}

	std::vector<vk::Format> depth_formats = {vk::Format::eD32SfloatS8Uint, vk::Format::eD32Sfloat, vk::Format::eD24UnormS8Uint,
											 vk::Format::eD16UnormS8Uint, vk::Format::eD16Unorm};

	m_depth_format = vk::Format::eUndefined;
	for(uint32_t i = 0; i < depth_formats.size(); ++i)
	{
		vk::FormatProperties format_props;
		m_physical_device.getFormatProperties(depth_formats[i], &format_props);
		// Format must support depth stencil attachment for optimal tiling
		if(format_props.optimalTilingFeatures & vk::FormatFeatureFlagBits::eDepthStencilAttachment)
		{
			m_depth_format = depth_formats[i];
			break;
		}
	}

	CreateRenderPass();
	CreateDepthStencil();
	CreateFramebuffers();
	return true;
}

bool Vulkan::CreateSemaphore(vk::Semaphore* semaphore)
{
	vk::SemaphoreCreateInfo info;
	vk::Result result = m_device.createSemaphore(&info, nullptr, semaphore);
	if(vk::Result::eSuccess != result)
	{
		LogError("Failed to create semaphore: %s\n", GetVulkanResultString(result));
		return false;
	}
	return true;
}

void Vulkan::CreateCommandPool()
{
	m_command_pools.resize(m_swapchain_images.size());
	for(uint32_t i = 0; i < m_command_pools.size(); ++i)
	{
		vk::CommandPoolCreateInfo createInfo;
		createInfo.flags = vk::CommandPoolCreateFlagBits::eResetCommandBuffer | vk::CommandPoolCreateFlagBits::eTransient;
		createInfo.queueFamilyIndex = m_graphics_queue_family_index;

		vk::Result result = m_device.createCommandPool(&createInfo, nullptr, &m_command_pools[i]);
		if(vk::Result::eSuccess != result)
		{
			m_command_pools[i] = nullptr;
			LogError("vkCreateCommandPool(): %s\n", GetVulkanResultString(result));
		}
	}
}

void Vulkan::CreateCommandBuffers()
{
	m_command_buffers.resize(m_swapchain_images.size());
	for(uint32_t i = 0; i < m_command_buffers.size(); ++i)
	{
		vk::CommandBufferAllocateInfo allocateInfo;
		allocateInfo.commandPool = m_command_pools[i];
		allocateInfo.level = vk::CommandBufferLevel::ePrimary;
		allocateInfo.commandBufferCount = 1;

		vk::Result result = m_device.allocateCommandBuffers(&allocateInfo, &m_command_buffers[i]);
		if(vk::Result::eSuccess != result)
		{
			m_command_buffers[i] = nullptr;
			LogError("vkAllocateCommandBuffers(): %s\n", GetVulkanResultString(result));
		}
	}
}

void Vulkan::CreateFences()
{
	m_fences.resize(m_swapchain_images.size());
	for(size_t i = 0; i < m_swapchain_images.size(); ++i)
	{
		vk::FenceCreateInfo createInfo;
		createInfo.flags = vk::FenceCreateFlagBits::eSignaled;

		vk::Result result = m_device.createFence(&createInfo, nullptr, &m_fences[i]);
		if(vk::Result::eSuccess != result)
		{
			for(; i > 0; i--)
			{
				m_device.destroyFence(m_fences[i], nullptr);
			}
			m_fences.clear();
			LogError("vkCreateFence(): %s\n", GetVulkanResultString(result));
		}
	}
}

void Vulkan::CreateRenderPass()
{
	std::array<vk::AttachmentDescription, 2> attachment;
	for(uint32_t i = 0; i < attachment.size(); ++i)
	{
		attachment[i].samples = vk::SampleCountFlagBits::e1;
		attachment[i].loadOp = vk::AttachmentLoadOp::eDontCare;
		attachment[i].storeOp = vk::AttachmentStoreOp::eStore;
		attachment[i].stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
		attachment[i].stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
		attachment[i].initialLayout = vk::ImageLayout::eUndefined;
	}

	attachment[0].format = m_surface_format.format;
	attachment[0].finalLayout = vk::ImageLayout::ePresentSrcKHR;
	attachment[1].format = m_depth_format;
	attachment[1].finalLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal;
	attachment[1].loadOp = vk::AttachmentLoadOp::eClear;

	vk::AttachmentReference color_attachment;
	color_attachment.attachment = 0;
	color_attachment.layout = vk::ImageLayout::eColorAttachmentOptimal;

	vk::AttachmentReference depth_attachment;
	depth_attachment.attachment = 1;
	depth_attachment.layout = vk::ImageLayout::eDepthStencilAttachmentOptimal;

	vk::SubpassDescription subpass;
	subpass.pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &color_attachment;
	subpass.pDepthStencilAttachment = &depth_attachment;

	vk::SubpassDependency dependencies[2];
	dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
	dependencies[0].dstSubpass = 0;
	dependencies[0].srcStageMask = vk::PipelineStageFlagBits::eBottomOfPipe;
	dependencies[0].dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	dependencies[0].srcAccessMask = vk::AccessFlagBits::eMemoryRead;
	dependencies[0].dstAccessMask = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;
	dependencies[0].dependencyFlags = vk::DependencyFlagBits::eByRegion;

	dependencies[1].srcSubpass = 0;
	dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
	dependencies[1].srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	dependencies[1].dstStageMask = vk::PipelineStageFlagBits::eBottomOfPipe;
	dependencies[1].srcAccessMask = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;
	dependencies[1].dstAccessMask = vk::AccessFlagBits::eMemoryRead;
	dependencies[1].dependencyFlags = vk::DependencyFlagBits::eByRegion;

	vk::RenderPassCreateInfo info;
	info.attachmentCount = attachment.size();
	info.pAttachments = attachment.data();
	info.subpassCount = 1;
	info.pSubpasses = &subpass;
	info.dependencyCount = 2;
	info.pDependencies = dependencies;

	vk::Result result = m_device.createRenderPass(&info, nullptr, &m_render_pass);
	if(vk::Result::eSuccess != result)
	{
		LogError("vkCreateRenderPass(): %s\n", GetVulkanResultString(result));
	}
}

void Vulkan::CreateDepthStencil()
{
	if(m_depth_format == vk::Format::eUndefined) return;

	vk::ImageCreateInfo create_image;
	create_image.imageType = vk::ImageType::e2D;
	create_image.format = m_depth_format;
	create_image.extent.width = m_swapchain_size.width;
	create_image.extent.height = m_swapchain_size.height;
	create_image.extent.depth = 1;
	create_image.mipLevels = 1;
	create_image.arrayLayers = 1;
	create_image.samples = vk::SampleCountFlagBits::e1;
	create_image.tiling = vk::ImageTiling::eOptimal;
	create_image.usage = vk::ImageUsageFlagBits::eDepthStencilAttachment | vk::ImageUsageFlagBits::eTransferSrc;

	vk::Result result = m_device.createImage(&create_image, nullptr, &m_depth_stencil);
	if(vk::Result::eSuccess != result)
	{
		LogError("Failed to create depth stencil image: %s\n", GetVulkanResultString(result));
		return;
	}

	vk::MemoryRequirements memory_requirements = m_device.getImageMemoryRequirements(m_depth_stencil);
	vk::MemoryAllocateInfo memory_info;
	memory_info.allocationSize = memory_requirements.size;
	memory_info.memoryTypeIndex = GetMemoryType(memory_requirements.memoryTypeBits, vk::MemoryPropertyFlagBits::eDeviceLocal);

	result = m_device.allocateMemory(&memory_info, nullptr, &m_depth_stencil_memory);
	if(vk::Result::eSuccess != result)
	{
		LogError("Failed to create depth stencil memory: %s\n", GetVulkanResultString(result));
		return;
	}

	m_device.bindImageMemory(m_depth_stencil, m_depth_stencil_memory, 0);

	vk::ImageViewCreateInfo create_image_view;
	create_image_view.viewType = vk::ImageViewType::e2D;
	create_image_view.format = m_depth_format;
	create_image_view.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil;
	create_image_view.subresourceRange.baseMipLevel = 0;
	create_image_view.subresourceRange.levelCount = 1;
	create_image_view.subresourceRange.baseArrayLayer = 0;
	create_image_view.subresourceRange.layerCount = 1;
	create_image_view.image = m_depth_stencil;

	result = m_device.createImageView(&create_image_view, nullptr, &m_depth_stencil_view);
	if(vk::Result::eSuccess != result)
	{
		LogError("Failed to create depth stencil view: %s\n", GetVulkanResultString(result));
		return;
	}
}

void Vulkan::CreateFramebuffers()
{
	vk::ImageView attachment[2];
	attachment[1] = m_depth_stencil_view;

	vk::FramebufferCreateInfo info;
	info.renderPass = m_render_pass;
	info.attachmentCount = 2;
	info.pAttachments = attachment;
	info.width = m_swapchain_size.width;
	info.height = m_swapchain_size.height;
	info.layers = 1;

	m_frame_buffers.resize(m_swapchain_images.size());
	for(uint32_t i = 0; i < m_frame_buffers.size(); ++i)
	{
		attachment[0] = m_swapchain_image_views[i];

		vk::Result result = m_device.createFramebuffer(&info, nullptr, &m_frame_buffers[i]);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkCreateFramebuffer(): %s\n", GetVulkanResultString(result));
		}
	}
}

vk::CommandBuffer Vulkan::CreateCommandBuffer(vk::CommandBufferLevel level, bool begin)
{
	vk::CommandBufferAllocateInfo info;
	info.level = level;
	info.commandBufferCount = 1;
	info.commandPool = m_command_pools[m_current_frame];

	vk::CommandBuffer command_buffer;
	vk::Result result = m_device.allocateCommandBuffers(&info, &command_buffer);
	if(vk::Result::eSuccess != result)
	{
		LogError("vkAllocateCommandBuffers(): %s\n", GetVulkanResultString(result));
		return nullptr;
	}

	if(begin)
	{
		vk::CommandBufferBeginInfo begin_info;
		result = command_buffer.begin(&begin_info);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkBeginCommandBuffer(): %s\n", GetVulkanResultString(result));
		}
	}
	return command_buffer;
}

void Vulkan::DestroyRenderPass()
{
	if(m_render_pass) m_device.destroyRenderPass(m_render_pass, nullptr);
	m_render_pass = nullptr;
}

void Vulkan::DestroyFramebuffers()
{
	if(m_frame_buffers.empty()) return;
	for(uint32_t i = 0; i < m_frame_buffers.size(); ++i)
	{
		m_device.destroyFramebuffer(m_frame_buffers[i], nullptr);
	}
	m_frame_buffers.clear();
}

void Vulkan::DestroyFences()
{
	if(m_fences.empty()) return;
	for(uint32_t i = 0; i < m_swapchain_images.size(); ++i)
	{
		m_device.destroyFence(m_fences[i], nullptr);
	}
	m_fences.clear();
}

void Vulkan::DestroyCommandBuffers()
{
	for(uint32_t i = 0; i < m_command_buffers.size(); ++i)
	{
		if(!m_command_buffers[i]) m_device.freeCommandBuffers(m_command_pools[i], 1, &m_command_buffers[i]);
	}
	m_command_buffers.clear();
}

void Vulkan::DestroyCommandPool()
{
	for(uint32_t i = 0; i < m_command_pools.size(); ++i)
	{
		if(m_command_pools[i]) m_device.destroyCommandPool(m_command_pools[i], nullptr);
	}
	m_command_pools.clear();
}

void Vulkan::DestroySwapchain()
{
	if(m_swapchain) m_device.destroySwapchainKHR(m_swapchain, nullptr);
	m_swapchain = nullptr;
	m_swapchain_images.clear();

	for(uint32_t i = 0; i < m_swapchain_image_views.size(); ++i)
	{
		m_device.destroyImageView(m_swapchain_image_views[i], nullptr);
	}
	m_swapchain_image_views.clear();
}

void Vulkan::DestroySwapchain(bool destroy_swapchain)
{
	DestroyRenderPass();
	DestroyFramebuffers();

	if(m_depth_stencil)
	{
		m_device.destroyImage(m_depth_stencil);
		m_depth_stencil = nullptr;
	}

	if(m_depth_stencil_view)
	{
		m_device.destroyImageView(m_depth_stencil_view, nullptr);
		m_depth_stencil_view = nullptr;
	}

	if(m_depth_stencil_memory)
	{
		m_device.freeMemory(m_depth_stencil_memory);
		m_depth_stencil_memory = nullptr;
	}

	DestroyFences();
	DestroyCommandBuffers();
	DestroyCommandPool();
	if(destroy_swapchain) DestroySwapchain();
}

bool Vulkan::RecreateSwapchain(SDL_Window* window)
{
	DestroySwapchain(false);
	GetSurfaceCaps();
	m_surface_formats = m_physical_device.getSurfaceFormatsKHR(m_surface);
	if(!CreateSwapchain(window)) return false;
	CreateCommandPool();
	CreateCommandBuffers();
	CreateFences();
	return true;
}

void Vulkan::FlushCommandBuffer(vk::CommandBuffer commandBuffer, vk::Queue queue, bool free)
{
	if(!commandBuffer) return;
	commandBuffer.end();

	vk::SubmitInfo info;
	info.commandBufferCount = 1;
	info.pCommandBuffers = &commandBuffer;

	vk::FenceCreateInfo fence_info;
	vk::Fence fence;

	vk::Result result = m_device.createFence(&fence_info, nullptr, &fence);
	if(vk::Result::eSuccess != result)
	{
		LogError("vkCreateFence(): %s\n", GetVulkanResultString(result));
		return;
	}

	queue.submit(1, &info, fence);

	result = m_device.waitForFences(1, &fence, true, std::numeric_limits<uint64_t>::max());
	if(vk::Result::eSuccess != result)
	{
		LogError("vkWaitForFences(): %s\n", GetVulkanResultString(result));
		return;
	}

	m_device.destroyFence(fence, nullptr);

	if(free)
	{
		m_device.freeCommandBuffers(m_command_pools[m_current_frame], 1, &commandBuffer);
	}
}

void Vulkan::GetSurfaceCaps()
{
	vk::Result result = m_physical_device.getSurfaceCapabilitiesKHR(m_surface, &m_surface_capabilities);
	if(vk::Result::eSuccess != result)
	{
		LogError("vkGetPhysicalDeviceSurfaceCapabilitiesKHR(): %s\n", GetVulkanResultString(result));
		return;
	}

	// check surface usage
	if(!(m_surface_capabilities.supportedUsageFlags & vk::ImageUsageFlagBits::eTransferDst))
	{
		LogError("Vulkan surface doesn't support VK_IMAGE_USAGE_TRANSFER_DST_BIT\n");
		return;
	}
}

uint32_t Vulkan::GetMemoryType(uint32_t type_bits, vk::MemoryPropertyFlags properies)
{
	for(uint32_t i = 0; i < m_physical_device_memory_properies.memoryTypeCount; ++i)
	{
		if(CHECK_BIT(type_bits, i))
		{
			if((m_physical_device_memory_properies.memoryTypes[i].propertyFlags & properies) == properies)
			{
				return i;
			}
		}
	}
	return std::numeric_limits<uint32_t>::max();
}

// https://github.com/SaschaWillems/Vulkan/blob/6330ed9d94f7c1753bd8a6d00ff3168dbcef9139/base/VulkanTools.cpp
void Vulkan::SetImageLayout(vk::CommandBuffer cmdBuffer, vk::Image image, vk::ImageAspectFlags aspectMask, vk::ImageLayout oldImageLayout,
							vk::ImageLayout newImageLayout, vk::ImageSubresourceRange subresourceRange, vk::PipelineStageFlags srcStageMask,
							vk::PipelineStageFlags dstStageMask)
{
	vk::ImageMemoryBarrier image_memory_barrier;
	image_memory_barrier.oldLayout = oldImageLayout;
	image_memory_barrier.newLayout = newImageLayout;
	image_memory_barrier.image = image;
	image_memory_barrier.subresourceRange = subresourceRange;

	// Source layouts (old)
	// Source access mask controls actions that have to be finished on the old layout
	// before it will be transitioned to the new layout
	switch(oldImageLayout)
	{
	case vk::ImageLayout::eUndefined:
		// Image layout is undefined (or does not matter)
		// Only valid as initial layout
		// No flags required, listed only for completeness
		image_memory_barrier.srcAccessMask = static_cast<vk::AccessFlagBits>(0);
		break;

	case vk::ImageLayout::ePreinitialized:
		// Image is preinitialized
		// Only valid as initial layout for linear images, preserves memory contents
		// Make sure host writes have been finished
		image_memory_barrier.srcAccessMask = vk::AccessFlagBits::eHostWrite;
		break;

	case vk::ImageLayout::eColorAttachmentOptimal:
		// Image is a color attachment
		// Make sure any writes to the color buffer have been finished
		image_memory_barrier.srcAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;
		break;

	case vk::ImageLayout::eDepthStencilAttachmentOptimal:
		// Image is a depth/stencil attachment
		// Make sure any writes to the depth/stencil buffer have been finished
		image_memory_barrier.srcAccessMask = vk::AccessFlagBits::eDepthStencilAttachmentWrite;
		break;

	case vk::ImageLayout::eTransferSrcOptimal:
		// Image is a transfer source
		// Make sure any reads from the image have been finished
		image_memory_barrier.srcAccessMask = vk::AccessFlagBits::eTransferRead;
		break;

	case vk::ImageLayout::eTransferDstOptimal:
		// Image is a transfer destination
		// Make sure any writes to the image have been finished
		image_memory_barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
		break;

	case vk::ImageLayout::eShaderReadOnlyOptimal:
		// Image is read by a shader
		// Make sure any shader reads from the image have been finished
		image_memory_barrier.srcAccessMask = vk::AccessFlagBits::eShaderRead;
		break;
	default:
		// Other source layouts aren't handled (yet)
		break;
	}

	// Target layouts (new)
	// Destination access mask controls the dependency for the new image layout
	switch(newImageLayout)
	{
	case vk::ImageLayout::eTransferDstOptimal:
		// Image will be used as a transfer destination
		// Make sure any writes to the image have been finished
		image_memory_barrier.dstAccessMask = vk::AccessFlagBits::eTransferWrite;
		break;

	case vk::ImageLayout::eTransferSrcOptimal:
		// Image will be used as a transfer source
		// Make sure any reads from the image have been finished
		image_memory_barrier.dstAccessMask = vk::AccessFlagBits::eTransferRead;
		break;

	case vk::ImageLayout::eColorAttachmentOptimal:
		// Image will be used as a color attachment
		// Make sure any writes to the color buffer have been finished
		image_memory_barrier.dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;
		break;

	case vk::ImageLayout::eDepthStencilAttachmentOptimal:
		// Image layout will be used as a depth/stencil attachment
		// Make sure any writes to depth/stencil buffer have been finished
		image_memory_barrier.dstAccessMask = image_memory_barrier.dstAccessMask | vk::AccessFlagBits::eDepthStencilAttachmentWrite;
		break;

	case vk::ImageLayout::eShaderReadOnlyOptimal:
		// Image will be read in a shader (sampler, input attachment)
		// Make sure any writes to the image have been finished
		if(image_memory_barrier.srcAccessMask == static_cast<vk::AccessFlagBits>(0))
		{
			image_memory_barrier.srcAccessMask = vk::AccessFlagBits::eHostWrite | vk::AccessFlagBits::eTransferWrite;
		}
		image_memory_barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;
		break;
	default:
		// Other source layouts aren't handled (yet)
		break;
	}

	// Put barrier inside setup command buffer
	cmdBuffer.pipelineBarrier(srcStageMask, dstStageMask, static_cast<vk::DependencyFlagBits>(0), 0, nullptr, 0, nullptr, 1, &image_memory_barrier);
}

void Vulkan::Release()
{
	if(m_descriptor_pool) m_device.destroyDescriptorPool(m_descriptor_pool, nullptr);
	m_descriptor_pool = nullptr;

	DestroySwapchain(true);

	if(m_rendering_finished_semaphore) m_device.destroySemaphore(m_rendering_finished_semaphore, nullptr);
	m_rendering_finished_semaphore = nullptr;

	if(m_image_available_semaphore) m_device.destroySemaphore(m_image_available_semaphore, nullptr);
	m_image_available_semaphore = nullptr;

	m_graphics_queue = nullptr;
	m_graphics_queue_family_index = 0;
	m_present_queue = nullptr;
	m_present_queue_family_index = 0;

	if(m_device) m_device.destroy(nullptr);
	m_device = nullptr;

	m_physical_device = nullptr;

	if(m_surface) m_instance.destroySurfaceKHR(m_surface, nullptr);
	m_surface = nullptr;

	if(m_instance) m_instance.destroy(nullptr);
	m_instance = nullptr;

	SDL_Vulkan_UnloadLibrary();

	m_swapchain_desired_image_count = 0;
	m_current_frame = 0;
	m_physical_device_properties = vk::PhysicalDeviceProperties();
	m_physical_device_features = vk::PhysicalDeviceFeatures();
	m_physical_device_memory_properies = vk::PhysicalDeviceMemoryProperties();
	m_surface_capabilities = vk::SurfaceCapabilitiesKHR();
	m_surface_format = vk::SurfaceFormatKHR();
	m_swapchain_size = vk::Extent2D();

	m_surface_formats.clear();
}

bool Vulkan::BeginFrame(SDL_Window* window)
{
	if(!m_swapchain)
	{
		bool result = RecreateSwapchain(window);
		if(!result) return false;
	}

	vk::Result result =
		m_device.acquireNextImageKHR(m_swapchain, std::numeric_limits<uint64_t>::max(), m_image_available_semaphore, nullptr, &m_current_frame);
	if(vk::Result::eSuccess != result)
	{
		if(vk::Result::eErrorOutOfDateKHR == result)
		{
			RecreateSwapchain(window);
		}
		else
		{
			LogError("vkAcquireNextImageKHR(): %s\n", GetVulkanResultString(result));
		}
		return false;
	}

	result = m_device.waitForFences(1, &m_fences[m_current_frame], false, std::numeric_limits<uint64_t>::max());
	if(vk::Result::eSuccess != result && vk::Result::eTimeout != result)
	{
		LogError("vkWaitForFences(): %s\n", GetVulkanResultString(result));
		return false;
	}

	result = m_device.resetFences(1, &m_fences[m_current_frame]);
	if(vk::Result::eSuccess != result)
	{
		LogError("vkResetFences(): %s\n", GetVulkanResultString(result));
		return false;
	}

	vk::CommandPoolResetFlags flags;
	m_device.resetCommandPool(m_command_pools[m_current_frame], flags);

	vk::CommandBufferBeginInfo begin;
	begin.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit | vk::CommandBufferUsageFlagBits::eSimultaneousUse;
	begin.pInheritanceInfo = nullptr;

	result = m_command_buffers[m_current_frame].begin(&begin);
	if(vk::Result::eSuccess != result)
	{
		LogError("vkBeginCommandBuffer(): %s\n", GetVulkanResultString(result));
		return false;
	}

	return true;
}

void Vulkan::EndFrame(SDL_Window* window)
{
	m_command_buffers[m_current_frame].end();

	vk::SubmitInfo submit_info;
	submit_info.pNext = nullptr;

	vk::Semaphore wait_semaphores[] = {m_image_available_semaphore};
	vk::PipelineStageFlags wait_stages[] = {vk::PipelineStageFlagBits::eColorAttachmentOutput};
	submit_info.waitSemaphoreCount = 1;
	submit_info.pWaitSemaphores = wait_semaphores;
	submit_info.pWaitDstStageMask = wait_stages;

	submit_info.commandBufferCount = 1;
	submit_info.pCommandBuffers = &m_command_buffers[m_current_frame];

	vk::Semaphore signal_semaphores[] = {m_rendering_finished_semaphore};
	submit_info.signalSemaphoreCount = 1;
	submit_info.pSignalSemaphores = signal_semaphores;

	vk::Result result = m_graphics_queue.submit(1, &submit_info, m_fences[m_current_frame]);
	if(vk::Result::eSuccess != result)
	{
		LogError("vkQueueSubmit(): %s\n", GetVulkanResultString(result));
		return;
	}

	vk::PresentInfoKHR present_info;
	present_info.waitSemaphoreCount = 1;
	present_info.pWaitSemaphores = &m_rendering_finished_semaphore;
	present_info.swapchainCount = 1;
	present_info.pSwapchains = &m_swapchain;
	present_info.pImageIndices = &m_current_frame;

	result = m_present_queue.presentKHR(&present_info);
	if(vk::Result::eErrorOutOfDateKHR == result || vk::Result::eSuboptimalKHR == result)
	{
		RecreateSwapchain(window);
		return;
	}

	if(vk::Result::eSuccess != result)
	{
		LogError("vkQueuePresentKHR(): %s\n", GetVulkanResultString(result));
		return;
	}

	int w = 0, h = 0;
	SDL_Vulkan_GetDrawableSize(window, &w, &h);
	if(static_cast<uint32_t>(w) != m_swapchain_size.width || static_cast<uint32_t>(h) != m_swapchain_size.height)
	{
		RecreateSwapchain(window);
	}
}

void Vulkan::FlushAllFrames()
{
	for(uint32_t i = 0; i < m_fences.size(); ++i)
	{
		vk::Result result = m_device.waitForFences(1, &m_fences[i], false, std::numeric_limits<uint64_t>::max());
		if(vk::Result::eSuccess != result && vk::Result::eTimeout != result)
		{
			LogError("vkWaitForFences(): %s\n", GetVulkanResultString(result));
			continue;
		}

		result = m_device.resetFences(1, &m_fences[i]);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkResetFences(): %s\n", GetVulkanResultString(result));
		}

		vk::CommandPoolResetFlags flags;
		m_device.resetCommandPool(m_command_pools[i], flags);
	}
}

Vulkan::Texture Vulkan::CreateImage(vk::ImageCreateInfo* info, void* data, vk::DeviceSize& data_size, uint8_t& bits_per_pixel)
{
	Texture texture;
	texture.m_size.width = info->extent.width;
	texture.m_size.height = info->extent.height;

	vk::FormatProperties props = m_physical_device.getFormatProperties(info->format);
	vk::FormatFeatureFlags no_feature;
	if(props.optimalTilingFeatures == no_feature)
	{
		LogError("Image Format %i unsupported!\n", info->format);
		return texture;
	}

	vk::Buffer staging_buffer;
	vk::DeviceMemory staging_memory;

	vk::BufferCreateInfo buffer_create_info;
	buffer_create_info.size = info->extent.width * info->extent.height * 4;
	buffer_create_info.usage = vk::BufferUsageFlagBits::eTransferSrc;
	buffer_create_info.sharingMode = vk::SharingMode::eExclusive;

	vk::Result result = m_device.createBuffer(&buffer_create_info, nullptr, &staging_buffer);
	if(vk::Result::eSuccess != result)
	{
		LogError("vkCreateBuffer(): %s\n", GetVulkanResultString(result));
		return texture;
	}

	vk::MemoryRequirements memory_requirements = m_device.getBufferMemoryRequirements(staging_buffer);

	vk::MemoryAllocateInfo memory_alloc_info;
	memory_alloc_info.allocationSize = memory_requirements.size;
	memory_alloc_info.memoryTypeIndex =
		GetMemoryType(memory_requirements.memoryTypeBits, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);

	result = m_device.allocateMemory(&memory_alloc_info, nullptr, &staging_memory);
	if(vk::Result::eSuccess != result)
	{
		LogError("vkAllocateMemory(): %s\n", GetVulkanResultString(result));
		return texture;
	}
	m_device.bindBufferMemory(staging_buffer, staging_memory, 0);

	vk::MemoryMapFlags flags;
	void* staging_data = nullptr;
	result = m_device.mapMemory(staging_memory, 0, memory_requirements.size, flags, &staging_data);
	if(vk::Result::eSuccess != result)
	{
		LogError("Failed to map memory!");
		return texture;
	}

	memcpy(staging_data, data, static_cast<size_t>(data_size));

	vk::MappedMemoryRange range[1];
	range[0].memory = staging_memory;
	range[0].size = memory_requirements.size;
	result = m_device.flushMappedMemoryRanges(1, range);
	if(vk::Result::eSuccess != result)
	{
		LogError("vkFlushMappedMemoryRanges(): %s\n", Vulkan::GetVulkanResultString(result));
		return texture;
	}

	m_device.unmapMemory(staging_memory);

	std::vector<vk::BufferImageCopy> buffer_copy_regions;
	uint32_t offset = 0;

	for(uint32_t i = 0; i < info->mipLevels; ++i)
	{
		vk::BufferImageCopy buffer_copy;
		buffer_copy.imageSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
		buffer_copy.imageSubresource.mipLevel = i;
		buffer_copy.imageSubresource.baseArrayLayer = 0;
		buffer_copy.imageSubresource.layerCount = 1;
		buffer_copy.imageExtent.width = static_cast<uint32_t>(info->extent.width >> i);
		buffer_copy.imageExtent.height = static_cast<uint32_t>(info->extent.height >> i);
		buffer_copy.imageExtent.depth = info->extent.depth;
		buffer_copy.bufferOffset = offset;

		buffer_copy_regions.push_back(buffer_copy);
		offset += static_cast<uint32_t>(buffer_copy.imageExtent.width * buffer_copy.imageExtent.height * bits_per_pixel);
	}

	{
		vk::Image image;
		result = m_device.createImage(info, nullptr, &image);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkCreateImage(): %s\n", GetVulkanResultString(result));
			return texture;
		}
		texture.m_image = image;
	}

	memory_requirements = m_device.getImageMemoryRequirements(texture.m_image);

	memory_alloc_info.allocationSize = memory_requirements.size;
	memory_alloc_info.memoryTypeIndex = GetMemoryType(memory_requirements.memoryTypeBits, vk::MemoryPropertyFlagBits::eDeviceLocal);

	{
		vk::DeviceMemory texture_memory;
		result = m_device.allocateMemory(&memory_alloc_info, nullptr, &texture_memory);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkAllocateMemory(): %s\n", GetVulkanResultString(result));
			return texture;
		}
		texture.m_image_memory = texture_memory;
	}
	m_device.bindImageMemory(texture.m_image, texture.m_image_memory, 0);

	vk::CommandBuffer copy_command = CreateCommandBuffer(vk::CommandBufferLevel::ePrimary, true);

	vk::ImageSubresourceRange sub_resource_range;
	sub_resource_range.aspectMask = vk::ImageAspectFlagBits::eColor;
	sub_resource_range.baseMipLevel = 0;
	sub_resource_range.levelCount = info->mipLevels;
	sub_resource_range.layerCount = 1;

	SetImageLayout(copy_command, texture.m_image, vk::ImageAspectFlagBits::eColor, vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal,
				   sub_resource_range);

	copy_command.copyBufferToImage(staging_buffer, texture.m_image, vk::ImageLayout::eTransferDstOptimal,
								   static_cast<uint32_t>(buffer_copy_regions.size()), &buffer_copy_regions[0]);

	vk::ImageLayout image_layout = vk::ImageLayout::eShaderReadOnlyOptimal;
	SetImageLayout(copy_command, texture.m_image, vk::ImageAspectFlagBits::eColor, vk::ImageLayout::eTransferDstOptimal, image_layout,
				   sub_resource_range);

	FlushCommandBuffer(copy_command, m_graphics_queue, true);

	m_device.freeMemory(staging_memory, nullptr);
	m_device.destroyBuffer(staging_buffer, nullptr);

	vk::ImageViewCreateInfo image_view_info;
	image_view_info.viewType = vk::ImageViewType::e2D;
	image_view_info.format = info->format;
	image_view_info.components =
		vk::ComponentMapping(vk::ComponentSwizzle::eR, vk::ComponentSwizzle::eG, vk::ComponentSwizzle::eB, vk::ComponentSwizzle::eA);
	image_view_info.subresourceRange = sub_resource_range;
	image_view_info.image = texture.m_image;

	{
		vk::ImageView image_view;
		result = m_device.createImageView(&image_view_info, nullptr, &image_view);
		if(vk::Result::eSuccess != result)
		{
			LogError("vkCreateImageView(): %s\n", GetVulkanResultString(result));
			return texture;
		}
		texture.m_descriptor.imageView = image_view;
	}

	vk::SamplerCreateInfo sampler_info;
	sampler_info.magFilter = vk::Filter::eLinear;
	sampler_info.minFilter = vk::Filter::eLinear;
	sampler_info.mipmapMode = vk::SamplerMipmapMode::eLinear;
	sampler_info.addressModeU = vk::SamplerAddressMode::eRepeat;
	sampler_info.addressModeV = vk::SamplerAddressMode::eRepeat;
	sampler_info.addressModeW = vk::SamplerAddressMode::eRepeat;
	sampler_info.mipLodBias = 0.0f;
	sampler_info.compareOp = vk::CompareOp::eNever;
	sampler_info.minLod = 0.0f;
	sampler_info.maxLod = info->mipLevels;
	sampler_info.maxAnisotropy = m_physical_device_features.samplerAnisotropy ? 8 : 1;
	sampler_info.anisotropyEnable = true;
	sampler_info.borderColor = vk::BorderColor::eFloatOpaqueWhite;

	vk::Sampler sampler;
	result = m_device.createSampler(&sampler_info, nullptr, &sampler);
	if(vk::Result::eSuccess != result)
	{
		LogError("Failed to create sampler: %s", GetVulkanResultString(result));
		return texture;
	}
	texture.m_descriptor.sampler = sampler;

	return texture;
}

void Vulkan::DestroyImage(Texture& image)
{
	if(image.m_descriptor.sampler) m_device.destroySampler(image.m_descriptor.sampler, nullptr);
	if(image.m_descriptor.imageView) m_device.destroyImageView(image.m_descriptor.imageView, nullptr);
	if(image.m_image) m_device.destroyImage(image.m_image, nullptr);
	if(image.m_image_memory) m_device.freeMemory(image.m_image_memory, nullptr);

	image.m_image = nullptr;
	image.m_image_memory = nullptr;
	image.m_descriptor = vk::DescriptorImageInfo();
	image.m_size = vk::Extent2D();
}

vk::Result Vulkan::CreateBuffer(Buffer& buffer, void* data, vk::DeviceSize size, vk::BufferUsageFlags usage,
								vk::MemoryPropertyFlags memory_properties)
{
	vk::BufferCreateInfo buffer_info;
	buffer_info.usage = usage;
	buffer_info.sharingMode = vk::SharingMode::eExclusive;
	buffer_info.size = size;
	vk::Result result = m_device.createBuffer(&buffer_info, nullptr, &buffer.m_buffer);
	if(vk::Result::eSuccess != result)
	{
		LogError("vkCreateBuffer(): %s\n", Vulkan::GetVulkanResultString(result));
		return result;
	}

	vk::MemoryRequirements req = m_device.getBufferMemoryRequirements(buffer.m_buffer);

	vk::MemoryAllocateInfo memoryAlloc;
	memoryAlloc.allocationSize = req.size;
	memoryAlloc.memoryTypeIndex = GetMemoryType(req.memoryTypeBits, memory_properties);
	result = m_device.allocateMemory(&memoryAlloc, nullptr, &buffer.m_memory);
	if(vk::Result::eSuccess != result)
	{
		m_device.destroyBuffer(buffer.m_buffer, nullptr);
		LogError("vkAllocateMemory(): %s\n", Vulkan::GetVulkanResultString(result));
		return result;
	}
	m_device.bindBufferMemory(buffer.m_buffer, buffer.m_memory, 0);

	if(data)
	{
		vk::MemoryMapFlags flags;
		void* mapped = m_device.mapMemory(buffer.m_memory, 0, buffer_info.size, flags);
		memcpy(mapped, data, buffer_info.size);
		m_device.unmapMemory(buffer.m_memory);
	}

	buffer.m_descriptor.buffer = buffer.m_buffer;
	buffer.m_descriptor.offset = 0;
	buffer.m_descriptor.range = size;

	return vk::Result::eSuccess;
}

void Vulkan::DestroyBuffer(Buffer& buffer)
{
	if(buffer.m_buffer)
	{
		m_device.destroyBuffer(buffer.m_buffer);
		buffer.m_buffer = nullptr;
	}

	if(buffer.m_memory)
	{
		m_device.freeMemory(buffer.m_memory);
		buffer.m_memory = nullptr;
	}
}

// https://github.com/SaschaWillems/Vulkan/blob/master/mesh/mesh.cpp
Vulkan::Mesh Vulkan::CreateMesh(void* vertex_data, uint32_t vertex_data_size, void* index_data, uint32_t index_data_size)
{
	Buffer vertex_buffer_staging;
	vk::Result result = CreateBuffer(vertex_buffer_staging, vertex_data, vertex_data_size, vk::BufferUsageFlagBits::eTransferSrc,
									 vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
	if(vk::Result::eSuccess != result) return Mesh();

	Buffer index_buffer_staging;
	result = CreateBuffer(index_buffer_staging, index_data, index_data_size, vk::BufferUsageFlagBits::eTransferSrc,
						  vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
	if(vk::Result::eSuccess != result)
	{
		DestroyBuffer(vertex_buffer_staging);
		return Mesh();
	}

	Buffer vertex_buffer;
	result = CreateBuffer(vertex_buffer, nullptr, vertex_data_size, vk::BufferUsageFlagBits::eVertexBuffer | vk::BufferUsageFlagBits::eTransferDst,
						  vk::MemoryPropertyFlagBits::eDeviceLocal);
	if(vk::Result::eSuccess != result)
	{
		DestroyBuffer(vertex_buffer_staging);
		DestroyBuffer(index_buffer_staging);
		return Mesh();
	}

	Buffer index_buffer;
	result = CreateBuffer(index_buffer, nullptr, index_data_size, vk::BufferUsageFlagBits::eIndexBuffer | vk::BufferUsageFlagBits::eTransferDst,
						  vk::MemoryPropertyFlagBits::eDeviceLocal);
	if(vk::Result::eSuccess != result)
	{
		DestroyBuffer(vertex_buffer_staging);
		DestroyBuffer(index_buffer_staging);
		DestroyBuffer(vertex_buffer);
		return Mesh();
	}

	vk::CommandBuffer command_buffer = CreateCommandBuffer(vk::CommandBufferLevel::ePrimary);
	if(command_buffer)
	{
		vk::BufferCopy copy_buffer;
		copy_buffer.size = vertex_data_size;
		command_buffer.copyBuffer(vertex_buffer_staging.m_buffer, vertex_buffer.m_buffer, 1, &copy_buffer);

		copy_buffer.size = index_data_size;
		command_buffer.copyBuffer(index_buffer_staging.m_buffer, index_buffer.m_buffer, 1, &copy_buffer);

		FlushCommandBuffer(command_buffer, m_graphics_queue);
	}
	else
	{
		DestroyBuffer(vertex_buffer);
		DestroyBuffer(index_buffer);
		index_buffer = vertex_buffer = Buffer();
		LogError("vkAllocateCommandBuffers(): %s\n", GetVulkanResultString(result));
	}

	DestroyBuffer(vertex_buffer_staging);
	DestroyBuffer(index_buffer_staging);

	Mesh mesh;
	mesh.m_vertex_buffer = vertex_buffer;
	mesh.m_index_buffer = index_buffer;
	return mesh;
}

void Vulkan::DestroyMesh(Mesh mesh)
{
	DestroyBuffer(mesh.m_vertex_buffer);
	DestroyBuffer(mesh.m_index_buffer);

	Game::GetInstance()->GetAssets()->UnloadTexture(mesh.m_albedo);
	Game::GetInstance()->GetAssets()->UnloadTexture(mesh.m_normal);
}

#include "platform.h"
#include "input.h"
#include "game.h"
#include "io/xml_reader.h"

#include <algorithm>

#include <imgui/imgui.h>

#include <SDL_mouse.h>
#include <SDL_keyboard.h>
#include <SDL_gamecontroller.h>

namespace
{
constexpr double kAxisNormalizer = 1.0 / 32767.0;
const InputState kNoneState;
}

Input::Input() { memset(m_controllers, 0, sizeof(m_controllers)); }

bool Input::Initialize()
{
	std::string file_path = Game::GetInstance()->GetAssetsFolder();
	file_path += "gamecontrollerdb.txt";

	if(SDL_GameControllerAddMappingsFromFile(file_path.c_str()) == -1)
	{
		LogError("Failed SDL_GameControllerAddMappingsFromFile: %s\n", SDL_GetError());
		return false;
	}

	struct XmlData
	{
		Input* m_input;
		Action m_current_action;
	};

	XmlData data{this, Action()};

	XmlReader::ReadXML("input_mappings.xml", &data,
					   [](void* user_data, const char* name, const char** atts) {
						   XmlData* data = reinterpret_cast<XmlData*>(user_data);
						   if(strncmp(name, "Action", 7) == 0)
						   {
							   const char* name = nullptr;
							   for(std::uint32_t i = 0; atts[i]; i += 2)
							   {
								   if(strncmp(atts[i], "name", 5) == 0)
								   {
									   name = atts[i + 1];
								   }
							   }
							   if(name)
							   {
								   data->m_current_action.m_action_name = name;
								   data->m_current_action.m_action_name_hash = Hash32(name);
								   data->m_current_action.m_action_buttons.clear();
								   data->m_current_action.m_action_states.resize(1);
							   }
						   }
						   else if(strncmp(name, "Button", 7) == 0)
						   {
							   std::uint32_t id = std::numeric_limits<std::uint32_t>::max();
							   for(std::uint32_t i = 0; atts[i]; i += 2)
							   {
								   if(strncmp(atts[i], "id", 3) == 0)
								   {
									   id = static_cast<std::uint32_t>(strtoull(atts[i + 1], nullptr, 0));
								   }
							   }
							   if(id < static_cast<std::uint32_t>(InputButton::kMaxInputButtons))
							   {
								   data->m_current_action.m_action_buttons.push_back(static_cast<InputButton>(id));
							   }
						   }
					   },
					   [](void* user_data, const char* name) {
						   XmlData* data = reinterpret_cast<XmlData*>(user_data);
						   if(strncmp(name, "Action", 7) == 0)
						   {
							   data->m_input->m_actions.push_back(data->m_current_action);
							   data->m_current_action = Action();
						   }
					   });

	return true;
}

void Input::Release()
{
	for(std::uint32_t i = 0; i < static_cast<std::uint32_t>(InputData::kMaxControllers); ++i)
	{
		if(m_controllers[i])
		{
			SDL_GameControllerClose(m_controllers[i]);
			m_controllers[i] = nullptr;
		}
	}

	FILE* mapping_file = fopen("input_mappings.xml", "w");
	File::Write(mapping_file, "<InputMappings>");
	{
		for(ActionContainer::const_iterator it = m_actions.cbegin(); it != m_actions.cend(); ++it)
		{
			File::Write(mapping_file, "<Action name=\"%s\">", it->m_action_name.c_str());
			for(Buttons::const_iterator button = it->m_action_buttons.cbegin(); button != it->m_action_buttons.cend(); ++button)
			{
				File::Write(mapping_file, "<Button id=\"%u\" />", static_cast<std::uint32_t>(*button));
			}
			File::Write(mapping_file, "</Action>");
		}
	}
	File::Write(mapping_file, "</InputMappings>");
	fclose(mapping_file);
}

void Input::Update()
{
	// Check for new or lost controllers
	for(int i = 0; i < SDL_NumJoysticks(); ++i)
	{
		if(!m_controllers[i] && SDL_IsGameController(i))
		{
			m_controllers[i] = SDL_GameControllerOpen(i);
			if(m_controllers[i])
			{
				// Gained controller
				LogInfo("Added \'%s\' controller on index: \'%i\'\n", SDL_GameControllerNameForIndex(i), i);
				char* mapping = SDL_GameControllerMapping(m_controllers[i]);
				LogInfo("Mapped as \"%s\"\n", mapping);
				SDL_free(mapping);
			}
			else
			{
				LogError("Failed SDL_GameControllerOpen: %s", SDL_GetError());
			}
		}
		else if(m_controllers[i] && !SDL_IsGameController(i))
		{
			// Lost controller
			SDL_GameControllerClose(m_controllers[i]);
			m_controllers[i] = nullptr;
		}
	}

	if(ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard)
	{
		// Ignore/clear input if imgui wants to play
		return;
	}

	if(!m_actions.empty())
	{
		int mouse_x = 0, mouse_y = 0;
		std::uint32_t mouse_buttons = SDL_GetMouseState(&mouse_x, &mouse_y);

		const glm::vec2& window_size = Game::GetInstance()->GetWindowSize();
		float x = static_cast<float>(mouse_x) / window_size.x;
		float y = static_cast<float>(mouse_y) / window_size.y;

		const Uint8* keyboard_states = SDL_GetKeyboardState(nullptr);

		for(ActionContainer::iterator it = m_actions.begin(); it != m_actions.end(); ++it)
		{
			if(it->m_action_states.empty())
			{
				it->m_action_states.resize(1);
			}

			for(InputStates::iterator state = it->m_action_states.begin(); state != it->m_action_states.end(); ++state)
			{
				state->prev = state->value;
				state->value = 0.0f;
			}

			for(Buttons::iterator button = it->m_action_buttons.begin(); button != it->m_action_buttons.end(); ++button)
			{
				if(*button < InputButton::kEndScancodes)
				{
					if(keyboard_states[static_cast<std::uint32_t>(*button)])
					{
						it->m_action_states[0].value = 1.0f;
						break;
					}
				}
				else if(*button < InputButton::kEndMouseButtons)
				{
					if(*button == InputButton::kMousePositionX)
					{
						it->m_action_states[0].value = x;
						break;
					}
					else if(*button == InputButton::kMousePositionY)
					{
						it->m_action_states[0].value = y;
						break;
					}
					else
					{
						std::uint32_t button_index =
							static_cast<std::uint32_t>(*button) - static_cast<std::uint32_t>(InputButton::kStartMouseButtons);
						if(mouse_buttons & SDL_BUTTON(button_index))
						{
							it->m_action_states[0].value = 1.0f;
							break;
						}
					}
				}
				else if(*button < InputButton::kEndControllerButtons)
				{
					it->m_action_states.resize(static_cast<std::uint32_t>(InputData::kMaxControllers));

					if(*button < InputButton::kControllerAxisLeftUp)
					{
						std::uint32_t button_index =
							static_cast<std::uint32_t>(*button) - static_cast<std::uint32_t>(InputButton::kStartControllerButtons);

						for(std::uint32_t controller = 0; controller < static_cast<std::uint32_t>(InputData::kMaxControllers); ++controller)
						{
							if(!m_controllers[controller]) continue;
							it->m_action_states[controller].value =
								SDL_GameControllerGetButton(m_controllers[controller], static_cast<SDL_GameControllerButton>(button_index));
						}
					}
					else
					{
						SDL_GameControllerAxis axis = SDL_CONTROLLER_AXIS_LEFTY;
						bool negate = false;
						switch(*button)
						{
						case InputButton::kControllerAxisLeftUp: negate = true; [[fallthrough]];
						case InputButton::kControllerAxisLeftDown: axis = SDL_CONTROLLER_AXIS_LEFTY; break;
						case InputButton::kControllerAxisLeftLeft: negate = true; [[fallthrough]];
						case InputButton::kControllerAxisLeftRight: axis = SDL_CONTROLLER_AXIS_LEFTX; break;
						case InputButton::kControllerAxisRightUp: negate = true; [[fallthrough]];
						case InputButton::kControllerAxisRightDown: axis = SDL_CONTROLLER_AXIS_RIGHTY; break;
						case InputButton::kControllerAxisRightLeft: negate = true; [[fallthrough]];
						case InputButton::kControllerAxisRightRight: axis = SDL_CONTROLLER_AXIS_RIGHTX; break;
						case InputButton::kControllerAxisTriggerLeft: axis = SDL_CONTROLLER_AXIS_TRIGGERLEFT; break;
						case InputButton::kControllerAxisTriggerRight: axis = SDL_CONTROLLER_AXIS_TRIGGERRIGHT; break;
						default: LogError("Should not be able to come here! =/");
						}

						for(std::uint32_t controller = 0; controller < static_cast<std::uint32_t>(InputData::kMaxControllers); ++controller)
						{
							if(!m_controllers[controller]) continue;

							int16_t button_value = SDL_GameControllerGetAxis(m_controllers[controller], axis);
							if(negate) button_value = -button_value;

							it->m_action_states[controller].value = static_cast<float>(static_cast<double>(button_value) * kAxisNormalizer);
							it->m_action_states[controller].value = Clamp(it->m_action_states[controller].value, InputState::kDeadZone, 1.0f);
						}
					}
				}
			}
		}
	}
}

bool Input::HasMouseInput() const { return SDL_GetCursor() != nullptr; }

std::uint32_t Input::RegisterAction(const char* action_name, const InputButton& default_button)
{
	std::uint32_t action_name_hash = Hash32(action_name);

	ActionContainer::iterator it =
		std::find_if(m_actions.begin(), m_actions.end(), [&](const Action& action) { return action.m_action_name_hash == action_name_hash; });
	if(it != m_actions.end())
	{
		return it->m_action_name_hash;
	}

	Action action;
	action.m_action_name = action_name;
	action.m_action_name_hash = action_name_hash;
	action.m_action_buttons.push_back(default_button);
	m_actions.push_back(action);
	return action_name_hash;
}

const InputState& Input::GetAction(const std::uint32_t& action_name_hash, std::uint32_t controller)
{
	ActionContainer::iterator it =
		std::find_if(m_actions.begin(), m_actions.end(), [&](const Action& action) { return action.m_action_name_hash == action_name_hash; });
	if(it == m_actions.end() || controller >= it->m_action_states.size()) return kNoneState;
	return it->m_action_states[controller];
}
